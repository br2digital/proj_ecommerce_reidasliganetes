import React, { PureComponent } from "react";
import { Chart, AdminButton } from "@components";
// import { NavigationActions } from "react-navigation";
import {
    ScrollView,
} from "react-native";
import FirebaseReq from "@services/FirebaseReq";


class Admin extends PureComponent {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: Back(navigation),
        headerRight: EmptyView(),
        headerTitle: Logo(),
        headerTintColor: Color.headerTintColor,
        headerStyle: Styles.Common.toolbar,
        headerTitleStyle: Styles.Common.headerStyle,
    });

    componentWillMount() {
        //download orders
        // console.log('Will mount Chart...')
        // this.getRawData()
    }

    componentDidMount() {
        // console.log(this.state.rawData)
    }

    // constructor() {
    //     super()
    //     this.state = {
    //         rawData: [],
    //         selectedIndex: 0,
    //         chartData: [],
    //         totalSales: 0,
    //         labels: [],
    //         data: [],
    //         bestSellers: [],
    //     }
    // }

    // async getRawData() {
    //     console.log('Waiting for Firebase...')
    //     await FirebaseReq.getOrders(function (orders) {
    //         this.setState({ rawData: orders })
    //         console.log(this.state.rawData)
    //         // this.updateData(0)
    //         // this.formatChartData(0)
    //     }.bind(this));
    // }

    render() {
        //download data
        // diary sales
        // const data = {
        //     labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        //     datasets: [{
        //       data: [ 20, 45, 28, 80, 99, 43, 50, 54, 67, 120, 23, 34 ]
        //     }]
        // }

        // product sale composition
        const data1 = [
            { name: 'Liganete 1', population: 0, color: 'rgba(131, 167, 234, 1)', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'Liganete 2', population: 45, color: '#F00', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'Liganete 3', population: 150, color: 'red', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'Liganete 4', population: 600, color: '#ffffff', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'Liganete 5', population: 300, color: 'rgb(0, 0, 255)', legendFontColor: '#7F7F7F, legendFontSize: 15' }
        ]

        // mean sales value per day

        // get number of orders
        const numberOfOrders = 15

        //
        // const { navigate, state, goBack } = this.props.navigation;

        return (
            <ScrollView>
                <Chart title={"Vendas"} subtitleStyle={'monetary'} type={"line"} options={"withButtonGroup"} />
                <AdminButton title={"Pedidos"} auxTitle={numberOfOrders} onPress={this.props.onOrderPress }/>
                {/* <Chart chartData={data1} title={"Produtos"} subtitleStyle={'text'} type={"pie"}/>  */}
            </ScrollView>
        )
    }
}

export default Admin;

