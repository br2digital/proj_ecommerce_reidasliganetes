import React, { PureComponent } from "react";
import { Back, EmptyView, Logo } from "./IconNav";
import { SafeAreaView, Text, View , FlatList, Dimensions } from 'react-native';
import { Color, Styles, Card, CardSection } from "@common";
import { OrderAdminButton } from "@components";
import { ButtonGroup } from 'react-native-elements';
import FirebaseReq from "@services/FirebaseReq";
import FirebaseQuery from "@services/FirebaseQuery";

const screenWidth = Dimensions.get('window').width;

export default class OrderAdmin extends PureComponent {
   
    static navigationOptions = ({ navigation }) => ({
        headerLeft: Back(navigation),
        headerRight: EmptyView(),
        headerTitle: Logo(),
        headerTintColor: Color.headerTintColor,
        headerStyle: Styles.Common.toolbar,
        headerTitleStyle: Styles.Common.headerStyle,
    });

    componentWillMount() {
        this.getRawData()
    }

    componentDidMount() {
        this.formatChartData(0)
    }

    constructor () {
        super()
        this.state = {
          selectedIndex: 0,
          rawData = [],
          formatedData = []
        };
        this.updateIndex = this.updateIndex.bind(this);
    }

    async getRawData() {
        console.log('Waiting for Firebase...')
        await FirebaseReq.getOrders(function (orders) {
            this.setState({ rawData: orders })
            console.log(this.state.rawData)
            // this.updateData(0)
            this.formatChartData(0)
        }.bind(this));
    }

    formatListData(selectedIndex) {
        var labels = []
        var totalPeriods = []
        var data = []
        var totalSales = 0

        var bestSellers = FirebaseQuery.getBestSellers(this.state.rawData)
        console.log(bestSellers)

        switch (selectedIndex) {
            case 0: //Day
                labels = ['0h-6h', '7h-8h', '9h-10h', '11h-12h', '13h-14h', '15h-16h', '17h-18h', "19h-23h"]
                totalPeriods = FirebaseQuery.getDayOrdersByPeriod(this.state.rawData)
                data = [totalPeriods.firstPeriod, totalPeriods.secondPeriod, totalPeriods.thirdPeriod, totalPeriods.fourthPeriod, totalPeriods.fifthPeriod, totalPeriods.sixthPeriod, totalPeriods.seventhPeriod, totalPeriods.eightPeriod]
                totalSales = data.reduce((a, b) => a + b, 0);
                // this.setState({ totalSales })
                this.setState({ labels, data })
                console.log('Periods: ' + data[0] + ' ' + data[1] + ' ' + data[2] + ' ' + data[3] + ' ' + data[4] + ' ' + data[5] + ' ' + data[6])
                break;
            case 1: // Week
                const labels = ['Dom', 'Seg', 'Ter','Qua', 'Qui', 'Sex', 'Sáb']
                const totalPeriods = FirebaseQuery.getWeekOrdersByPeriod(this.state.rawData)
                data = [totalPeriods.dom, totalPeriods.seg, totalPeriods.ter, totalPeriods.qua, totalPeriods.qui, totalPeriods.sex, totalPeriods.sab]
                totalSales = data.reduce((a, b) => a + b, 0);
                this.setState({totalSales})
                this.setState({ labels, data })
                console.log('Periods: ' + data[0] + ' ' + data[1] + ' ' + data[2] + ' ' + data[3] + ' ' + data[4] + ' ' + data[5] + ' ' + data[6])
                break;
            case 2: //Month
                labels = ['1', '2', '3', '4', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31']
                totalPeriods = FirebaseQuery.getMonthOrdersByPeriod(this.state.rawData);
                // data = [totalPeriods[0], totalPeriods[1], totalPeriods[2], totalPeriods[3], totalPeriods[4], totalPeriods[5], totalPeriods[6], totalPeriods[7], totalPeriods[8], totalPeriods[9], totalPeriods[10], totalPeriods[11], totalPeriods[12], totalPeriods[13], totalPeriods[14], totalPeriods[15], totalPeriods[16], totalPeriods[17], totalPeriods[18], totalPeriods[19], totalPeriods[20], totalPeriods[21], totalPeriods[22], totalPeriods[23], totalPeriods[24], totalPeriods[25], totalPeriods[26], totalPeriods[27], totalPeriods[28], totalPeriods[29], totalPeriods[30], totalPeriods[31]];
                data = totalPeriods
                totalSales = data.reduce((a, b) => a + b, 0);
                this.setState({ totalSales })
                this.setState({ labels, data })
                console.log('Periods: ' + data[1] + ' ' + data[2] + ' ' + data[3] + ' ' + data[4] + ' ' + data[5] + ' ' + data[6])
                break;
            default:
                break;
        }
    }


    updateIndex (selectedIndex) {
        this.setState({selectedIndex})
        console.log('Index selected: ' + selectedIndex.toString());
    }


    renderButtonGroup(){
        const { selectedIndex } = this.state;
        const buttons = ['Processados', 'Não Processados'];

        return(
            <CardSection>
                <ButtonGroup
                    onPress={this.updateIndex}
                    selectedIndex={selectedIndex}
                    buttons={buttons}
                    containerStyle={{height: 20, width: screenWidth - 40}}
                    // innerBorderStyle={{width: 0.2, color: 'green'}}
                    // buttonStyle={{backgroundColor: 'blue'}}
                    // textStyle={{color: 'white', }}
                />
            </CardSection>  
        );
    }

    render() {

        // get data
        const data = [
            {title: "Pedido #42536", status: "processed"},
            {title: "Pedido #68596", status: "processed"},
            {title: "Pedido #48596", status: "processed"},
            {title: "Pedido #30496", status: "notProcessed"},
            {title: "Pedido #57685", status: "notProcessed"},
            {title: "Pedido #06968", status: "processed"},
            {title: "Pedido #12345", status: "processed"},
        ]

        return (
            <SafeAreaView>
                <View>
                    {/* <Card> */}
                        {this.renderButtonGroup()}
                        {/* <CardSection> */}
                            <FlatList 
                                data={data}
                                renderItem={({item}) => <OrderAdminButton title={item.title} status={item.status} />}
                            />
                        {/* </CardSection> */}
                    {/* </Card> */}
                </View>
            </SafeAreaView>
        );
    }
}

