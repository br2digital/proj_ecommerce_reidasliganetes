/**
 * Created by InspireUI on 06/03/2017.
 *
 * @format
 */

import { WooWorker } from "api-ecommerce";
import FirebaseReq from '@services/FirebaseReq';

const types = {
  PAYMENT_FETCH_SUCCESS: "PAYMENT_FETCH_SUCCESS",
  PAYMENT_FETCHING: "PAYMENT_FETCHING",
  PAYMENT_FETCH_FAILURE: "PAYMENT_FETCH_FAILURE",
};

export const actions = {
  fetchPayments: async (dispatch) => {
    dispatch({ type: types.PAYMENT_FETCHING });
    
    console.log('fetch payment')
    // var json = [
    //   {
    //     "_links": {
    //       "collection": [
    //         {
    //           "href": "http://mstore.io/wp-json/wc/v2/payment_gateways",
    //         },
    //       ],
    //       "self": [
    //         {
    //           "href": "http://mstore.io/wp-json/wc/v2/payment_gateways/stripe",
    //         },
    //       ],
    //     },
    //     "description": "Transferência bancária direta (5% off).",
    //     "enabled": true,
    //     "id": "transfer",
    //     "method_description": "Transferência bancária direta (5% off).",
    //     "method_title": "Transferência bancária",
    //     "order": "",
    //     "settings": {
    //       "apple_pay": {
    //         "default": "no",
    //         "description": "If enabled, users will be able to pay with Apple Pay.",
    //         "id": "apple_pay",
    //         "label": "Enable Apple Pay. <br />By using Apple Pay, you agree to <a href=\"https://stripe.com/apple-pay/legal\" target=\"_blank\">Stripe</a> and <a href=\"https://developer.apple.com/apple-pay/acceptable-use-guidelines-for-websites/\" target=\"_blank\">Apple</a>'s terms of service.",
    //         "placeholder": "",
    //         "tip": "If enabled, users will be able to pay with Apple Pay.",
    //         "type": "checkbox",
    //         "value": "yes",
    //       }
    //     }
    //   },
    //   {
    //     "_links": {
    //       "collection": [
    //         {
    //           "href": "http://mstore.io/wp-json/wc/v2/payment_gateways",
    //         },
    //       ],
    //       "self": [
    //         {
    //           "href": "http://mstore.io/wp-json/wc/v2/payment_gateways/stripe",
    //         },
    //       ],
    //     },
    //     "description": "Com o PagSeguro você pode tem várias opções de pagamento com cartões ou boleto.",
    //     "enabled": true,
    //     "id": "pagseguro",
    //     "method_description": "Com o PagSeguro você pode tem várias opções de pagamento com cartões ou boleto.",
    //     "method_title": "PagSeguro",
    //     "order": "",
    //     "settings": {
    //       "apple_pay": {
    //         "default": "no",
    //         "description": "If enabled, users will be able to pay with Apple Pay.",
    //         "id": "apple_pay",
    //         "label": "Enable Apple Pay. <br />By using Apple Pay, you agree to <a href=\"https://stripe.com/apple-pay/legal\" target=\"_blank\">Stripe</a> and <a href=\"https://developer.apple.com/apple-pay/acceptable-use-guidelines-for-websites/\" target=\"_blank\">Apple</a>'s terms of service.",
    //         "placeholder": "",
    //         "tip": "If enabled, users will be able to pay with Apple Pay.",
    //         "type": "checkbox",
    //         "value": "yes",
    //       }
    //     }
    //   },
    // ];

    await FirebaseReq.getPaymentMethods(function(json){
      console.log(json)
      if (json === undefined) {
        dispatch({ type: types.PAYMENT_FETCH_FAILURE });
      } else if (json.code) {
        dispatch({ type: types.PAYMENT_FETCH_FAILURE });
      } else {
        console.log(json)
        dispatch({
          type: types.PAYMENT_FETCH_SUCCESS,
          payload: json,
          finish: true,
        });
      }
    })

    // const json = await WooWorker.getPayments();

    // if (json === undefined) {
    //   dispatch({ type: types.PAYMENT_FETCH_FAILURE });
    // } else if (json.code) {
    //   dispatch({ type: types.PAYMENT_FETCH_FAILURE });
    // } else {
    //   console.log(json)
    //   dispatch({
    //     type: types.PAYMENT_FETCH_SUCCESS,
    //     payload: json,
    //     finish: true,
    //   });
    // }
  },
};

const initialState = {
  list: [],
  isFetching: false,
};

export const reducer = (state = initialState, action) => {
  const { extra, type, payload, finish } = action;

  switch (type) {
    case types.PAYMENT_FETCH_SUCCESS:
      return {
        ...state,
        list: payload.filter((payment) => payment.enabled === true),
        isFetching: false,
      };

    case types.PAYMENT_FETCH_FAILURE:
      return {
        ...state,
        finish: true,
        isFetching: false,
      };

    case types.PAYMENT_FETCHING:
      return {
        ...state,
        isFetching: true,
      };

    default:
      return state;
  }
};
