import React, { PureComponent } from "react";
import { Admin } from '@containers';
import { SafeAreaView, Text } from 'react-native';
import { Back, EmptyView, Logo } from "./IconNav";
import { Color, Styles } from "@common";

export default class AdminScreen extends PureComponent {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: Back(navigation),
        headerRight: EmptyView(),
        headerTitle: Logo(),
        headerTintColor: Color.headerTintColor,
        headerStyle: Styles.Common.toolbar,
        headerTitleStyle: Styles.Common.headerStyle,
    });
     
    render() {
        const { navigate } = this.props.navigation;

        return (
            <SafeAreaView>
                <Admin 
                    onOrderPress={()=>navigate("OrderAdminScreen")}
                />
            </SafeAreaView>
        );
    }
}

