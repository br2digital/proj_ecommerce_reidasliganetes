import React, { PureComponent } from "react";
import { SafeAreaView, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { Back, EmptyView, Logo } from "./IconNav";
import { Color, Styles, Card, CardSection } from "@common";
import Icon from 'react-native-vector-icons/FontAwesome';

export default class OrderDetailAdminScreen extends PureComponent {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: Back(navigation),
        headerRight: EmptyView(),
        headerTitle: Logo(),
        headerTintColor: Color.headerTintColor,
        headerStyle: Styles.Common.toolbar,
        headerTitleStyle: Styles.Common.headerStyle,
    });

    
     
    render() {
        const { navigate } = this.props.navigation;

        return (
            <SafeAreaView>
                <ScrollView>
                <Card>
                  <CardSection> 
                    <Text style={styles.orderTitle}>Pedido #49508</Text>
                  </CardSection>
                  <Card>
                    <CardSection>
                      <View style={styles.sectionTitleContainer}>
                        <Text style={styles.sectionTitle}>Descrição</Text>
                        <TouchableOpacity>
                          <Icon name="edit" size={20} color="blue" />
                        </TouchableOpacity>
                      </View>
                    </CardSection>
                    <CardSection>
                      <Text style={styles.sectionDescription}>1x Liganete 1{"\n"}4x Liganete 2{"\n"}5x Liganete 6{"\n"}</Text>
                    </CardSection>
                  </Card>
                  <Card>
                    <CardSection>
                      <View style={styles.sectionTitleContainer}>
                        <Text style={styles.sectionTitle}>Endereço de Entrega</Text>
                        <TouchableOpacity>
                          <Icon name="edit" size={20} color="blue" />
                        </TouchableOpacity>
                      </View>
                    </CardSection>
                    <CardSection>
                      <Text style={styles.sectionDescription}>Rua Almir Azevedo, 197{"\n"}CEP: 50740-034{"\n"}Afogados{"\n"}Recife-PE</Text>
                    </CardSection>
                  </Card>
                  <Card>
                    <CardSection>
                      <View style={styles.sectionTitleContainer}>
                        <Text style={styles.sectionTitle}>Cliente</Text>
                        <TouchableOpacity>
                          <Icon name="edit" size={20} color="blue" />
                        </TouchableOpacity>
                      </View>
                    </CardSection>
                    <CardSection>
                      <Text style={styles.sectionDescription}>Lojas Americanas{"\n"}Rua Almir Azevedo, 197{"\n"}CEP: 50740-034{"\n"}Afogados{"\n"}Recife-PE</Text>
                    </CardSection>
                  </Card>
                </Card>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = {
  orderTitle: {
    fontSize: 25,
    padding: 5,
  },
  sectionTitle: {
    fontSize: 18,
    padding: 15,
  },
  sectionDescription: {
    padding: 20,
    fontSize: 14,
    color: 'grey'
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  sectionTitleContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: 10,
  },
  editButton:{
    fontSize:18,
    color: "gray",
    marginRight: 80,
    marginTop: 10,
    marginBottom: 10,
    paddingRight: 50,
  },
}

