import React, { PureComponent } from "react";
import { Back, EmptyView, Logo } from "./IconNav";
import { SafeAreaView, Text, View , FlatList, Dimensions } from 'react-native';
import { Color, Styles, Card, CardSection } from "@common";
import { OrderAdminButton } from "@components";
import { ButtonGroup } from 'react-native-elements';


const screenWidth = Dimensions.get('window').width;

export default class OrderAdminScreen extends PureComponent {
   
    static navigationOptions = ({ navigation }) => ({
        headerLeft: Back(navigation),
        headerRight: EmptyView(),
        headerTitle: Logo(),
        headerTintColor: Color.headerTintColor,
        headerStyle: Styles.Common.toolbar,
        headerTitleStyle: Styles.Common.headerStyle,
    });

    constructor () {
        super()
        this.state = {
          selectedIndex: 0,
        };
        this.updateIndex = this.updateIndex.bind(this);
    }

    updateIndex (selectedIndex) {
        this.setState({selectedIndex})
        console.log('Index selected: ' + selectedIndex.toString());
    }

    renderButtonGroup(){
        const { selectedIndex } = this.state;
        const buttons = ['Processados', 'Não Processados'];

        return(
            <CardSection>
                <ButtonGroup
                    onPress={this.updateIndex}
                    selectedIndex={selectedIndex}
                    buttons={buttons}
                    containerStyle={{height: 20, width: screenWidth - 40}}
                    // innerBorderStyle={{width: 0.2, color: 'green'}}
                    // buttonStyle={{backgroundColor: 'blue'}}
                    // textStyle={{color: 'white', }}
                />
            </CardSection>  
        );
    }

    render() {

        // get data
        const data = [
            {title: "Pedido #42536", status: "processed"},
            {title: "Pedido #68596", status: "processed"},
            {title: "Pedido #48596", status: "processed"},
            {title: "Pedido #30496", status: "notProcessed"},
            {title: "Pedido #57685", status: "notProcessed"},
            {title: "Pedido #06968", status: "processed"},
            {title: "Pedido #12345", status: "processed"},
        ]

        const { navigate } = this.props.navigation;

        return (
            <SafeAreaView>
                <View>
                    {/* <Card> */}
                        {this.renderButtonGroup()}
                        {/* <CardSection> */}
                            <FlatList 
                                data={data}
                                renderItem={({item}) => <OrderAdminButton title={item.title} status={item.status} onPress={()=>navigate("OrderDetailAdminScreen")} />}
                            />
                        {/* </CardSection> */}
                    {/* </Card> */}
                </View>
            </SafeAreaView>
        );
    }
}

