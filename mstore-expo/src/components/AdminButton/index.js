import React, { PureComponent } from 'react';
import { Card, CardSection } from '@common';
import { TouchableOpacity, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './index_style';

class AdminButton extends PureComponent {

  render() {

    return (
        <TouchableOpacity onPress={this.props.onPress}>
            <Card>
                <CardSection>
                    <View style={styles.buttonContainer}>
                        <Text style={styles.title}>{this.props.title} <Text style={styles.auxTitle}>({this.props.auxTitle})</Text></Text>
                        <Icon name="angle-right" size={30} color="gray" />
                    </View>
                </CardSection>
            </Card>
        </TouchableOpacity>
    );
  }
}

export default AdminButton;