import React, { PureComponent } from 'react';
import { Card, CardSection } from '@common';
import { TouchableOpacity, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './index_style';

class OrderAdminButton extends PureComponent {
  
  renderStatusIcon(){
      if(this.props.status == "processed"){
        return(
          <Icon name="check-circle" size={15} color="green" />
        );
      } else if (this.props.status == "notProcessed"){
        return(
          <Icon name="minus" size={15} color="red" />
        );
      }
  }

  render() {

    return (
        <TouchableOpacity onPress={this.props.onPress}>
            <Card>
                <CardSection>
                    <View style={styles.buttonContainer}>
                        <Text style={styles.title}>{this.props.title}</Text>
                        {this.renderStatusIcon()}
                    </View>
                </CardSection>
            </Card>
        </TouchableOpacity>
    );
  }
}

export default OrderAdminButton;