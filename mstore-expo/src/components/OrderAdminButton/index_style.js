
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {},
  productView: {},
  scrollView: {
    flex: 1,
    width: null,
    height: null,
    marginBottom: 20,
  },
  title: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 18,
    color: "black",
    textAlign: "center",
  },
  subtitle: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    fontSize: 18,
    color: "black",
    textAlign: "center",
  },
  arrow: {
      fontSize:18,
      color: "gray",
      marginRight: 80,
      marginTop: 10,
      marginBottom: 10,
      paddingRight: 50,
   },
   buttonContainer: {
       flex: 1,
       flexDirection: 'row',
       justifyContent: 'space-between',
       alignItems: 'center',
   },
   auxTitle: {
     fontSize: 15,
     color: "gray"
   }
});