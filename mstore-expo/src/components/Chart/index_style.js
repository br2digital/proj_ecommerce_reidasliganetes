import { StyleSheet } from "react-native";

export default StyleSheet.create({
  scrollView: {
    flex: 1,
    width: null,
    height: null,
    marginBottom: 20,
  },
  title: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    fontSize: 25,
    color: "black",
    textAlign: "center",
  },
  subtitle: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    fontSize: 18,
    color: "black",
    textAlign: "center",
  },
});