import React, { PureComponent } from "react";
import { ScrollView, Dimensions, SafeAreaView, Text } from 'react-native';
import { BarChart, PieChart, LineChart } from 'react-native-chart-kit';
import { Card, CardSection } from '@common';
import styles from "./index_style";
import { ButtonGroup } from 'react-native-elements';
import FirebaseReq from "@services/FirebaseReq";
import FirebaseQuery from "@services/FirebaseQuery";
import Moment from 'moment';


// const chartConfig = {
//     backgroundColor: '#e26a00',
//     backgroundGradientFrom: '#fb8c00',
//     backgroundGradientTo: '#ffa726',
//     color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
//     style: {
//       borderRadius: 18,
//     }
//   };

// const chartConfig = {
//     backgroundColor: '#1e3972',
//     backgroundGradientFrom: '#274d9e',
//     backgroundGradientTo: '#416ac1',
//     color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
//     style: {
//       borderRadius: 18,
//     }
//   };

const chartConfig = {
    backgroundColor: '#fff',
    backgroundGradientFrom: '#fff',
    backgroundGradientTo: '#fff',
    color: (opacity = 1) => `rgba(39, 77, 158, ${opacity})`,
    style: {
        borderRadius: 18,
    }
};

const screenWidth = Dimensions.get('window').width;

class Chart extends PureComponent {

    componentWillMount() {
        //download orders
        console.log('Will mount Chart...')
        this.getRawData()
    }

    componentDidMount() {
        // this.updateData(0)
        this.formatChartData(0)
    }

    componentWillUnmount() {
        console.log('Unmounting Chart...')
    }

    constructor() {
        super()
        this.state = {
            rawData: [],
            selectedIndex: 0,
            chartData: [],
            totalSales: 0,
            labels: [],
            data: [],
            bestSellers: [],
        };

        this.updateIndex = this.updateIndex.bind(this)
    }

    async getRawData() {
        console.log('Waiting for Firebase...')
        await FirebaseReq.getOrders(function (orders) {
            this.setState({ rawData: orders })
            console.log(this.state.rawData)
            // this.updateData(0)
            this.formatChartData(0)
        }.bind(this));
    }

    formatChartData(selectedIndex) {
        var labels = []
        var totalPeriods = []
        var data = []
        var totalSales = 0

        var bestSellers = FirebaseQuery.getBestSellers(this.state.rawData)
        console.log(bestSellers)

        switch (selectedIndex) {
            case 0: //Day
                labels = ['0h-6h', '7h-8h', '9h-10h', '11h-12h', '13h-14h', '15h-16h', '17h-18h', "19h-23h"]
                totalPeriods = FirebaseQuery.getDayOrdersByPeriod(this.state.rawData)
                data = [totalPeriods.firstPeriod, totalPeriods.secondPeriod, totalPeriods.thirdPeriod, totalPeriods.fourthPeriod, totalPeriods.fifthPeriod, totalPeriods.sixthPeriod, totalPeriods.seventhPeriod, totalPeriods.eightPeriod]
                totalSales = data.reduce((a, b) => a + b, 0);
                this.setState({ totalSales })
                this.setState({ labels, data })
                console.log('Periods: ' + data[0] + ' ' + data[1] + ' ' + data[2] + ' ' + data[3] + ' ' + data[4] + ' ' + data[5] + ' ' + data[6])
                break;
            case 1: // Week
                const labels = ['Dom', 'Seg', 'Ter','Qua', 'Qui', 'Sex', 'Sáb']
                const totalPeriods = FirebaseQuery.getWeekOrdersByPeriod(this.state.rawData)
                data = [totalPeriods.dom, totalPeriods.seg, totalPeriods.ter, totalPeriods.qua, totalPeriods.qui, totalPeriods.sex, totalPeriods.sab]
                totalSales = data.reduce((a, b) => a + b, 0);
                this.setState({totalSales})
                this.setState({ labels, data })
                console.log('Periods: ' + data[0] + ' ' + data[1] + ' ' + data[2] + ' ' + data[3] + ' ' + data[4] + ' ' + data[5] + ' ' + data[6])
                break;
            case 2: //Month
                labels = ['1', '2', '3', '4', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31']
                totalPeriods = FirebaseQuery.getMonthOrdersByPeriod(this.state.rawData);
                // data = [totalPeriods[0], totalPeriods[1], totalPeriods[2], totalPeriods[3], totalPeriods[4], totalPeriods[5], totalPeriods[6], totalPeriods[7], totalPeriods[8], totalPeriods[9], totalPeriods[10], totalPeriods[11], totalPeriods[12], totalPeriods[13], totalPeriods[14], totalPeriods[15], totalPeriods[16], totalPeriods[17], totalPeriods[18], totalPeriods[19], totalPeriods[20], totalPeriods[21], totalPeriods[22], totalPeriods[23], totalPeriods[24], totalPeriods[25], totalPeriods[26], totalPeriods[27], totalPeriods[28], totalPeriods[29], totalPeriods[30], totalPeriods[31]];
                data = totalPeriods
                totalSales = data.reduce((a, b) => a + b, 0);
                this.setState({ totalSales })
                this.setState({ labels, data })
                console.log('Periods: ' + data[1] + ' ' + data[2] + ' ' + data[3] + ' ' + data[4] + ' ' + data[5] + ' ' + data[6])
                break;
            default:
                break;
        }
    }

    renderChart() {
        if (this.props.type == "pie") {
            return (
                <PieChart
                    data={this.props.chartData}
                    width={screenWidth + 150}
                    height={300}
                    chartConfig={chartConfig}
                    accessor="population"
                    bgColor="transparent"
                    paddingLeft="15"
                />
            );
        } else if (this.props.type == "line") {
            const data = {
                labels: this.state.labels,
                datasets: [{
                    data: this.state.data,
                }]
            }

            return (
                <LineChart
                    // data={this.props.chartData}
                    data={data}
                    width={screenWidth + 500}
                    height={300}
                    chartConfig={chartConfig}
                />
            );
        } else if (this.props.type == 'line') {
            // return(
            //     // <Line
            // )
        }
    }

    renderButtonGroup() {
        const { selectedIndex } = this.state;
        const buttons = ['Dia', 'Semana', 'Mês'];

        if (this.props.options == 'withButtonGroup') {
            return (
                <CardSection>
                    <ButtonGroup
                        onPress={this.updateIndex}
                        selectedIndex={selectedIndex}
                        buttons={buttons}
                        containerStyle={{ height: 20, width: screenWidth - 40 }}
                    // innerBorderStyle={{width: 0.2, color: 'green'}}
                    // buttonStyle={{backgroundColor: 'blue'}}
                    // textStyle={{color: 'white', }}
                    />
                </CardSection>
            )
        }
    }

    renderSubtitle(style) {
        switch (style) {
            case 'text':
                return (
                    <Text style={styles.subtitle}>mais vendido: Liganete 1</Text>
                )
            case 'monetary':
                return (
                    <Text style={styles.subtitle}>{'R$ ' + this.floatToString(this.state.totalSales)}</Text>
                );
            default:
                break;
        }
    }

    updateIndex(selectedIndex) {
        this.setState({ selectedIndex })
        // this.updateData(selectedIndex)
        this.formatChartData(selectedIndex)
        console.log('Index selected: ' + selectedIndex.toString());
    }

    floatToString(x) {
        return x.toFixed(2).toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }

    render() {
        return (
            <SafeAreaView>
                <Card>
                    <CardSection>
                        <Text style={styles.title}>{this.props.title}</Text>
                    </CardSection>
                    {this.renderButtonGroup()}
                    <CardSection>
                        {/* <Text style={styles.subtitle}>{'R$ '+this.floatToString(this.state.totalSales)}</Text> */}
                        {this.renderSubtitle(this.props.subtitleStyle)}
                    </CardSection>
                    <CardSection>
                        <ScrollView horizontal={true} style={styles.container}>
                            {this.renderChart()}
                        </ScrollView>
                    </CardSection>
                </Card>
            </SafeAreaView>
        );
    }
}

export default Chart;