// /** @format */

// export default {
//   Exit: "Exit",
//   ExitConfirm: "Are you sure you want to exit this app",
//   YES: "YES",
//   OK: "OK",
//   ViewMyOrders: "View My Oders",
//   CANCEL: "CANCEL",
//   Confirm: "Confirm",

//   // Scene's Titles
//   Home: "Home",
//   Intro: "Intro",
//   Product: "Product",
//   Cart: "Cart",
//   WishList: "WishList",

//   // Home
//   products: "products",

//   // TopBar
//   ShowFilter: "Sub Categories",
//   HideFilter: "Hide",
//   Sort: "Sort",
//   textFilter: "Recent",

//   // Category
//   ThereIsNoMore: "There is no more product to show",

//   // Product
//   AddtoCart: "Add to Cart",
//   AddtoWishlist: "Add to Wishlist",
//   ProductVariations: "Variations",
//   NoVariation: "This product don't have any variation",
//   AdditionalInformation: "Description",
//   NoProductDescription: "No Product Description",
//   ProductReviews: "Reviews",
//   NoReview: "This product don't have any reviews ...yet",
//   BUYNOW: "BUY NOW",
//   OutOfStock: "OUT OF STOCK",
//   ProductLimitWaring: "You can't add more than 10 product",
//   EmptyProductAttribute: "This product don't have any attributes",
//   ProductFeatures: "Features",
//   ErrorMessageRequest: "Can't get data from server",
//   NoConnection: "No internet connection",
//   ProductRelated: "Related Products",

//   // Cart
//   NoCartItem: "There is no product in cart",
//   Total: "Total",
//   EmptyCheckout: "Sorry, you can't check out an empty cart",
//   RemoveCartItemConfirm: "Remove this product from cart?",
//   MyCart: "Cart",
//   Order: "Order",
//   ShoppingCart: "Shopping Cart",
//   ShoppingCartIsEmpty: "Your Cart is Empty",
//   AddProductToCart: "Add a product to the shopping cart",
//   TotalPrice: "Total Price:",
//   YourDeliveryInfo: "Your Shipping Detail",
//   ShopNow: "Shop Now",
//   YourChoice: "Your cart:",
//   YourSale: "Your Sale:",
//   SubtotalPrice: "Subtotal Price:",
//   BuyNow: "Buy Now",
//   Items: "items",
//   Item: "item",
//   ThankYou: "Thank you",
//   FinishOrderCOD: "You can use to number of order to track shipping status",
//   FinishOrder:
//     "Thank you so much for your purchased, to check your delivery status please go to My Orders",
//   NextStep: "Next Step",
//   ConfirmOrder: "Confirm Order",
//   RequireEnterAllFileds: "Please enter all fields",
//   Error: "Error",
//   InvalidEmail: "Invalid email address",
//   Finish: "Finish",

//   // Wishlist
//   NoWishListItem: "There is no item in wishlist",
//   MoveAllToCart: "Add all to cart",
//   EmptyWishList: "Empty wishlist",
//   EmptyAddToCart: "Sorry, the wishlist is empty",
//   RemoveWishListItemConfirm: "Remove this product from wishlist?",
//   CleanAll: "Clean All",

//   // Sidemenu
//   SignIn: "Log In",
//   SignOut: "Log Out",
//   GuestAccount: "Guest Account",
//   CantReactEmailError:
//     "We can't reach your email address, please try other login method",
//   NoEmailError: "Your account don't have valid email address",
//   EmailIsNotVerifiedError:
//     "Your email address is not verified, we can' trust you",
//   Login: "Login",
//   Logout: "Logout",
//   Shop: "Shop",
//   Category: "Category",

//   // Checkout
//   Checkout: "Checkout",
//   ProceedPayment: "Proceed Payment",
//   Purchase: "Purchase",
//   CashOnDelivery: "Cash on Delivery",
//   CreditCard: "Credit Card",
//   PaymentMethod: "Payment Method - Not select",
//   PaymentMethodError: "Please select your payment method",
//   PayWithCoD: "Your purchase will be pay when goods were delivered",
//   PayWithPayPal: "Your purchase will be pay with PayPal",
//   Paypal: "paypal",
//   Stripe: "stripe",
//   PayWithStripe: "Your purchase will be pay with Stripe",
//   ApplyCoupon: "Apply",
//   CouponPlaceholder: "Coupon Code",
//   Apply: "Apply",
//   Applying: "Applying",
//   Back: "Back",
//   CardNamePlaceholder: "Name written on card",
//   BackToHome: "Back to Home",
//   OrderCompleted: "Your order was completed",
//   OrderCanceled: "Your order was canceled",
//   OrderFailed: "Something went wrong...",
//   OrderCompletedDesc: "Your order id is ",
//   OrderCanceledDesc:
//     "You have canceled the order. The transaction has not been completed",
//   OrderFailedDesc:
//     "We have encountered an error while processing your order. The transaction has not been completed. Please try again",
//   OrderTip:
//     'Tip: You could track your order status in "My Orders" section from side menu',
//   Delivery: "Delivery",
//   Payment: "Payment",
//   Complete: "Complete",
//   EnterYourFirstName: "Enter your First Name",
//   EnterYourLastName: "Enter your Last Name",
//   EnterYourEmail: "Enter your email",
//   EnterYourPhone: "Enter your phone",
//   EnterYourAddress: "Enter your address",
//   CreateOrderError: "Cannot create new order. Please try again later",

//   // myorder
//   MyOrder: "My Order",
//   NoOrder: "You don't have any orders",
//   OrderDate: "Order Date: ",
//   OrderStatus: "Status: ",
//   OrderPayment: "Payment method: ",
//   OrderTotal: "Total: ",
//   OrderDetails: "Show detail",

//   News: "News",
//   PostDetails: "Post Details",
//   FeatureArticles: "Feature articles",
//   MostViews: "Most views",
//   EditorChoice: "Editor choice",

//   // settings
//   Settings: "Settings",
//   BASICSETTINGS: "BASIC SETTINGS",
//   Language: "Language",
//   INFO: "INFO",
//   About: "About us",

//   // language
//   AvailableLanguages: "Available Languages",
//   SwitchLanguage: "Switch Language",
//   SwitchLanguageConfirm: "Switch language require an app reload, continue?",

//   // about us
//   AppName: "MSTORE",
//   AppDescription: "React Native template for mCommerce",
//   AppContact: " Contact us at: mstore.io",
//   AppEmail: " Email: support@mstore.io",
//   AppCopyRights: "© MSTORE 2016",

//   // contact us
//   contactus: "Contact Us",

//   // form
//   NotSelected: "Not selected",
//   EmptyError: "This field is empty",
//   DeliveryInfo: "Delivery Info",
//   FirstName: "First Name",
//   LastName: "Last Name",
//   Address: "Address",
//   City: "Town/City",
//   State: "State",
//   NotSelectedError: "Please choose one",
//   Postcode: "Postcode",
//   Country: "Country",
//   Email: "Email",
//   Phone: "Phone Number",
//   Note: "Note",

//   // search
//   Search: "Search",
//   SearchPlaceHolder: "Search product by name",
//   NoResultError: "Your search keyword did not match any products.",
//   Details: "Details",

//   // filter panel
//   Categories: "Categories",
//   Loading: "LOADING...",
//   welcomeBack: "Welcome back! ",
//   seeAll: "Show All",

//   // Layout
//   cardView: "Card ",
//   simpleView: "List View",
//   twoColumnView: "Two Column ",
//   threeColumnView: "Three Column ",
//   listView: "List View",
//   default: "Default",
//   advanceView: "Advance ",
//   horizontal: "Horizontal ",

//   couponCodeIsExpired: "This coupon code is expired",
//   invalidCouponCode: "This coupon code is invalid",
//   remove: "Remove",
//   applyCouponSuccess: "Congratulations! Coupon code applied successfully ",
//   reload: "Reload",

//   ShippingType: "Shipping method",

//   // Place holder
//   TypeFirstName: "Type your first name",
//   TypeLastName: "Type your last name",
//   TypeAddress: "Type address",
//   TypeCity: "Type your town or city",
//   TypeState: "Type your state",
//   TypeNotSelectedError: "Please choose one",
//   TypePostcode: "Type postcode",
//   TypeEmail: "Type email (Ex. acb@gmail.com), ",
//   TypePhone: "Type your phone number",
//   TypeNote: "Note",
//   TypeCountry: "Select country",
//   SelectPayment: "Select Payment method",
//   close: "CLOSE",
//   noConnection: "NO INTERNET ACCESS",

//   // user profile screen
//   AccountInformations: "Account Informations",
//   PushNotification: "Push notification",
//   Privacy: "Privacy policies",
//   SelectCurrency: "Select currency",
//   Name: "Name",
//   Currency: "Currency",
//   Languages: "Languages",
//   Guest: "Guest",
//   FacebookLogin: "Facebook Login",
//   Or: "Or",
//   UserOrEmail: "Username or Email",
//   DontHaveAccount: "Don't have account? ",
//   accountDetails: "Account Details",
//   username: "Username",
//   email: "Email",
//   generatePass: "Use generate password",
//   password: "Password",
//   signup: "Sign Up",
//   profileDetail: "Profile Details",
//   firstName: "First name",
//   lastName: "Last name",

//   // Horizontal
//   featureProducts: "Feature Products",
//   bagsCollections: "Bags Collections",
//   womanBestSeller: "Woman Best Seller",
//   manCollections: "Man Collections",

//   // Modal
//   Select: "Select",
//   Cancel: "Cancel",

//   // review
//   vendorTitle: "Vendor",
//   comment: "Leave a review",
//   yourcomment: "Your comment",
//   placeComment:
//     "Tell something about your experience or leave a tip for others",
//   writeReview: "Write A Review",
//   thanksForReview:
//     "Thanks for the review, your content will be verify by the admin and will be published later",
//   errInputComment: "Please input your content to submit",
//   errRatingComment: "Please rating to submit",
//   send: "Send",
// };

/** @format */

export default {
  Exit: "Sair",
  ExitConfirm: "Tem certeza que deseja sair do app",
  YES: "SIM",
  OK: "OK",
  ViewMyOrders: "Ver meus pedidos",
  CANCEL: "CANCELAR",
  Confirm: "Confirmar",

  // Scene's Titles
  Home: "Home",
  Intro: "Intro",
  Product: "Produto",
  Cart: "Carrinho",
  WishList: "Lista de desejos",

  // Home
  products: "produtos",

  // TopBar
  ShowFilter: "Sub Categorias",
  HideFilter: "Esconder",
  Sort: "Ordenar",
  textFilter: "Recente",

  // Category
  ThereIsNoMore: "Não há mais produtos para mostrar",

  // Product
  AddtoCart: "Adicionar ao carrinho",
  AddtoWishlist: "Adicionar a Lista de Desejos",
  ProductVariations: "Variações",
  NoVariation: "Esse produto não tem variações",
  AdditionalInformation: "Descrição",
  NoProductDescription: "Sem descrição",
  ProductReviews: "Reviews",
  NoReview: "Esse produto não tem nenhum review ...ainda",
  BUYNOW: "COMPRE AGORA",
  OutOfStock: "FORA DE ESTOQUE",
  ProductLimitWaring: "Você não pode adicionar mais que 10 produtos",
  EmptyProductAttribute: "Esse produto não tem nenhum atributo",
  ProductFeatures: "Detalhes",
  ErrorMessageRequest: "Não foi possível carregar os dados",
  NoConnection: "Sem conexão a internet",
  ProductRelated: "Produtos Relacionados",

  // Cart
  NoCartItem: "Não há produtos no carrinho",
  Total: "Total",
  EmptyCheckout: "Desculpe, não é possível fazer check out em um carrinho vazio",
  RemoveCartItemConfirm: "Remover produto do carrinho?",
  MyCart: "Carrinho",
  Order: "Pedido",
  ShoppingCart: "Carrinho de Compras",
  ShoppingCartIsEmpty: "Seu carrinho está vazio",
  AddProductToCart: "Adicione um produto ao seu carrinho",
  TotalPrice: "Preço Total:",
  YourDeliveryInfo: "Detalhes de envio",
  ShopNow: "Compre agora",
  YourChoice: "Seu carrinho",
  YourSale: "Sua Venda:",
  SubtotalPrice: "Subtotal:",
  BuyNow: "Compre Agora",
  Items: "itens",
  Item: "item",
  ThankYou: "Obrigado",
  FinishOrderCOD: "Você pode usar o número do pedido para rastrear o status da entrega.",
  FinishOrder:
    "Muito obrigado pela sua compra, para checar o status de entrega por favor verifique Meus Pedidos.",
  NextStep: "Próximo passo",
  ConfirmOrder: "Confirmar pedido",
  RequireEnterAllFileds: "Por favor preencha todos os campos",
  Error: "Erro",
  InvalidEmail: "Email inválido",
  Finish: "Completar",

  // Wishlist
  NoWishListItem: "Não há itens na Lista de Desejo",
  MoveAllToCart: "Adicionar tudo ao carrinho",
  EmptyWishList: "Lista de desejos vazia",
  EmptyAddToCart: "Desculpe, sua Lista de Desejos está vazia",
  RemoveWishListItemConfirm: "Remover produto da Lista de Desejos?",
  CleanAll: "Limpar tudo",

  // Sidemenu
  SignIn: "Log In",
  SignOut: "Log Out",
  GuestAccount: "Conta de convidado",
  CantReactEmailError:
    "Não conseguimos verificar seu endereço de email, por favor tente outra forma de Login",
  NoEmailError: "Sua conta não possui um Email válido",
  EmailIsNotVerifiedError:
    "Seu conta ainda não foi validada, cheque seu Email",
  Login: "Login",
  Logout: "Logout",
  Shop: "Compras",
  Category: "Categoria",

  // Checkout
  Checkout: "Checkout",
  ProceedPayment: "Prosseguir com pagamento",
  Purchase: "Compra",
  CashOnDelivery: "Pagamento na entrega",
  CreditCard: "Cartão de crédito",
  PaymentMethod: "Método de pagamento - Não selecionado",
  PaymentMethodError: "Por favor selecione seu método de pagamento",
  PayWithCoD: "Sua compra será paga na Entrega.",
  PayWithPayPal: "Sua compra será paga com PayPal",
  Paypal: "paypal",
  Stripe: "stripe",
  PayWithStripe: "Sua compra será paga com o Stripe",
  ApplyCoupon: "Aplicar",
  CouponPlaceholder: "Código de cupom",
  Apply: "Aplicar",
  Applying: "Aplicando",
  Back: "Voltar",
  CardNamePlaceholder: "Nome como está no cartão",
  BackToHome: "Voltar para Home",
  OrderCompleted: "Seu pedido foi completado",
  OrderCanceled: "Seu pedido foi cancelado",
  OrderFailed: "Algo deu errado...",
  OrderCompletedDesc: "Seu número do pedido é ",
  OrderCanceledDesc:
    "Você cancelou o pedido. A transação não foi completada",
  OrderFailedDesc:
    "Nós encontramos um erro ao processar seu pedido. A transação não foi completada. Por favor, tente novamente",
  OrderTip:
    'Dica: Você pode verificar seu pedido na seção "Meus pedidos" no menu lateral',
  Delivery: "Entrega",
  Payment: "Pagamento",
  Complete: "Completo",
  EnterYourFirstName: "Escreva seu Primeiro Nome",
  EnterYourLastName: "Escreva seu Último Nome",
  EnterYourEmail: "Escreva seu Email",
  EnterYourPhone: "Escreva seu telefone",
  EnterYourAddress: "Escreva seu Endereço",
  CreateOrderError: "Não foi possível criar um novo pedido. Por favor, tente novamente",

  // myorder
  MyOrder: "Meu Pedido",
  NoOrder: "Você não tem Pedidos",
  OrderDate: "Data do Pedido: ",
  OrderStatus: "Status: ",
  OrderPayment: "Método de Pagamento: ",
  OrderTotal: "Total: ",
  OrderDetails: "Mostrar detalhes",

  News: "Novidades",
  PostDetails: "Detalhes do post",
  FeatureArticles: "Principais Artigos",
  MostViews: "Mais vistos",
  EditorChoice: "Escolha do editor",

  // settings
  Settings: "Configurações",
  BASICSETTINGS: "CONFIGURAÇÕES BÁSICAS",
  Language: "Idioma",
  INFO: "INFO",
  About: "Sobre nós",

  // language
  AvailableLanguages: "Linguagens disponíveis",
  SwitchLanguage: "Trocar idioma",
  SwitchLanguageConfirm: "Trocar o idioma requer reiniciar o app, quer continuar?",

  // about us
  AppName: "O Rei das Liganetes",
  AppDescription: "Fábrica de Roupas no Atacado Baratas Para Revender é aqui no O Rei da Liganete!",
  AppContact: " Entre em contato: (81) 9 - 9930 - 6677 (TIM)",
  AppEmail: " Email: contato@oreidaliganete.com.br",
  AppCopyRights: "© O Rei das Liganetes 2018",

  // contact us
  contactus: "Entre em contato",

  // form
  NotSelected: "Não selecionado",
  EmptyError: "Esse campo está vazio",
  DeliveryInfo: "Informações de entrega",
  FirstName: "Primeiro Nome",
  LastName: "Último Nome",
  Address: "Endereço",
  City: "Cidade",
  State: "Estado",
  NotSelectedError: "Por favor escolha um",
  Postcode: "CEP",
  Country: "País",
  Email: "Email",
  Phone: "Número de telefone",
  Note: "Nota",

  // search
  Search: "Procurar",
  SearchPlaceHolder: "Procurar por nome",
  NoResultError: "Sua busca não retornou nenhum produto.",
  Details: "Detalhes",

  // filter panel
  Categories: "Categorias",
  Loading: "CARREGANDO...",
  welcomeBack: "Bem-vindo de volta! ",
  seeAll: "Mostrar Tudo",

  // Layout
  cardView: "Card ",
  simpleView: "List View",
  twoColumnView: "Two Column ",
  threeColumnView: "Three Column ",
  listView: "List View",
  default: "Default",
  advanceView: "Advance ",
  horizontal: "Horizontal ",

  couponCodeIsExpired: "Esse cupom está expirado",
  invalidCouponCode: "Esse cupom é inválido",
  remove: "Remover",
  applyCouponSuccess: "Parabéns! Cupom aplicado com sucesso ",
  reload: "Atualizar",

  ShippingType: "Método de envio",

  // Place holder
  TypeFirstName: "Escreva seu Primeiro Nome",
  TypeLastName: "Escreva seu Último Nome",
  TypeAddress: "Escreva seu Endereço",
  TypeCity: "Escreva sua Cidade",
  TypeState: "Escreva seu Estado",
  TypeNotSelectedError: "Por favor seleciona uma opção",
  TypePostcode: "Escreva seu CEP",
  TypeEmail: "Escreva seu Email (Ex.: acb@gmail.com), ",
  TypePhone: "Escreva seu telefone",
  TypeNote: "Note",
  TypeCountry: "Selecione seu país",
  SelectPayment: "Selecione o Método de Pagamento",
  close: "FECHAR",
  noConnection: "SEM ACESSO À INTERNETE",

  // user profile screen
  AccountInformations: "Informações da Conta",
  PushNotification: "Notificações",
  Privacy: "Políticas de Privacidade",
  SelectCurrency: "Selecionar moeda",
  Name: "Nome",
  Currency: "Moeda",
  Languages: "Idioma",
  Guest: "Convidado",
  FacebookLogin: "Login pelo Facebook",
  Or: "Ou",
  UserOrEmail: "Username ou Email",
  DontHaveAccount: "Não tem uma Conta? ",
  accountDetails: "Detalhes da Conta",
  username: "Username",
  email: "Email",
  generatePass: "Gerar uma senha",
  password: "Senha (no mínimo 6 caracteres)",
  signup: "Cadastrar",
  profileDetail: "Detalhes do Perfil",
  firstName: "Primeiro Nome",
  lastName: "Último Nome",

  // Horizontal
  featureProducts: "Destaques",
  bagsCollections: "Coleções de Bolsas",
  womanBestSeller: "Mais Vendidos Femininos",
  manCollections: "Coleções Masculinas",

  // Modal
  Select: "Selecionar",
  Cancel: "Cancelar",

  // review
  vendorTitle: "O Rei das Liganetes",
  comment: "Deixe um comentário",
  yourcomment: "Seu comentário",
  placeComment:
    "Comente sobre sua experiência de compra ou deixe alguma dica",
  writeReview: "Escreva um Review",
  thanksForReview:
    "Obrigado pelo seu Review, ele será verificado por nós e será publicado",
  errInputComment: "Por favor escreva algo para enviar",
  errRatingComment: "Por favor dê sua nota",
  send: "Enviar",

  // messages form
  invalidEmailFormat: "Email inválido. Por favor tente novamente.",
  incompleteForm: "Por favor preencha todos os campos.",
  wrongPasswordLength: "Sua senha deve ter pelo menos 6 caracteres.",
  successSignUp: "Sua conta foi criada com sucesso!",
  serverProblem: "Não foi possível efetuar o cadastro. Tente novamente.",

  // internet
  regainInternetConnection: "Conectado à internet",
};

