import Firebase from 'firebase';
import {  Languages } from "@common";


const firebaseConfig = {
  apiKey: "AIzaSyAwW45-mnAtLFQSInya2wd3ZLs4K_b_ZO8",
  authDomain: "teste-reidasliganetes.firebaseapp.com",
  databaseURL: "https://teste-reidasliganetes.firebaseio.com",
  projectId: "teste-reidasliganetes",
  storageBucket: "teste-reidasliganetes.appspot.com",
  messagingSenderId: "341314931155"
};

const firebaseApp = Firebase.initializeApp(firebaseConfig);
const db = firebaseApp.database();

const FirebaseReq = {

  getProducts: (fn) => {
    let itemsRef = db.ref('/Products');
    var products = [];

    itemsRef.on('value', (snapshot) => {
      let data = JSON.stringify(snapshot.val());
      let json = JSON.parse(data)

      for (let i = 0; i < json.length; i++) {

        var newProduct = {
          id: String,
          name: String,
          images: Array,
          price: Float32Array,
          total_sales: Int16Array,
          weight: Float32Array,
          categories: String,
          in_stock: Float32Array,
          description: String,
        }

        newProduct.id = json[i].id
        newProduct.name = json[i].name;
        newProduct.images = json[i].images;
        newProduct.price = json[i].price;
        newProduct.total_sales = json[i].total_sales;
        newProduct.weight = json[i].weight;
        newProduct.categories = json[i].categories;
        newProduct.in_stock = json[i].in_stock;
        newProduct.description = json[i].description;
        newProduct.rating_count = json[i].rating_count;

        products.push(newProduct)
      }

      fn(products)
    });

  },
  
  // getCategories: (fn) => {
  //   let itemsRef = db.ref('/Products');
  //   var categories = [];

  //   itemsRef.on('value', (snapshot) => {
  //     let data = JSON.stringify(snapshot.val());
  //     let json = JSON.parse(data)

  //     for (let i = 0; i < json.length; i++) {
  //       var currentProductCategories = json[i].categories
  //       for (let j = 0; j < currentProductCategories.length; j++) {
  //         const currentCat = currentProductCategories[j];
  //         // console.log(currentCat)
  //         if (!categories.find(function(elem){ return currentCat['id']==elem['id']})) {
  //           categories.push(currentCat)
  //           // console.log(currentCat)
  //         }
  //       }
  //     }
  //     // console.log(categories)
  //     fn(categories)
  //   });
  // },
  getCategories: (fn) => {
    let itemsRef = db.ref('/Categories');
    var categories = []

    console.log('FirebaseReq.getCategories()')

    itemsRef.on('value', (snapshot) => {
      let data = JSON.stringify(snapshot.val());
      let json = JSON.parse(data)
      console.log(json)

      // for (let i = 0; i < json.length; i++) {
      //   console.log(json[i].name)
      //   categories.push(json[i].name)
      // }
      // fn(categories);
      fn(json);
    });
  },

  getOrders: (fn) => {
    console.log('Getting all orders...')
    let itemsRef = db.ref('/Orders');
    var orders = []

    itemsRef.on('value', (snapshot) => {
      let data = JSON.stringify(snapshot.val());
      let json = JSON.parse(data)

      // console.log(json)

      for (let i = 0; i < json.length; i++) {

        var newOrder = {
          dateCreated: Date,
          dateModified: Date,
          total: Float32Array,
          status: String,
          shippingValue: Float32Array,
          shippingCity: String,
          zipCode: String,
          products: [],
        }

        newOrder.dateCreated = json[i].date_created;
        newOrder.dateModified = json[i].date_modified;
        newOrder.total = json[i].total;
        newOrder.status = json[i].status;
        newOrder.shippingValue = json[i].shipping.total;
        newOrder.shippingCity = json[i].shipping.city;
        newOrder.zipCode = json[i].shipping.postcode;
        newOrder.products = json[i].line_items;

        console.log(newOrder)

        orders.push(newOrder);
      }

      fn(orders);
    });
  },

  login: (email, password, fn) => {
    console.log('Firebase login')
    var self = this
    var customer = {token:''}
    Firebase.auth().signInWithEmailAndPassword(email, password)
    .then(function(credentials){
      // console.log(credentials['stsTokenManager'])
      // let data = JSON.stringify(credentials.val());
      let data = JSON.stringify(credentials);
      let json = JSON.parse(data)
      
      for (const item in json) {
        console.log(item)
        customer = json[item]
        customer['token'] = item
        fn(customer)
      }
      // console.log(json['stsTokenManager'])
      // self.getUser(email, function(customer){
      //   console.log('then')
      //   fn(customer)
      // })
    })
    .catch(function(err) {
      // Handle errors
      console.log(err)
    });

  },
  
  logout: (email, password, fn) => {
    console.log('Firebase logout')
    Firebase.auth().signOut()
    .catch(function (err) {
      // Handle errors
      console.log(err)
    });

    fn('deu ok')
  },
  
  signUp: (email, password, username, firstName, lastName, fn) => {
    console.log('Firebase: Signing up '+ email);

    var res = {
      message: undefined,
      success1: false,
      success2: false,
    }

    let newUser = {
      'email': email,
      'username': username,
      'firstName': firstName,
      'lastName': lastName,
    }
    // Create a user with email and password Firebase Auth, and store the rest of the info in a table called UsersInfo
    let usersRef = db.ref('/UsersInfo');
    usersRef.push(newUser)
    .then(function(){
      console.log('Added user to database')
      res.success1 = true;
    })
    .catch(function (err) {
      // Handle errors
      console.log(err)
      res.message = err
      fn(res)
    })
    
    // Register a new user
    Firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(function(){
      console.log('Added user to auth')
      res.success2 = true;
      res.message = Languages.successSignUp;
      fn(res)
    })
    .catch(function (err) {
      // Handle errors
      res.message = err
      fn(res)
    });
    
  },

  getUser: (email, fn) => {
    console.log('getUser(): ' + email)
    let usersRef = db.ref('/UsersInfo');

    usersRef.orderByChild("email").equalTo(email).on("value", (snapshot) => {
      var customer = {}

      let data = JSON.stringify(snapshot.val());
      let json = JSON.parse(data)

      for (const item in json) {
        customer = json[item]
        customer['key'] = item
      }

      console.log(customer);

      fn(customer)
    });
  },

  getPaymentMethods: (fn) => {
    console.log('getPaymentMethods(): ')
    let itemsRef = db.ref('/PaymentMethods');

    itemsRef.on('value', (snapshot) => {
      let data = JSON.stringify(snapshot.val());
      let json = JSON.parse(data)
      console.log(json)
      fn(json);
    });
  },

  getShippingMethods:(fn) => {
    console.log('getShippingMethods(): ')
    let itemsRef = db.ref('/ShippingMethods');

    itemsRef.on('value', (snapshot) => {
      let data = JSON.stringify(snapshot.val());
      let json = JSON.parse(data)
      console.log(json)
      
      fn(json);
    });
  }

};

export default FirebaseReq;
