import Moment from 'moment';
// import FirebaseReq from "../services/FirebaseReq";
// import { networkInterfaces } from 'os';

const FirebaseQuery = {
  getDayOrders: (orders) => {

    console.log(orders)
    console.log('getDayOrders()')

    // Get orders from the current day -> change graphic to line
    var filteredOrders = []
    var totalValue = 0

    var filteredOrder = {
      dateCreated: Date,
      dateModified: Date,
      total: Number,
      status: String,
      shippingValue: Float32Array,
      shippingCity: String,
      zipCode: String,
    }

    // Get current day date
    Moment.locale('en');
    // var currentDate = Moment().format('DD MM YYYY');
    var currentDate = '22 06 2018'

    for (let i = 0; i < orders.length; i++) {
      let dateFormated = Moment(orders[i].dateCreated).format('DD MM YYYY');
      // console.log('order date: ' + dateFormated);

      if (currentDate == dateFormated) {
        // filteredOrder.dateCreated = orders[i].dateCreated;
        // filteredOrder.dateModified = orders[i].dateModified;
        filteredOrder.total = orders[i].total;
        // filteredOrder.status = orders[i].status;
        // filteredOrder.shippingValue = orders[i].shippingValue;
        // filteredOrder.shippingCity = orders[i].shippingCity;
        // filteredOrder.zipCode = orders[i].zipCode;

        // filteredOrders.push(filteredOrder);
        totalValue = totalValue + parseFloat(orders[i].total)
      }
    }

    // return filteredOrders
    return totalValue
  },

  getDayOrdersByPeriod: (orders) => {
    // console.log(orders)
    console.log('getDayOrdersByPeriod()')

    // Get orders from the current day -> change graphic to line
    var filteredOrders = {
      firstPeriod: 0,
      secondPeriod: 0,
      thirdPeriod: 0,
      fourthPeriod: 0,
      fifthPeriod: 0,
      sixthPeriod: 0,
      seventhPeriod: 0,
      eightPeriod: 0,
    }
    // var totalValue = 0

    // var filteredOrder = {
    //   dateCreated: Date,
    //   dateModified: Date,
    //   total: Number,
    //   status: String,
    //   shippingValue: Float32Array,
    //   shippingCity: String,
    //   zipCode: String,
    // }

    // Get current day date
    Moment.locale('en');
    // var currentDate = Moment().format('DD MM YYYY');
    var currentDate = '2018-06-22'

    for (let i = 0; i < orders.length; i++) {
      let dateFormated = Moment(orders[i].dateCreated).format('YYYY-MM-DD');
      let hour = parseInt(Moment(orders[i].dateCreated).format('h'));
      // let minute = parseInt(Moment(orders[i].dateCreated).format('mm'));
      // let seconds = parseInt(Moment(orders[i].dateCreated).format('ss'));
      // console.log('order date: ' + dateFormated + hour);

      if (currentDate == dateFormated) {
        // filteredOrder.dateCreated = orders[i].dateCreated;
        // filteredOrder.dateModified = orders[i].dateModified;
        // filteredOrder.total = orders[i].total;
        // filteredOrder.status = orders[i].status;
        // filteredOrder.shippingValue = orders[i].shippingValue;
        // filteredOrder.shippingCity = orders[i].shippingCity;
        // filteredOrder.zipCode = orders[i].zipCode;

        if (hour >= 0 && hour <= 6) {
          filteredOrders.firstPeriod += parseFloat(orders[i].total)
          console.log('First period: ' + hour + 'h ' + filteredOrders.firstPeriod)
        } else if (hour > 6 && hour <= 8) {
          filteredOrders.secondPeriod += parseFloat(orders[i].total)
          console.log('Second period: ' + hour + 'h ' + filteredOrders.secondPeriod)
        } else if (hour > 8 && hour <= 10) {
          filteredOrders.thirdPeriod += parseFloat(orders[i].total)
          console.log('Third period: ' + hour + 'h ' + filteredOrders.thirdPeriod)
        } else if (hour > 10 && hour <= 12) {
          filteredOrders.fourthPeriod += parseFloat(orders[i].total)
          console.log('Fourth period: ' + hour + 'h ' + filteredOrders.fourthPeriod)
        } else if (hour > 12 && hour <= 14) {
          filteredOrders.fifthPeriod += parseFloat(orders[i].total)
          console.log('fifth period: ' + hour + 'h ' + filteredOrders.fifthPeriod)
        } else if (hour > 14 && hour <= 16) {
          filteredOrders.sixthPeriod += parseFloat(orders[i].total)
          console.log('Sixth period: ' + hour + 'h ' + filteredOrders.sixthPeriod)
        } else if (hour > 14 && hour <= 18) {
          filteredOrders.sixthPeriod += parseFloat(orders[i].total)
          console.log('Sixth period: ' + hour + 'h ' + filteredOrders.sixthPeriod)
        } else if (hour > 18 && hour <= 23) {
          filteredOrders.sixthPeriod += parseFloat(orders[i].total)
          console.log('Sixth period: ' + hour + 'h ' + filteredOrders.sixthPeriod)
        }

        // filteredOrders.push(filteredOrder);
        // totalValue = totalValue + parseFloat(orders[i].total)
      }
    }

    return filteredOrders
    // return totalValue
  },

  getWeekOrders: (orders) => {
    // Get days of the current week
    var filteredOrders = []

    for (let i = 0; i < orders.length; i++) {
      var filteredOrder = {
        dateCreated: Date,
        dateModified: Date,
        total: Float32Array,
        status: String,
        shippingValue: Float32Array,
        shippingCity: String,
        zipCode: String,
      }

      filteredOrder.dateCreated = orders[i].dateCreated;
      filteredOrder.dateModified = orders[i].dateModified;
      filteredOrder.total = orders[i].total;
      filteredOrder.status = orders[i].status;
      filteredOrder.shippingValue = orders[i].shippingValue;
      filteredOrder.shippingCity = orders[i].shippingCity;
      filteredOrder.zipCode = orders[i].zipCode;


      filteredOrders.push(filteredOrder);
    }

    return filteredOrders
  },

  getWeekOrdersByPeriod: (orders) => {
    console.log('getWeekOrdersByPeriod()')
    // Get days of the current week
    var filteredOrders = {
      dom: 0.0,
      seg: 0.0,
      ter: 0.0,
      qua: 0.0,
      qui: 0.0,
      sex: 0.0,
      sab: 0.0,
    }

    // Get current day date
    Moment.locale('en');
    // var currentDate = Moment().format('DD MM YYYY');
    var dayOfWeek = ''
    var currentDate = '2018-06-22'
    var weekDays = getWeekDays(currentDate)

    for (let i = 0; i < orders.length; i++) {

      let dateFormatted = Moment(orders[i].dateCreated).format('YYYY-MM-DD');
      // let day = parseInt(Moment(orders[i].dateCreated).format('DD'));

      // var filteredOrder = {
      //   dateCreated: Date,
      //   dateModified: Date,
      //   total: Float32Array,
      //   status: String,
      //   shippingValue: Float32Array,
      //   shippingCity: String,
      //   zipCode: String,
      // }

      // if (currentDate == dateFormated) {
      if (weekDays.find(function (elem) { return elem == dateFormatted })) {
        dayOfWeek = Moment(dateFormatted).weekday()
        console.log('Week Day:' + dayOfWeek + 'Date formatted: ' + dateFormatted)
        // filteredOrder.dateCreated = orders[i].dateCreated;
        // filteredOrder.dateModified = orders[i].dateModified;
        // filteredOrder.total = orders[i].total;
        // filteredOrder.status = orders[i].status;
        // filteredOrder.shippingValue = orders[i].shippingValue;
        // filteredOrder.shippingCity = orders[i].shippingCity;
        // filteredOrder.zipCode = orders[i].zipCode;

        if (dayOfWeek == 0) {
          filteredOrders.dom += parseFloat(orders[i].total)
          // console.log('First period: ' + filteredOrders.dom)
        } else if (dayOfWeek == 1) {
          filteredOrders.seg += parseFloat(orders[i].total)
          // console.log('Second period: ' + filteredOrders.seg)
        } else if (dayOfWeek == 2) {
          filteredOrders.ter += parseFloat(orders[i].total)
          // console.log('Third period: ' + filteredOrders.ter)
        } else if (dayOfWeek == 3) {
          filteredOrders.qua += parseFloat(orders[i].total)
          // console.log('Fourth period: ' + filteredOrders.qua)
        } else if (dayOfWeek == 4) {
          filteredOrders.qui += parseFloat(orders[i].total)
          // console.log('fifth period: ' + filteredOrders.qui)
        } else if (dayOfWeek == 5) {
          filteredOrders.sex += parseFloat(orders[i].total)
          // console.log('Sixth period: ' + filteredOrders.sex)
        } else if (dayOfWeek == 6) {
          filteredOrders.sab += parseFloat(orders[i].total)
          // console.log('Sixth period: ' + filteredOrders.sab)
        }
        // filteredOrders.push(filteredOrder);
        // filteredOrders.push(filteredOrder);
        // totalValue = totalValue + parseFloat(orders[i].total)
      }
    }

    return filteredOrders
  },


  getMonthOrders: (orders) => {
    // Get days of the current week
    var filteredOrders = []

    // Current time
    var currentDate = Moment().format('MM YYYY');

    for (let i = 0; i < orders.length; i++) {
      let dateFormated = Moment(orders[i].dateCreated).format('MM YYYY');

      var filteredOrder = {
        dateCreated: Date,
        dateModified: Date,
        total: Float32Array,
        status: String,
        shippingValue: Float32Array,
        shippingCity: String,
        zipCode: String,
      }

      if (currentDate == dateFormated) {
        filteredOrder.dateCreated = orders[i].dateCreated;
        filteredOrder.dateModified = orders[i].dateModified;
        filteredOrder.total = orders[i].total;
        filteredOrder.status = orders[i].status;
        filteredOrder.shippingValue = orders[i].shippingValue;
        filteredOrder.shippingCity = orders[i].shippingCity;
        filteredOrder.zipCode = orders[i].zipCode;

        filteredOrders.push(filteredOrder);
      }
    }

    return filteredOrders
  },

  getMonthOrdersByPeriod: (orders) => {
    console.log('getMonthOrdersByPeriod()')
    // Get days of the current week
    // var filteredOrders = []
    var filteredOrders = Array.apply(null, Array(31)).map(Number.prototype.valueOf, 0);

    // Current time
    // var currentDate = Moment().format('MM-YYYY');
    var currentDate = '06-2018'

    for (let i = 0; i < orders.length; i++) {
      let dateFormated = Moment(orders[i].dateCreated).format('MM-YYYY');
      let day = Moment(orders[i].dateCreated).format('DD');

      var filteredOrder = {
        dateCreated: Date,
        dateModified: Date,
        total: Float32Array,
        status: String,
        shippingValue: Float32Array,
        shippingCity: String,
        zipCode: String,
      }

      if (currentDate == dateFormated) {

        // filteredOrder.total = orders[i].total;
        switch (parseInt(day)) {
          case 1:
            filteredOrders[0] += parseFloat(orders[i].total);
            // console.log('Day 1: ' + day + ' ' + filteredOrders[0])
          case 2:
            filteredOrders[1] += parseFloat(orders[i].total);
            // console.log('Day 2: ' + day + ' ' + filteredOrders[0])
          case 3:
            filteredOrders[2] += parseFloat(orders[i].total);
            // console.log('Day 3: ' + day + ' ' + filteredOrders[0])
          case 4:
            filteredOrders[3] += parseFloat(orders[i].total);
            // console.log('Day 4: ' + day + ' ' + filteredOrders[0])
          case 5:
            filteredOrders[4] += parseFloat(orders[i].total);
            // console.log('Day 5: ' + day + ' ' + filteredOrders[0])
          case 6:
            filteredOrders[5] += parseFloat(orders[i].total);
            // console.log('Day 6: ' + day + ' ' + filteredOrders[0])
          case 7:
            filteredOrders[6] += parseFloat(orders[i].total);
            // console.log('Day 7: ' + day + ' ' + filteredOrders[0])
          case 8:
            filteredOrders[7] += parseFloat(orders[i].total);
            // console.log('Day 8: ' + day + ' ' + filteredOrders[0])
          case 9:
            filteredOrders[8] += parseFloat(orders[i].total);
            // console.log('Day 9: ' + day + ' ' + filteredOrders[0])
          case 10:
            filteredOrders[9] += parseFloat(orders[i].total);
            // console.log('Day 10: ' + day + ' ' + filteredOrders[0])
          case 11:
            filteredOrders[10] += parseFloat(orders[i].total);
            // console.log('Day 11: ' + day + ' ' + filteredOrders[0])
          case 12:
            filteredOrders[11] += parseFloat(orders[i].total);
            // console.log('Day 12: ' + day + ' ' + filteredOrders[0])
          case 13:
            filteredOrders[12] += parseFloat(orders[i].total);
            // console.log('Day 13: ' + day + ' ' + filteredOrders[0])
          case 14:
            filteredOrders[13] += parseFloat(orders[i].total);
            // console.log('Day 14: ' + day + ' ' + filteredOrders[0])
          case 15:
            filteredOrders[14] += parseFloat(orders[i].total);
            // console.log('Day 15: ' + day + ' ' + filteredOrders[0])
          case 16:
            filteredOrders[15] += parseFloat(orders[i].total);
            // console.log('Day 16: ' + day + ' ' + filteredOrders[0])
          case 17:
            filteredOrders[16] += parseFloat(orders[i].total);
            // console.log('Day 17: ' + day + ' ' + filteredOrders[0])
          case 18:
            filteredOrders[17] += parseFloat(orders[i].total);
            // console.log('Day 18: ' + day + ' ' + filteredOrders[0])
          case 19:
            filteredOrders[18] += parseFloat(orders[i].total);
            // console.log('Day 19: ' + day + ' ' + filteredOrders[0])
          case 20:
            filteredOrders[19] += parseFloat(orders[i].total);
            // console.log('Day 20: ' + day + ' ' + filteredOrders[0])
          case 21:
            filteredOrders[20] += parseFloat(orders[i].total);
            // console.log('Day 21: ' + day + ' ' + filteredOrders[0])
          case 22:
            filteredOrders[21] += parseFloat(orders[i].total);
            // console.log('Day 22: ' + day + ' ' + filteredOrders[0])
          case 23:
            filteredOrders[22] += parseFloat(orders[i].total);
            // console.log('Day 23: ' + day + ' ' + filteredOrders[0])
          case 24:
            filteredOrders[23] += parseFloat(orders[i].total);
            // console.log('Day 24: ' + day + ' ' + filteredOrders[0])
          case 25:
            filteredOrders[24] += parseFloat(orders[i].total);
            // console.log('Day 25: ' + day + ' ' + filteredOrders[0])
          case 26:
            filteredOrders[25] += parseFloat(orders[i].total);
            // console.log('Day 26: ' + day + ' ' + filteredOrders[0])
          case 27:
            filteredOrders[26] += parseFloat(orders[i].total);
            // console.log('Day 27: ' + day + ' ' + filteredOrders[0])
          case 28:
            filteredOrders[27] += parseFloat(orders[i].total);
            // console.log('Day 28: ' + day + ' ' + filteredOrders[0])
          case 29:
            filteredOrders[28] += parseFloat(orders[i].total);
            // console.log('Day 29: ' + day + ' ' + filteredOrders[0])
          case 30:
            filteredOrders[29] += parseFloat(orders[i].total);
            // console.log('Day 30: ' + day + ' ' + filteredOrders[0])
          case 31:
            filteredOrders[30] += parseFloat(orders[i].total);
            // console.log('Day 31: ' + day + ' ' + filteredOrders[0])
        }

        // filteredOrder.dateCreated = orders[i].dateCreated;
        // filteredOrder.dateModified = orders[i].dateModified;
        // filteredOrder.total = orders[i].total;
        // filteredOrder.status = orders[i].status;
        // filteredOrder.shippingValue = orders[i].shippingValue;
        // filteredOrder.shippingCity = orders[i].shippingCity;
        // filteredOrder.zipCode = orders[i].zipCode;

        // filteredOrders.push(filteredOrder);
      }
    }

    return filteredOrders
  },

  getBestSellers(orders) {
    console.log('getBestSellers()')

    // Current time
    // var currentDate = Moment().format('MM-YYYY');
    var currentDate = '06-2018'

    var bestSellers = [{
      name: String,
      count: 0,
    }]

    var products = []

    // Gets the list of products
    for (let i = 0; i < orders.length; i++) {
      let dateFormated = Moment(orders[i].dateCreated).format('MM-YYYY');

      if (dateFormated == currentDate) {
        for (let j = 0; j < orders[i].products.length; j++) {
          const element = orders[i].products[j];
          products.push(element.name)
          // console.log(element.name)
        }
      }
    }

    // console.log('List of Products: \n' + products)

    // Counts the frequency
    for (let index = 0; index < products.length; index++) {
      const product = products[index];
      //if it wasnt a product added yet
      if (!bestSellers.find(function (elem) { return elem == product })) {
        // console.log("CHECK: " + product)
        bestSellers.push({ name: product, count: 1 })
      } else {
        for (var elem in bestSellers) {
          if (bestSellers[elem].name == prod) {
            bestSellers[elem].count++
          }
        }
      }
    }

    // console.log('Best Sellers: \n' + bestSellers)

    return bestSellers;
  },

  getProductsByCategory(products, id) {
    console.log('getProductsByCategory')
    var filteredProducts = []
    for (let i = 0; i < products.length; i++) {
      let cat = products[i].categories
      // console.log(cat)
      for (let j = 0; j < cat.length; j++) {
        // console
        if (cat[j]['id'] == id) {
          filteredProducts.push(products[i])
          console.log('Produto adicionado')
        }
      }
    }
    console.log(filteredProducts)
    console.log('tamanho: '+filteredProducts.length)
    return filteredProducts;
  }
}

export default FirebaseQuery;

function getWeekDays(currentDate) {
  var weekDays = []

  var startOfWeek = Moment(currentDate).startOf('week');
  var endOfWeek = Moment().endOf('week');
  var day = startOfWeek;
  var newDay = ''
  while (day <= endOfWeek) {
    newDay = day.toDate()
    weekDays.push(Moment(newDay).format('YYYY-MM-DD'));
    day = day.clone().add(1, 'd');
  }

  return weekDays = weekDays.slice(0, 7)
}