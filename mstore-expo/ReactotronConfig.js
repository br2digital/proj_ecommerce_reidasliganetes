import Reactotron from 'reactotron-react-native'
import { reactotronRedux as reduxPlugin } from 'reactotron-redux'

console.disableYellowBox = true
console.ignoredYellowBox = ['Warning: `flexWrap: `wrap``'];

console.log('Reactotron Connect 2');

Reactotron.configure({name: 'MStore'})
Reactotron.configure({host: 'exp://localhost:19000'})

// Reactotron.configure({lan: 'exp://172.22.69.18:19000'}).useReactNative().connect();
// Reactotron.configure({host: 'exp://localhost:19000'}).useReactNative().connect();

Reactotron.useReactNative({
    asyncStorage: { ignore: ['secret'] }
})

Reactotron.use(reduxPlugin())

console.tron = Reactotron

// if (__DEV__) {
//     console.log('Reactotron Connect 1');
//     Reactotron.connect();
//     Reactotron.clear();
// }