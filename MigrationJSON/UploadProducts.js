var firebase = require("firebase");

var config = {
  apiKey: "AIzaSyAwW45-mnAtLFQSInya2wd3ZLs4K_b_ZO8",
  authDomain: "teste-reidasliganetes.firebaseapp.com",
  databaseURL: "https://teste-reidasliganetes.firebaseio.com",
  projectId: "teste-reidasliganetes",
  storageBucket: "teste-reidasliganetes.appspot.com",
  messagingSenderId: "341314931155"
};
var app = firebase.initializeApp(config);
var database = app.database();

database.ref('Products/').set(
    [
      {
        "id": 4061,
        "name": "Macaquito Liganete REF:MGIL12 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refmgil12-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refmgil12-lote-10unid-r740-cada\/",
        "date_created": "2017-11-14T00:40:01",
        "date_created_gmt": "2017-11-14T00:40:01",
        "date_modified": "2018-07-18T07:58:30",
        "date_modified_gmt": "2018-07-18T07:58:30",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Macaquito meia al\u00e7a short saia estampado, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "MGIL12",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 0,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1030,
          967,
          966,
          964,
          965
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4062,
            "date_created": "2017-11-14T00:38:25",
            "date_created_gmt": "2017-11-14T03:38:25",
            "date_modified": "2017-11-14T00:38:25",
            "date_modified_gmt": "2017-11-14T03:38:25",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6699.jpg",
            "name": "MACAQUITO-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4064,
            "date_created": "2017-11-14T00:39:08",
            "date_created_gmt": "2017-11-14T03:39:08",
            "date_modified": "2017-11-14T00:39:08",
            "date_modified_gmt": "2017-11-14T03:39:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6702.jpg",
            "name": "_SGA6702",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 240,
        "meta_data": [
          {
            "id": 13099,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 13100,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 13106,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 13107,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 13108,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 13137,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 13138,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 13139,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 13140,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 13141,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 13142,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 13143,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4061"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4056,
        "name": "Camisa Manga Longa Masculina REF: Camisamangalongaadulto <br \/>(lote 10 UN)<div id=\"destaque\">R$8,20 UN<\/div>",
        "slug": "camisa-manga-longa-masculina-ref-camisamangalongaadulto-lote-10-unid-r820-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/camisa-manga-longa-masculina-ref-camisamangalongaadulto-lote-10-unid-r820-cada\/",
        "date_created": "2017-11-14T00:38:18",
        "date_created_gmt": "2017-11-14T00:38:18",
        "date_modified": "2018-07-18T07:58:30",
        "date_modified_gmt": "2018-07-18T07:58:30",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Camisa gola careca manga longa lisa, malha PV .<\/p>\n<p>Composi\u00e7\u00e3o: 100% poliester<\/p>\n<p>Tamanho: P\/M\/G<\/p>\n<p>&nbsp;<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "Camisamangalongaadulto",
        "price": "82.00",
        "regular_price": "82.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>82,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 17,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          4063,
          3286,
          1300,
          1288,
          1322
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 42,
            "name": "Camisa de manga longa",
            "slug": "camisa-de-manga-longa"
          },
          {
            "id": 40,
            "name": "Camisas",
            "slug": "camisa"
          },
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4060,
            "date_created": "2017-11-14T00:36:58",
            "date_created_gmt": "2017-11-14T03:36:58",
            "date_modified": "2017-11-14T00:36:58",
            "date_modified_gmt": "2017-11-14T03:36:58",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/CG7_0769.jpg",
            "name": "CAMISA-MANGA-LONGA-ATACADO-BARATA",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 109,
        "meta_data": [
          {
            "id": 13009,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 13010,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 13016,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 13017,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 13018,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 13047,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 13048,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 13049,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 13050,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 13051,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 13052,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 13053,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4056"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4057,
        "name": "Macaquito Liganete REF:MGIL11 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refmgil11-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refmgil11-lote-10unid-r740-cada\/",
        "date_created": "2017-11-14T00:36:26",
        "date_created_gmt": "2017-11-14T00:36:26",
        "date_modified": "2018-07-18T07:58:30",
        "date_modified_gmt": "2018-07-18T07:58:30",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Macaquito de al\u00e7a short saia estampada, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideal para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "MGIL11",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 0,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1040,
          970,
          1039,
          968,
          1033
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4058,
            "date_created": "2017-11-14T00:34:37",
            "date_created_gmt": "2017-11-14T03:34:37",
            "date_modified": "2017-11-14T00:34:37",
            "date_modified_gmt": "2017-11-14T03:34:37",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6696.jpg",
            "name": "MACAQUITO-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4059,
            "date_created": "2017-11-14T00:35:08",
            "date_created_gmt": "2017-11-14T03:35:08",
            "date_modified": "2017-11-14T00:35:08",
            "date_modified_gmt": "2017-11-14T03:35:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6698.jpg",
            "name": "_SGA6698",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 239,
        "meta_data": [
          {
            "id": 13054,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 13055,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 13061,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 13062,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 13063,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 13092,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 13093,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 13094,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 13095,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 13096,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 13097,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 13098,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4057"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4051,
        "name": "Cal\u00e7a Tactel Masculina REF: 2001 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,30 UN<\/div>",
        "slug": "calca-tactel-masculina-ref-2001-lote-10-unid-r930-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/calca-tactel-masculina-ref-2001-lote-10-unid-r930-cada\/",
        "date_created": "2017-11-14T00:32:38",
        "date_created_gmt": "2017-11-14T00:32:38",
        "date_modified": "2018-07-18T07:58:30",
        "date_modified_gmt": "2018-07-18T07:58:30",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Cal\u00e7a masculina, tecido tactel.<\/p>\n<p>Composi\u00e7\u00e3o: 100% poliester.<\/p>\n<p>Tamanho: P\/M\/G<\/p>\n<p>&nbsp;<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "2001",
        "price": "93.00",
        "regular_price": "93.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>93,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 23,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.900",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          4046,
          1288,
          1335,
          1322,
          4093
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 54,
            "name": "Cal\u00e7a tactel",
            "slug": "calca-tactel"
          },
          {
            "id": 58,
            "name": "Cal\u00e7as",
            "slug": "calcas-masculino"
          },
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4054,
            "date_created": "2017-11-14T00:31:21",
            "date_created_gmt": "2017-11-14T03:31:21",
            "date_modified": "2017-11-14T00:31:21",
            "date_modified_gmt": "2017-11-14T03:31:21",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/CG7_0829.jpg",
            "name": "CAL\u00c7A-TACTEL-BARATA-ATACADO",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 75,
        "meta_data": [
          {
            "id": 12964,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12965,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12971,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12972,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12973,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 13002,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 13003,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 13004,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 13005,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 13006,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 13007,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 13008,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4051"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4047,
        "name": "Macaquito Liganete REF:M23 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm23-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm23-lote-10unid-r740-cada\/",
        "date_created": "2017-11-14T00:28:47",
        "date_created_gmt": "2017-11-14T00:28:47",
        "date_modified": "2018-05-27T18:27:24",
        "date_modified_gmt": "2018-05-27T18:27:24",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Macaquito meia al\u00e7a estampado, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "M23",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 3,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          980,
          915,
          1028,
          979,
          965
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4052,
            "date_created": "2017-11-14T00:30:05",
            "date_created_gmt": "2017-11-14T03:30:05",
            "date_modified": "2017-11-14T00:30:05",
            "date_modified_gmt": "2017-11-14T03:30:05",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6630-1.jpg",
            "name": "MACAQUITO-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4055,
            "date_created": "2017-11-14T00:32:03",
            "date_created_gmt": "2017-11-14T03:32:03",
            "date_modified": "2017-11-14T00:32:03",
            "date_modified_gmt": "2017-11-14T03:32:03",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6632-1.jpg",
            "name": "_SGA6632",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 229,
        "meta_data": [
          {
            "id": 12919,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12920,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12926,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12927,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12928,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12957,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12958,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12959,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12960,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12961,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12962,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12963,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4047"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4046,
        "name": "Cal\u00e7a Cacharrel Masculina REF: 2002 <br \/>(lote 10 UN)<div id=\"destaque\">R$10,50 UN<\/div>",
        "slug": "calca-cacharrel-masculina-ref-2002-lote-10-unid-r1050-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/calca-cacharrel-masculina-ref-2002-lote-10-unid-r1050-cada\/",
        "date_created": "2017-11-14T00:28:43",
        "date_created_gmt": "2017-11-14T00:28:43",
        "date_modified": "2018-05-27T23:04:21",
        "date_modified_gmt": "2018-05-27T23:04:21",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Cal\u00e7a masculina, tecido cacharrel.<\/p>\n<p>Composi\u00e7\u00e3o: 100% poliester.<\/p>\n<p>Tamanho: P\/M\/G<\/p>\n<p>&nbsp;<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "2002",
        "price": "105.50",
        "regular_price": "105.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>105,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 13,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.000",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          4125,
          4093,
          1301,
          4063,
          1288
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 58,
            "name": "Cal\u00e7as",
            "slug": "calcas-masculino"
          },
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4049,
            "date_created": "2017-11-14T00:27:40",
            "date_created_gmt": "2017-11-14T03:27:40",
            "date_modified": "2017-11-14T00:27:40",
            "date_modified_gmt": "2017-11-14T03:27:40",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/CG7_0825.jpg",
            "name": "CAL\u00c7A-CACHARREL-BARATA-ATACADO",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 70,
        "meta_data": [
          {
            "id": 12874,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12875,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12881,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12882,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12883,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12912,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12913,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12914,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12915,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12916,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12917,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12918,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4046"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4011,
        "name": "Bermuda Jeans Masculina REF: 2104 (lote 10 UN) <div id=\"destaque\">R$12,50 UN<\/div>",
        "slug": "bermuda-jeans-masculina-ref-2104-lote-10-unid-r1250-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/bermuda-jeans-masculina-ref-2104-lote-10-unid-r1250-cada\/",
        "date_created": "2017-11-14T00:23:15",
        "date_created_gmt": "2017-11-14T00:23:15",
        "date_modified": "2018-08-08T05:42:30",
        "date_modified_gmt": "2018-08-08T05:42:30",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Bermuda jeans masculina. Composi\u00e7\u00e3o: 100% poliester Tamanho: 36 ao 44<\/p>\n",
        "short_description": "<p>Bermuda jeans masculina. Composi\u00e7\u00e3o: 100% poliester Tamanho: 36 ao 44<\/p>\n",
        "sku": "2104",
        "price": "125.00",
        "regular_price": "125.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>125,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 46,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": true,
        "stock_quantity": 2,
        "in_stock": true,
        "backorders": "notify",
        "backorders_allowed": true,
        "backordered": false,
        "sold_individually": false,
        "weight": "3.200",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1301,
          1335,
          4056,
          4125,
          4051
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          },
          {
            "id": 56,
            "name": "Short Jeans",
            "slug": "short-jeans"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4041,
            "date_created": "2017-11-14T00:17:45",
            "date_created_gmt": "2017-11-14T03:17:45",
            "date_modified": "2017-11-14T00:17:45",
            "date_modified_gmt": "2017-11-14T03:17:45",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/CG7_0805.jpg",
            "name": "BERMUDA-JEANS-BARATA-ATACADO",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 5,
        "meta_data": [
          {
            "id": 12470,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12476,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12477,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12478,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12507,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12508,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12509,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12510,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12511,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": ""
          },
          {
            "id": 12512,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12513,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 227939,
            "key": "_gtm_vars",
            "value": []
          },
          {
            "id": 232329,
            "key": "min_quantity",
            "value": ""
          },
          {
            "id": 232330,
            "key": "max_quantity",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4011"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4042,
        "name": "Macaquito Liganete REF:M22 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm22-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm22-lote-10unid-r740-cada\/",
        "date_created": "2017-11-14T00:20:44",
        "date_created_gmt": "2017-11-14T00:20:44",
        "date_modified": "2018-05-27T18:28:59",
        "date_modified_gmt": "2018-05-27T18:28:59",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Macaquito com manga 3\/4 estampado, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "M22",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 7,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1033,
          975,
          1038,
          972,
          965
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4043,
            "date_created": "2017-11-14T00:19:39",
            "date_created_gmt": "2017-11-14T03:19:39",
            "date_modified": "2017-11-14T00:19:39",
            "date_modified_gmt": "2017-11-14T03:19:39",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6623.jpg",
            "name": "MACAQUITO-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4044,
            "date_created": "2017-11-14T00:21:58",
            "date_created_gmt": "2017-11-14T03:21:58",
            "date_modified": "2017-11-14T00:21:58",
            "date_modified_gmt": "2017-11-14T03:21:58",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6625.jpg",
            "name": "_SGA6625",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 228,
        "meta_data": [
          {
            "id": 12829,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12830,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12836,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12837,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12838,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12867,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12868,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12869,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12870,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12871,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12872,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12873,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4042"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4038,
        "name": "Conjunto Liganete REF:CONJ1 <br \/>(lote 10 UN)<div id=\"destaque\">R$10,99 UN<\/div>",
        "slug": "conjunto-liganete-refconj1-lote-10unid-r1099-cada-2",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/conjunto-liganete-refconj1-lote-10unid-r1099-cada-2\/",
        "date_created": "2017-11-14T00:15:55",
        "date_created_gmt": "2017-11-14T00:15:55",
        "date_modified": "2018-05-27T18:29:35",
        "date_modified_gmt": "2018-05-27T18:29:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Conjunto cropped com manga e saia longa em malha liganete, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "CONJ2",
        "price": "109.90",
        "regular_price": "109.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>109,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 8,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.400",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          972,
          969,
          915,
          1033,
          978
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 57,
            "name": "conjunto",
            "slug": "conjunto-feminino"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4039,
            "date_created": "2017-11-14T00:14:07",
            "date_created_gmt": "2017-11-14T03:14:07",
            "date_modified": "2017-11-14T00:14:07",
            "date_modified_gmt": "2017-11-14T03:14:07",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/cacgarrel.jpg",
            "name": "CONJUNTO-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4040,
            "date_created": "2017-11-14T00:14:47",
            "date_created_gmt": "2017-11-14T03:14:47",
            "date_modified": "2017-11-14T00:14:47",
            "date_modified_gmt": "2017-11-14T03:14:47",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6797.jpg",
            "name": "_SGA6797",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 181,
        "meta_data": [
          {
            "id": 12784,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12785,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12791,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12792,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12793,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12822,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12823,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12824,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12825,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12826,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12827,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12828,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4038"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4034,
        "name": "Conjunto Liganete REF:CONJ1 <br \/>(lote 10 UN)<div id=\"destaque\">R$10,99 UN<\/div>",
        "slug": "conjunto-liganete-refconj1-lote-10unid-r1099-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/conjunto-liganete-refconj1-lote-10unid-r1099-cada\/",
        "date_created": "2017-11-14T00:11:18",
        "date_created_gmt": "2017-11-14T00:11:18",
        "date_modified": "2018-05-27T18:29:59",
        "date_modified_gmt": "2018-05-27T18:29:59",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Conjunto cropped sem manga com saia longa em malha liganete, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "CONJ1",
        "price": "109.90",
        "regular_price": "109.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>109,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 10,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.400",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1028,
          966,
          969,
          1033,
          974
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 57,
            "name": "conjunto",
            "slug": "conjunto-feminino"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4035,
            "date_created": "2017-11-14T00:09:24",
            "date_created_gmt": "2017-11-14T03:09:24",
            "date_modified": "2017-11-14T00:09:24",
            "date_modified_gmt": "2017-11-14T03:09:24",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6786.jpg",
            "name": "CONJUNTO-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4036,
            "date_created": "2017-11-14T00:10:04",
            "date_created_gmt": "2017-11-14T03:10:04",
            "date_modified": "2017-11-14T00:10:04",
            "date_modified_gmt": "2017-11-14T03:10:04",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6788.jpg",
            "name": "_SGA6788",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 180,
        "meta_data": [
          {
            "id": 12739,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12740,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12746,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12747,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12748,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12777,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12778,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12779,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12780,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12781,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "103"
          },
          {
            "id": 12782,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12783,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4034"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4031,
        "name": "Blusa Liganete REF:B83 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "blusa-liganete-refb83-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/blusa-liganete-refb83-lote-10unid-r740-cada\/",
        "date_created": "2017-11-14T00:00:27",
        "date_created_gmt": "2017-11-14T00:00:27",
        "date_modified": "2018-07-18T07:56:32",
        "date_modified_gmt": "2018-07-18T07:56:32",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Blusa tomara que caia de manga com babado, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "B83",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 40,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          967,
          1028,
          1039,
          966,
          970
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 26,
            "name": "Blusas",
            "slug": "blusas"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4032,
            "date_created": "2017-11-13T23:57:09",
            "date_created_gmt": "2017-11-14T02:57:09",
            "date_modified": "2017-11-13T23:57:09",
            "date_modified_gmt": "2017-11-14T02:57:09",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6517.jpg",
            "name": "BLUSA-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4033,
            "date_created": "2017-11-13T23:57:53",
            "date_created_gmt": "2017-11-14T02:57:53",
            "date_modified": "2017-11-13T23:57:53",
            "date_modified_gmt": "2017-11-14T02:57:53",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6518.jpg",
            "name": "_SGA6518",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 59,
        "meta_data": [
          {
            "id": 12694,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12695,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12701,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12702,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12703,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12732,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12733,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12734,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12735,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12736,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12737,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12738,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4031"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4027,
        "name": "Blusa Liganete REF:B82 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "blusa-liganete-refb82-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/blusa-liganete-refb82-lote-10unid-r740-cada\/",
        "date_created": "2017-11-13T23:54:28",
        "date_created_gmt": "2017-11-13T23:54:28",
        "date_modified": "2018-07-18T07:56:32",
        "date_modified_gmt": "2018-07-18T07:56:32",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Blusa al\u00e7a, detalhe X nas costas, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "B82",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 2,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          975,
          973,
          979,
          980,
          970
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 26,
            "name": "Blusas",
            "slug": "blusas"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4029,
            "date_created": "2017-11-13T23:53:00",
            "date_created_gmt": "2017-11-14T02:53:00",
            "date_modified": "2017-11-13T23:53:00",
            "date_modified_gmt": "2017-11-14T02:53:00",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6542.jpg",
            "name": "BLUSA-LIGANTE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4030,
            "date_created": "2017-11-13T23:53:35",
            "date_created_gmt": "2017-11-14T02:53:35",
            "date_modified": "2017-11-13T23:53:35",
            "date_modified_gmt": "2017-11-14T02:53:35",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6543.jpg",
            "name": "_SGA6543",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 58,
        "meta_data": [
          {
            "id": 12649,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12650,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12656,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12657,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12658,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12687,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12688,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12689,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12690,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12691,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12692,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12693,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4027"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4022,
        "name": "Blusa Liganete REF:B81 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "blusa-liganete-refb81-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/blusa-liganete-refb81-lote-10unid-r740-cada\/",
        "date_created": "2017-11-13T23:31:53",
        "date_created_gmt": "2017-11-13T23:31:53",
        "date_modified": "2018-07-18T07:56:32",
        "date_modified_gmt": "2018-07-18T07:56:32",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Blusa manga curta, detalhe la\u00e7o na manga, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "B81",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 12,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          968,
          1036,
          975,
          978,
          972
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 26,
            "name": "Blusas",
            "slug": "blusas"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4024,
            "date_created": "2017-11-13T23:29:08",
            "date_created_gmt": "2017-11-14T02:29:08",
            "date_modified": "2017-11-13T23:29:08",
            "date_modified_gmt": "2017-11-14T02:29:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6530.jpg",
            "name": "BLUSA-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4026,
            "date_created": "2017-11-13T23:30:16",
            "date_created_gmt": "2017-11-14T02:30:16",
            "date_modified": "2017-11-13T23:30:16",
            "date_modified_gmt": "2017-11-14T02:30:16",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6546.jpg",
            "name": "_SGA6546",
            "alt": "",
            "position": 1
          },
          {
            "id": 4025,
            "date_created": "2017-11-13T23:30:00",
            "date_created_gmt": "2017-11-14T02:30:00",
            "date_modified": "2017-11-13T23:30:00",
            "date_modified_gmt": "2017-11-14T02:30:00",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6532.jpg",
            "name": "_SGA6532",
            "alt": "",
            "position": 2
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 57,
        "meta_data": [
          {
            "id": 12604,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12605,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12611,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12612,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12613,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12642,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12643,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12644,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12645,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12646,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12647,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12648,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4022"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4017,
        "name": "Blusa Liganete REF:B80 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "blusa-liganete-refb80-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/blusa-liganete-refb80-lote-10unid-r740-cada\/",
        "date_created": "2017-11-13T23:14:56",
        "date_created_gmt": "2017-11-13T23:14:56",
        "date_modified": "2018-07-18T07:56:32",
        "date_modified_gmt": "2018-07-18T07:56:32",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Blusa manga curta, detalhe corte entre a manga e o ombro, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "B80",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 33,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          973,
          1040,
          1036,
          966,
          915
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 26,
            "name": "Blusas",
            "slug": "blusas"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4018,
            "date_created": "2017-11-13T23:08:42",
            "date_created_gmt": "2017-11-14T02:08:42",
            "date_modified": "2017-11-13T23:08:42",
            "date_modified_gmt": "2017-11-14T02:08:42",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6553.jpg",
            "name": "BLUSA-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4020,
            "date_created": "2017-11-13T23:13:35",
            "date_created_gmt": "2017-11-14T02:13:35",
            "date_modified": "2017-11-13T23:13:35",
            "date_modified_gmt": "2017-11-14T02:13:35",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6526.jpg",
            "name": "_SGA6526",
            "alt": "",
            "position": 1
          },
          {
            "id": 4021,
            "date_created": "2017-11-13T23:13:49",
            "date_created_gmt": "2017-11-14T02:13:49",
            "date_modified": "2017-11-13T23:13:49",
            "date_modified_gmt": "2017-11-14T02:13:49",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6556.jpg",
            "name": "_SGA6556",
            "alt": "",
            "position": 2
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 56,
        "meta_data": [
          {
            "id": 12559,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12560,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12566,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12567,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12568,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12597,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12598,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12599,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12600,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12601,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12602,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12603,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4017"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 4012,
        "name": "Blusa Liganete REF:B78  <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "blusa-liganete-refb78-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/blusa-liganete-refb78-lote-10unid-r740-cada\/",
        "date_created": "2017-11-13T23:04:10",
        "date_created_gmt": "2017-11-13T23:04:10",
        "date_modified": "2018-07-18T07:56:32",
        "date_modified_gmt": "2018-07-18T07:56:32",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Blusa manga curta, detalhe na linha do decote, confeccionada com malha liganete proporcionando um \u00f3timo caimento, ideial para uso no dia a dia.<\/p>\n<p>Composi\u00e7\u00e3o: 96% poliester 4% elastano<\/p>\n<p>Tamanho: \u00fanico (M)<\/p>\n<p>O lote ser\u00e1 enviado com sortimento de estampas de acordo com a disponibilidade de estoque.<\/p>\n",
        "short_description": "",
        "sku": "B78",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 4,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1035,
          1036,
          972,
          965,
          1033
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 26,
            "name": "Blusas",
            "slug": "blusas"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 4013,
            "date_created": "2017-11-13T22:49:23",
            "date_created_gmt": "2017-11-14T01:49:23",
            "date_modified": "2017-11-13T22:49:23",
            "date_modified_gmt": "2017-11-14T01:49:23",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6509.jpg",
            "name": "_SGA6509 BLUSA-LIGANETE-ATACADO",
            "alt": "",
            "position": 0
          },
          {
            "id": 4016,
            "date_created": "2017-11-13T22:50:00",
            "date_created_gmt": "2017-11-14T01:50:00",
            "date_modified": "2017-11-13T22:50:00",
            "date_modified_gmt": "2017-11-14T01:50:00",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/11\/SGA6513.jpg",
            "name": "_SGA6513",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 55,
        "meta_data": [
          {
            "id": 12514,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          },
          {
            "id": 12515,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12523,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12524,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12525,
            "key": "fswp_post_meta",
            "value": {
              "disable_in_cash": "0",
              "disable_installments": "0"
            }
          },
          {
            "id": 12526,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12527,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12528,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12529,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12530,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12534,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12535,
            "key": "_product_video_code",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/4012"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3541,
        "name": "Macaquitos \/ Vestidos Liganete Plus Size REF:MVPluslote50 <br \/>(lote 50 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "macaquitos-vestidos-liganete-plus-size-refmvpluslote50-lote-50unid-r949-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquitos-vestidos-liganete-plus-size-refmvpluslote50-lote-50unid-r949-cada\/",
        "date_created": "2017-02-08T15:32:50",
        "date_created_gmt": "2017-02-08T15:32:50",
        "date_modified": "2018-07-18T07:56:33",
        "date_modified_gmt": "2018-07-18T07:56:33",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Lote composto com 50 pe\u00e7as Plus Size, 25 macaquitos\u00a0e 25 vestidos\u00a0que v\u00e3o com modelos, estampas e cores sortidas.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>97% Poliester<\/p>\n<p>3% Elastano<\/p>\n<p>TAMANHOS \u00daNICOS<\/p>\n<p>Veste do 44 ao 48<\/p>\n",
        "short_description": "",
        "sku": "MVPlusLote50",
        "price": "474.50",
        "regular_price": "474.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>474,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 17,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "14.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1038,
          978,
          1036,
          1033,
          1030
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 51,
            "name": "LOTES COM 50 PE\u00c7AS",
            "slug": "lotes-com-50-pecas"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3543,
            "date_created": "2017-02-08T15:31:32",
            "date_created_gmt": "2017-02-08T18:31:32",
            "date_modified": "2017-02-08T15:31:32",
            "date_modified_gmt": "2017-02-08T18:31:32",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2017\/02\/vestidos-e-macaquitos-plus.jpg",
            "name": "vestidos-e-macaquitos-plus",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 249,
        "meta_data": [
          {
            "id": 12423,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12424,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12425,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 12426,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12427,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12428,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12453,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12454,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12455,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12462,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12463,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3541"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3311,
        "name": "Macaquitos Liganete Plus Size REF:MPluslote50 <br \/>(lote 50 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "macaquitos-liganete-plus-size-refmpluslote50-lote-50unid-r949-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquitos-liganete-plus-size-refmpluslote50-lote-50unid-r949-cada\/",
        "date_created": "2016-12-28T01:51:52",
        "date_created_gmt": "2016-12-28T01:51:52",
        "date_modified": "2018-07-18T07:56:33",
        "date_modified_gmt": "2018-07-18T07:56:33",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Lote composto com 50 pe\u00e7as Plus Size que v\u00e3o com modelos, estampas e cores sortidas.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>97% Poliester<\/p>\n<p>3% Elastano<\/p>\n<p>TAMANHOS \u00daNICOS<\/p>\n<p>Veste do 44 ao 48<\/p>\n",
        "short_description": "",
        "sku": "MPlusLote50",
        "price": "474.50",
        "regular_price": "474.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>474,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 14,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "14.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          3541,
          1626,
          2838,
          1625,
          979
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 51,
            "name": "LOTES COM 50 PE\u00c7AS",
            "slug": "lotes-com-50-pecas"
          },
          {
            "id": 24,
            "name": "Macaquito Plus Size",
            "slug": "macaquito-plus-size"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3310,
            "date_created": "2016-12-28T01:48:42",
            "date_created_gmt": "2016-12-28T04:48:42",
            "date_modified": "2016-12-28T01:48:42",
            "date_modified_gmt": "2016-12-28T04:48:42",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/macaquitolote50.png",
            "name": "macaquitolote50",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 250,
        "meta_data": [
          {
            "id": 12374,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12379,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12380,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12381,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12406,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12407,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12408,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12409,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12410,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12411,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12415,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          },
          {
            "id": 12416,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3311"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3308,
        "name": "Vestidos Liganete Plus Size REF:Vlote50 <br \/>(lote 50 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestidos-liganete-plus-size-refvlote50-lote-50unid-r949-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestidos-liganete-plus-size-refvlote50-lote-50unid-r949-cada\/",
        "date_created": "2016-12-28T01:49:33",
        "date_created_gmt": "2016-12-28T01:49:33",
        "date_modified": "2018-07-18T07:56:33",
        "date_modified_gmt": "2018-07-18T07:56:33",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Lote composto com 50 pe\u00e7as Plus Size que v\u00e3o com modelos, estampas e cores sortidas.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>97% Poliester<\/p>\n<p>3% Elastano<\/p>\n<p>&nbsp;<\/p>\n<p>TAMANHOS \u00daNICOS<\/p>\n<p>Veste do 44 ao 48<\/p>\n<p>&nbsp;<\/p>\n",
        "short_description": "",
        "sku": "Vlote50 Plus Size",
        "price": "474.50",
        "regular_price": "474.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>474,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 29,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "14.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1619,
          1625,
          3541,
          1614,
          1623
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 51,
            "name": "LOTES COM 50 PE\u00c7AS",
            "slug": "lotes-com-50-pecas"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3309,
            "date_created": "2016-12-28T01:47:47",
            "date_created_gmt": "2016-12-28T04:47:47",
            "date_modified": "2016-12-28T01:47:47",
            "date_modified_gmt": "2016-12-28T04:47:47",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/vestidosplus50.png",
            "name": "vestidosplus50",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 509,
        "meta_data": [
          {
            "id": 12326,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12331,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12332,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12333,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12358,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12359,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12360,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12361,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "79"
          },
          {
            "id": 12362,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12363,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12367,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          },
          {
            "id": 12368,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3308"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3306,
        "name": "Vestido Infantil Beb\u00ea  REF: vestInfSublimado <br \/>(lote 10 UN)<div id=\"destaque\">R$7,70 UN<\/div>",
        "slug": "vestido-infantil-bebe-ref-vestinfsublimado-lote-10-unid-r770-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-bebe-ref-vestinfsublimado-lote-10-unid-r770-cada\/",
        "date_created": "2016-12-27T20:27:43",
        "date_created_gmt": "2016-12-27T20:27:43",
        "date_modified": "2018-07-18T07:56:34",
        "date_modified_gmt": "2018-07-18T07:56:34",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil beb\u00ea \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 2 ano<\/p>\n<p>Tamanho<\/p>\n<p>P, M, G (os tamanhos v\u00e3o sortidos)<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestInfSublimado",
        "price": "77.00",
        "regular_price": "77.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>77,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 6,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "0.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1232,
          1215,
          1311,
          1313,
          1363
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3267,
            "date_created": "2016-12-27T03:01:58",
            "date_created_gmt": "2016-12-27T06:01:58",
            "date_modified": "2016-12-27T03:01:58",
            "date_modified_gmt": "2016-12-27T06:01:58",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/4-1.png",
            "name": "4",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 367,
        "meta_data": [
          {
            "id": 12279,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12284,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12285,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12286,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12311,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12312,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12313,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12314,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12315,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12316,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12320,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3306"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3305,
        "name": "Vestido Infantil Beb\u00ea  REF: VestInf2002 <br \/>(lote 10 UN)<div id=\"destaque\">R$5,99 UN<\/div>",
        "slug": "vestido-infantil-bebe-ref-vestinf2002-lote-10-unid-r599-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-bebe-ref-vestinf2002-lote-10-unid-r599-cada\/",
        "date_created": "2016-12-27T20:22:35",
        "date_created_gmt": "2016-12-27T20:22:35",
        "date_modified": "2018-07-18T07:56:34",
        "date_modified_gmt": "2018-07-18T07:56:34",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil beb\u00ea \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 2 ano<\/p>\n<p>Tamanho<\/p>\n<p>2 \u00a0anos<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VestInfNaNaBebe2002",
        "price": "59.90",
        "regular_price": "59.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>59,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 19,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "0.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1221,
          2564,
          1313,
          1314,
          1220
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3265,
            "date_created": "2016-12-27T03:01:27",
            "date_created_gmt": "2016-12-27T06:01:27",
            "date_modified": "2016-12-27T03:01:27",
            "date_modified_gmt": "2016-12-27T06:01:27",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-10.png",
            "name": "2",
            "alt": "",
            "position": 0
          },
          {
            "id": 3266,
            "date_created": "2016-12-27T03:01:41",
            "date_created_gmt": "2016-12-27T06:01:41",
            "date_modified": "2016-12-27T03:01:41",
            "date_modified_gmt": "2016-12-27T06:01:41",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/3-1.png",
            "name": "3",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 366,
        "meta_data": [
          {
            "id": 12231,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12236,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12237,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12238,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12263,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12264,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12265,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12266,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12267,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12268,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12271,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          },
          {
            "id": 12273,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3305"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3303,
        "name": "Vestido Infantil REF: vestidoinfnana010 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana010-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana010-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T20:13:11",
        "date_created_gmt": "2016-12-27T20:13:11",
        "date_modified": "2018-07-18T07:56:34",
        "date_modified_gmt": "2018-07-18T07:56:34",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana010",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 17,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1364,
          2564,
          1363,
          1311,
          2561
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3263,
            "date_created": "2016-12-27T03:00:53",
            "date_created_gmt": "2016-12-27T06:00:53",
            "date_modified": "2016-12-27T03:00:53",
            "date_modified_gmt": "2016-12-27T06:00:53",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-17.png",
            "name": "1",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 370,
        "meta_data": [
          {
            "id": 12184,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12189,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12190,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12191,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12216,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12217,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12218,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12219,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12220,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12221,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12225,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3303"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3302,
        "name": "Vestido Infantil REF: vestidoinfnana09 <br \/>(lote 10 UN)<div id=\"destaque\">R$,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana09-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana09-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T20:11:13",
        "date_created_gmt": "2016-12-27T20:11:13",
        "date_modified": "2018-07-18T07:56:34",
        "date_modified_gmt": "2018-07-18T07:56:34",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana09",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 7,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1215,
          1217,
          1364,
          1365,
          1233
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3262,
            "date_created": "2016-12-27T03:00:39",
            "date_created_gmt": "2016-12-27T06:00:39",
            "date_modified": "2016-12-27T03:00:39",
            "date_modified_gmt": "2016-12-27T06:00:39",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-16.png",
            "name": "1",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 378,
        "meta_data": [
          {
            "id": 12137,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12142,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12143,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12144,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12169,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12170,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12171,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12172,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12173,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12174,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12178,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3302"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3301,
        "name": "Vestido Infantil REF: vestidoinfnana08 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana08-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana08-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T20:06:05",
        "date_created_gmt": "2016-12-27T20:06:05",
        "date_modified": "2018-07-18T07:56:34",
        "date_modified_gmt": "2018-07-18T07:56:34",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana08",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 13,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1232,
          1310,
          1215,
          1234,
          1355
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3260,
            "date_created": "2016-12-27T03:00:08",
            "date_created_gmt": "2016-12-27T06:00:08",
            "date_modified": "2016-12-27T03:00:08",
            "date_modified_gmt": "2016-12-27T06:00:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-15.png",
            "name": "1",
            "alt": "",
            "position": 0
          },
          {
            "id": 3261,
            "date_created": "2016-12-27T03:00:21",
            "date_created_gmt": "2016-12-27T06:00:21",
            "date_modified": "2016-12-27T03:00:21",
            "date_modified_gmt": "2016-12-27T06:00:21",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-9.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 377,
        "meta_data": [
          {
            "id": 12090,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12095,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12096,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12097,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12122,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12123,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12124,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12125,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12126,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12127,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12131,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3301"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3299,
        "name": "Vestido Infantil REF: vestidoinfnana07 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana07-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana07-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T19:56:13",
        "date_created_gmt": "2016-12-27T19:56:13",
        "date_modified": "2018-07-18T07:56:34",
        "date_modified_gmt": "2018-07-18T07:56:34",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana07",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 15,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1215,
          1314,
          1223,
          1229,
          1220
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3259,
            "date_created": "2016-12-27T02:59:53",
            "date_created_gmt": "2016-12-27T05:59:53",
            "date_modified": "2016-12-27T02:59:53",
            "date_modified_gmt": "2016-12-27T05:59:53",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-14.png",
            "name": "1",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 376,
        "meta_data": [
          {
            "id": 12043,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12048,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12049,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12050,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12075,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12076,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12077,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12078,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12079,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12080,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12084,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3299"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3298,
        "name": "Vestido Infantil REF: vestidoinfnana06 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana06-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana06-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T19:52:16",
        "date_created_gmt": "2016-12-27T19:52:16",
        "date_modified": "2018-07-18T07:56:35",
        "date_modified_gmt": "2018-07-18T07:56:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana06",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 7,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1233,
          1365,
          1232,
          1222,
          1313
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3257,
            "date_created": "2016-12-27T02:59:27",
            "date_created_gmt": "2016-12-27T05:59:27",
            "date_modified": "2016-12-27T02:59:27",
            "date_modified_gmt": "2016-12-27T05:59:27",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-13.png",
            "name": "1",
            "alt": "",
            "position": 0
          },
          {
            "id": 3258,
            "date_created": "2016-12-27T02:59:39",
            "date_created_gmt": "2016-12-27T05:59:39",
            "date_modified": "2016-12-27T02:59:39",
            "date_modified_gmt": "2016-12-27T05:59:39",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-8.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 375,
        "meta_data": [
          {
            "id": 11996,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 12001,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 12002,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 12003,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 12028,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 12029,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 12030,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 12031,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 12032,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 12033,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 12037,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3298"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3297,
        "name": "Vestido Infantil REF: vestidoinfnana05 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana05-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana05-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T19:50:09",
        "date_created_gmt": "2016-12-27T19:50:09",
        "date_modified": "2018-07-18T07:56:35",
        "date_modified_gmt": "2018-07-18T07:56:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana05",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 4,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1223,
          1363,
          1217,
          1310,
          1305
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3255,
            "date_created": "2016-12-27T02:58:58",
            "date_created_gmt": "2016-12-27T05:58:58",
            "date_modified": "2016-12-27T02:58:58",
            "date_modified_gmt": "2016-12-27T05:58:58",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-12.png",
            "name": "1",
            "alt": "",
            "position": 0
          },
          {
            "id": 3256,
            "date_created": "2016-12-27T02:59:11",
            "date_created_gmt": "2016-12-27T05:59:11",
            "date_modified": "2016-12-27T02:59:11",
            "date_modified_gmt": "2016-12-27T05:59:11",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-7.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 374,
        "meta_data": [
          {
            "id": 11949,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11954,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11955,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11956,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11981,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11982,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11983,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11984,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 11985,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11986,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11990,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3297"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3296,
        "name": "Vestido Infantil REF: vestidoinfnana04 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana04-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana04-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T19:48:08",
        "date_created_gmt": "2016-12-27T19:48:08",
        "date_modified": "2018-07-18T07:56:35",
        "date_modified_gmt": "2018-07-18T07:56:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana04",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 10,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          2564,
          1229,
          1313,
          1222,
          1233
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3253,
            "date_created": "2016-12-27T02:58:27",
            "date_created_gmt": "2016-12-27T05:58:27",
            "date_modified": "2016-12-27T02:58:27",
            "date_modified_gmt": "2016-12-27T05:58:27",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-11.png",
            "name": "1",
            "alt": "",
            "position": 0
          },
          {
            "id": 3254,
            "date_created": "2016-12-27T02:58:42",
            "date_created_gmt": "2016-12-27T05:58:42",
            "date_modified": "2016-12-27T02:58:42",
            "date_modified_gmt": "2016-12-27T05:58:42",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-6.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 373,
        "meta_data": [
          {
            "id": 11902,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11903,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11904,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11929,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11930,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11935,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11936,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11937,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 11938,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11939,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11943,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3296"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3295,
        "name": "Vestido Infantil REF: vestidoinfnana03 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana03-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana03-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T19:42:30",
        "date_created_gmt": "2016-12-27T19:42:30",
        "date_modified": "2018-07-18T07:56:35",
        "date_modified_gmt": "2018-07-18T07:56:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana03",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 7,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1313,
          1229,
          1217,
          1314,
          1317
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3251,
            "date_created": "2016-12-27T02:57:53",
            "date_created_gmt": "2016-12-27T05:57:53",
            "date_modified": "2016-12-27T02:57:53",
            "date_modified_gmt": "2016-12-27T05:57:53",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-10.png",
            "name": "1",
            "alt": "",
            "position": 0
          },
          {
            "id": 3252,
            "date_created": "2016-12-27T02:58:09",
            "date_created_gmt": "2016-12-27T05:58:09",
            "date_modified": "2016-12-27T02:58:09",
            "date_modified_gmt": "2016-12-27T05:58:09",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-5.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 372,
        "meta_data": [
          {
            "id": 11866,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11867,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11868,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11869,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 11870,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11871,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11875,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11880,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11881,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11882,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11896,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3295"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3294,
        "name": "Vestido Infantil REF: vestidoinfnana02 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana02-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana02-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T19:39:27",
        "date_created_gmt": "2016-12-27T19:39:27",
        "date_modified": "2018-07-18T07:56:35",
        "date_modified_gmt": "2018-07-18T07:56:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana02",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 22,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1317,
          1314,
          2558,
          1234,
          1311
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3249,
            "date_created": "2016-12-27T02:57:23",
            "date_created_gmt": "2016-12-27T05:57:23",
            "date_modified": "2016-12-27T02:57:23",
            "date_modified_gmt": "2016-12-27T05:57:23",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-9.png",
            "name": "1",
            "alt": "",
            "position": 0
          },
          {
            "id": 3250,
            "date_created": "2016-12-27T02:57:38",
            "date_created_gmt": "2016-12-27T05:57:38",
            "date_modified": "2016-12-27T02:57:38",
            "date_modified_gmt": "2016-12-27T05:57:38",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-4.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 371,
        "meta_data": [
          {
            "id": 11809,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11810,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 11811,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11812,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11815,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11816,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11817,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11842,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11843,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11844,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11849,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3294"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3291,
        "name": "Vestido Infantil REF: vestidoinfnana01 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vestidoinfnana01-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestidoinfnana01-lote-10-unid-r735-cada\/",
        "date_created": "2016-12-27T19:09:55",
        "date_created_gmt": "2016-12-27T19:09:55",
        "date_modified": "2018-07-18T07:56:35",
        "date_modified_gmt": "2018-07-18T07:56:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>&nbsp;<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinfnana01",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 8,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          2558,
          1317,
          1232,
          1314,
          1364
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3247,
            "date_created": "2016-12-27T02:56:43",
            "date_created_gmt": "2016-12-27T05:56:43",
            "date_modified": "2016-12-27T02:56:43",
            "date_modified_gmt": "2016-12-27T05:56:43",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-8.png",
            "name": "1",
            "alt": "",
            "position": 0
          },
          {
            "id": 3248,
            "date_created": "2016-12-27T02:56:57",
            "date_created_gmt": "2016-12-27T05:56:57",
            "date_modified": "2016-12-27T02:56:57",
            "date_modified_gmt": "2016-12-27T05:56:57",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-3.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 369,
        "meta_data": [
          {
            "id": 11764,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11765,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11766,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11767,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 11768,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11769,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11774,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11779,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11780,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11781,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11802,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3291"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3290,
        "name": "Cal\u00e7a Tactel REF:cal\u00e7atactelco29 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,99 UN<\/div>",
        "slug": "calca-tactel-refcalcatactelco29-lote-10-unid-r999-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/calca-tactel-refcalcatactelco29-lote-10-unid-r999-cada\/",
        "date_created": "2016-12-27T18:52:03",
        "date_created_gmt": "2016-12-27T18:52:03",
        "date_modified": "2018-08-01T14:34:35",
        "date_modified_gmt": "2018-08-01T14:34:35",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Cal\u00e7a\u00a0Tactel masculina \u00a0 de \u00f3tima qualidade, excelente para revenda.<\/p>\n<p>TAMANHOS: P, M, G, GG\u00a0<strong><em>(ATEN\u00c7\u00c3O! os tamanhos v\u00e3o sortidos)<\/em><\/strong><\/p>\n<p>100% Poliester<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "cal\u00e7atactel029",
        "price": "99.90",
        "regular_price": "99.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>99,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 37,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.800",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          4063,
          1321,
          4046,
          1295,
          4072
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 54,
            "name": "Cal\u00e7a tactel",
            "slug": "calca-tactel"
          },
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3264,
            "date_created": "2016-12-27T03:01:08",
            "date_created_gmt": "2016-12-27T06:01:08",
            "date_modified": "2016-12-27T03:01:08",
            "date_modified_gmt": "2016-12-27T06:01:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/1-18.png",
            "name": "1",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 77,
        "meta_data": [
          {
            "id": 11713,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11718,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11719,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11720,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11745,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11746,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11747,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11748,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "58"
          },
          {
            "id": 11749,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11750,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11754,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          },
          {
            "id": 11755,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3290"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3286,
        "name": "Cal\u00e7a Tactel REF: cal\u00e7atactelco29 (LOTE 10UNID) R$9,99 CADA",
        "slug": "calca-tactel-ref-calcatactelco29-lote-10unid-r999-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/calca-tactel-ref-calcatactelco29-lote-10unid-r999-cada\/",
        "date_created": "2016-12-27T21:46:57",
        "date_created_gmt": "2016-12-27T21:46:57",
        "date_modified": "2018-07-18T07:56:36",
        "date_modified_gmt": "2018-07-18T07:56:36",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Cal\u00e7a\u00a0masculina Tactel \u00a0barato de \u00f3tima qualidade, excelente para revenda.<\/p>\n<p>TAMANHOS: P, M, G, GG\u00a0<strong><em>(ATEN\u00c7\u00c3O! os tamanhos v\u00e3o sortidos)<\/em><\/strong><\/p>\n<p>100% Poliester<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "",
        "price": "",
        "regular_price": "",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "",
        "on_sale": false,
        "purchasable": false,
        "total_sales": 0,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1303,
          1328,
          1288,
          1335,
          1327
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 54,
            "name": "Cal\u00e7a tactel",
            "slug": "calca-tactel"
          },
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          },
          {
            "id": 55,
            "name": "NOVIDADES",
            "slug": "novidades"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 0,
            "date_created": "2018-09-07T01:32:57",
            "date_created_gmt": "2018-09-07T01:32:57",
            "date_modified": "2018-09-07T01:32:57",
            "date_modified_gmt": "2018-09-07T01:32:57",
            "src": "http:\/\/oreidaliganete.com.br\/wp-content\/plugins\/woocommerce\/assets\/images\/placeholder.png",
            "name": "Padr\u00e3o",
            "alt": "Padr\u00e3o",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 76,
        "meta_data": [
          {
            "id": 11704,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11708,
            "key": "_publicize_pending",
            "value": "1"
          },
          {
            "id": 11709,
            "key": "slide_template",
            "value": "default"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3286"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3284,
        "name": "Macaquito Liganete REF:Mgil10 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refmgil10-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refmgil10-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T16:15:05",
        "date_created_gmt": "2016-12-27T16:15:05",
        "date_modified": "2018-07-18T07:56:36",
        "date_modified_gmt": "2018-07-18T07:56:36",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "mgil10",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 27,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          968,
          973,
          1028,
          915,
          1039
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3190,
            "date_created": "2016-12-27T02:37:22",
            "date_created_gmt": "2016-12-27T05:37:22",
            "date_modified": "2016-12-27T02:37:22",
            "date_modified_gmt": "2016-12-27T05:37:22",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/25.png",
            "name": "25",
            "alt": "",
            "position": 0
          },
          {
            "id": 3179,
            "date_created": "2016-12-27T02:33:17",
            "date_created_gmt": "2016-12-27T05:33:17",
            "date_modified": "2016-12-27T02:33:17",
            "date_modified_gmt": "2016-12-27T05:33:17",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/14.png",
            "name": "14",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 238,
        "meta_data": [
          {
            "id": 11657,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11661,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11662,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11663,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11688,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11689,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11690,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11691,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11692,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11693,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11697,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3284"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3283,
        "name": "Macaquito Liganete REF:m13 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm13-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm13-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T16:09:25",
        "date_created_gmt": "2016-12-27T16:09:25",
        "date_modified": "2018-07-18T07:56:36",
        "date_modified_gmt": "2018-07-18T07:56:36",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 6,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          966,
          965,
          1035,
          969,
          967
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3185,
            "date_created": "2016-12-27T02:35:36",
            "date_created_gmt": "2016-12-27T05:35:36",
            "date_modified": "2016-12-27T02:35:36",
            "date_modified_gmt": "2016-12-27T05:35:36",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/20.png",
            "name": "20",
            "alt": "",
            "position": 0
          },
          {
            "id": 3178,
            "date_created": "2016-12-27T02:32:57",
            "date_created_gmt": "2016-12-27T05:32:57",
            "date_modified": "2016-12-27T02:32:57",
            "date_modified_gmt": "2016-12-27T05:32:57",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/13.png",
            "name": "13",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 217,
        "meta_data": [
          {
            "id": 11624,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11625,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11626,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11627,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11628,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11629,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11633,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11638,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11639,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11640,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11649,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3283"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3282,
        "name": "Macaquito Liganete REF:Mgil9 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refmgil9-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refmgil9-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T16:06:41",
        "date_created_gmt": "2016-12-27T16:06:41",
        "date_modified": "2018-07-18T07:56:36",
        "date_modified_gmt": "2018-07-18T07:56:36",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "MGIL9",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 22,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1039,
          966,
          1033,
          965,
          1028
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3189,
            "date_created": "2016-12-27T02:37:03",
            "date_created_gmt": "2016-12-27T05:37:03",
            "date_modified": "2016-12-27T02:37:03",
            "date_modified_gmt": "2016-12-27T05:37:03",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/24.png",
            "name": "24",
            "alt": "",
            "position": 0
          },
          {
            "id": 3176,
            "date_created": "2016-12-27T02:32:17",
            "date_created_gmt": "2016-12-27T05:32:17",
            "date_modified": "2016-12-27T02:32:17",
            "date_modified_gmt": "2016-12-27T05:32:17",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/11.png",
            "name": "11",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 246,
        "meta_data": [
          {
            "id": 11562,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11567,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11568,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11569,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11594,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11595,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11596,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11597,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11598,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11599,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11603,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3282"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3281,
        "name": "Macaquito Liganete REF:Mgil8 (LOTE 10UNID) R$7,40 CADA",
        "slug": "macaquito-liganete-refmgil8-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refmgil8-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T19:02:42",
        "date_created_gmt": "2016-12-27T19:02:42",
        "date_modified": "2018-07-18T07:56:36",
        "date_modified_gmt": "2018-07-18T07:56:36",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "MGIL8",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 11,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          969,
          979,
          965,
          1030,
          974
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3173,
            "date_created": "2016-12-27T02:31:21",
            "date_created_gmt": "2016-12-27T05:31:21",
            "date_modified": "2016-12-27T02:31:21",
            "date_modified_gmt": "2016-12-27T05:31:21",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/8.png",
            "name": "8",
            "alt": "",
            "position": 0
          },
          {
            "id": 3188,
            "date_created": "2016-12-27T02:36:45",
            "date_created_gmt": "2016-12-27T05:36:45",
            "date_modified": "2016-12-27T02:36:45",
            "date_modified_gmt": "2016-12-27T05:36:45",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/23.png",
            "name": "23",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 245,
        "meta_data": [
          {
            "id": 11515,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11520,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11521,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11522,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11547,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11548,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11549,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11550,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11551,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11552,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11556,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3281"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3280,
        "name": "Macaquito Liganete REF:Mgil7 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refmgil7-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refmgil7-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:59:37",
        "date_created_gmt": "2016-12-27T15:59:37",
        "date_modified": "2018-07-18T07:56:37",
        "date_modified_gmt": "2018-07-18T07:56:37",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "Mgil7",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 3,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          973,
          967,
          968,
          964,
          1040
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3187,
            "date_created": "2016-12-27T02:36:26",
            "date_created_gmt": "2016-12-27T05:36:26",
            "date_modified": "2016-12-27T02:36:26",
            "date_modified_gmt": "2016-12-27T05:36:26",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/22.png",
            "name": "22",
            "alt": "",
            "position": 0
          },
          {
            "id": 3172,
            "date_created": "2016-12-27T02:31:02",
            "date_created_gmt": "2016-12-27T05:31:02",
            "date_modified": "2016-12-27T02:31:02",
            "date_modified_gmt": "2016-12-27T05:31:02",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/7.png",
            "name": "7",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 244,
        "meta_data": [
          {
            "id": 11468,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11473,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11474,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11475,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11500,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11501,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11502,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11503,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11504,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11505,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11509,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3280"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3279,
        "name": "Macaquito Liganete REF:M12 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm12-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm12-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:55:54",
        "date_created_gmt": "2016-12-27T15:55:54",
        "date_modified": "2018-07-18T07:56:37",
        "date_modified_gmt": "2018-07-18T07:56:37",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M12",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 12,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          972,
          967,
          970,
          912,
          1038
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3184,
            "date_created": "2016-12-27T02:35:09",
            "date_created_gmt": "2016-12-27T05:35:09",
            "date_modified": "2016-12-27T02:35:09",
            "date_modified_gmt": "2016-12-27T05:35:09",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/19.png",
            "name": "19",
            "alt": "",
            "position": 0
          },
          {
            "id": 3171,
            "date_created": "2016-12-27T02:30:44",
            "date_created_gmt": "2016-12-27T05:30:44",
            "date_modified": "2016-12-27T02:30:44",
            "date_modified_gmt": "2016-12-27T05:30:44",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/6.png",
            "name": "6",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 216,
        "meta_data": [
          {
            "id": 11421,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11426,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11427,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11428,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11453,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11454,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11455,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11456,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11457,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11458,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11462,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3279"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3278,
        "name": "Macaquito Liganete REF:M20 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm20-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm20-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:52:53",
        "date_created_gmt": "2016-12-27T15:52:53",
        "date_modified": "2018-07-18T07:56:37",
        "date_modified_gmt": "2018-07-18T07:56:37",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M20",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 28,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1040,
          980,
          966,
          973,
          1035
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3186,
            "date_created": "2016-12-27T02:36:03",
            "date_created_gmt": "2016-12-27T05:36:03",
            "date_modified": "2016-12-27T02:36:03",
            "date_modified_gmt": "2016-12-27T05:36:03",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/21.png",
            "name": "21",
            "alt": "",
            "position": 0
          },
          {
            "id": 3168,
            "date_created": "2016-12-27T02:29:43",
            "date_created_gmt": "2016-12-27T05:29:43",
            "date_modified": "2016-12-27T02:29:43",
            "date_modified_gmt": "2016-12-27T05:29:43",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/3.png",
            "name": "3",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 226,
        "meta_data": [
          {
            "id": 11374,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11379,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11380,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11381,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11406,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11407,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11408,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11409,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11410,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11411,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11415,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3278"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3277,
        "name": "Vestido Liganete REF:V69 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refv69-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refv69-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:47:37",
        "date_created_gmt": "2016-12-27T15:47:37",
        "date_modified": "2018-07-18T07:56:37",
        "date_modified_gmt": "2018-07-18T07:56:37",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "v69",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 9,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1039,
          980,
          975,
          1038,
          979
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3198,
            "date_created": "2016-12-27T02:39:41",
            "date_created_gmt": "2016-12-27T05:39:41",
            "date_modified": "2016-12-27T02:39:41",
            "date_modified_gmt": "2016-12-27T05:39:41",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/33.png",
            "name": "33",
            "alt": "",
            "position": 0
          },
          {
            "id": 3183,
            "date_created": "2016-12-27T02:34:44",
            "date_created_gmt": "2016-12-27T05:34:44",
            "date_modified": "2016-12-27T02:34:44",
            "date_modified_gmt": "2016-12-27T05:34:44",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/18.png",
            "name": "18",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 477,
        "meta_data": [
          {
            "id": 11327,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11332,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11333,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11334,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11359,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11360,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11361,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11362,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11363,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11364,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11368,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3277"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3276,
        "name": "Vestido Liganete REF:V62 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refv62-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refv62-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:42:48",
        "date_created_gmt": "2016-12-27T15:42:48",
        "date_modified": "2018-07-18T07:56:37",
        "date_modified_gmt": "2018-07-18T07:56:37",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "v62",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 12,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1035,
          1033,
          1030,
          968,
          969
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3197,
            "date_created": "2016-12-27T02:39:25",
            "date_created_gmt": "2016-12-27T05:39:25",
            "date_modified": "2016-12-27T02:39:25",
            "date_modified_gmt": "2016-12-27T05:39:25",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/32.png",
            "name": "32",
            "alt": "",
            "position": 0
          },
          {
            "id": 3182,
            "date_created": "2016-12-27T02:34:18",
            "date_created_gmt": "2016-12-27T05:34:18",
            "date_modified": "2016-12-27T02:34:18",
            "date_modified_gmt": "2016-12-27T05:34:18",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/17.png",
            "name": "17",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 476,
        "meta_data": [
          {
            "id": 11280,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11285,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11286,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11287,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11312,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11313,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11314,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11315,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11316,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11317,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11321,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3276"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3275,
        "name": "Vestido Liganete REF:vnalva1 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refvnalva1-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refvnalva1-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:38:29",
        "date_created_gmt": "2016-12-27T15:38:29",
        "date_modified": "2018-07-18T07:56:37",
        "date_modified_gmt": "2018-07-18T07:56:37",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vnalva1",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 47,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          979,
          1028,
          1033,
          1036,
          912
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3201,
            "date_created": "2016-12-27T02:40:34",
            "date_created_gmt": "2016-12-27T05:40:34",
            "date_modified": "2016-12-27T02:40:34",
            "date_modified_gmt": "2016-12-27T05:40:34",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/36.png",
            "name": "36",
            "alt": "",
            "position": 0
          },
          {
            "id": 3180,
            "date_created": "2016-12-27T02:33:37",
            "date_created_gmt": "2016-12-27T05:33:37",
            "date_modified": "2016-12-27T02:33:37",
            "date_modified_gmt": "2016-12-27T05:33:37",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/15.png",
            "name": "15",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 492,
        "meta_data": [
          {
            "id": 11233,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11238,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11239,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11240,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11265,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11266,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11267,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11268,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11269,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11270,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11274,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3275"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3274,
        "name": "Vestido Liganete REF:vgil4 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refvgil4-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refvgil4-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:29:59",
        "date_created_gmt": "2016-12-27T15:29:59",
        "date_modified": "2018-07-18T07:56:38",
        "date_modified_gmt": "2018-07-18T07:56:38",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vgil4",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 33,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1028,
          966,
          979,
          912,
          1040
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3199,
            "date_created": "2016-12-27T02:40:01",
            "date_created_gmt": "2016-12-27T05:40:01",
            "date_modified": "2016-12-27T02:40:01",
            "date_modified_gmt": "2016-12-27T05:40:01",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/34.png",
            "name": "34",
            "alt": "",
            "position": 0
          },
          {
            "id": 3177,
            "date_created": "2016-12-27T02:32:38",
            "date_created_gmt": "2016-12-27T05:32:38",
            "date_modified": "2016-12-27T02:32:38",
            "date_modified_gmt": "2016-12-27T05:32:38",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/12.png",
            "name": "12",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 485,
        "meta_data": [
          {
            "id": 11186,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11191,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11192,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11193,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11218,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11219,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11220,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11221,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11222,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11223,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11227,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3274"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3273,
        "name": "Vestido Liganete REF:vn\u00f3 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refvno-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refvno-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:08:38",
        "date_created_gmt": "2016-12-27T15:08:38",
        "date_modified": "2018-07-25T21:20:31",
        "date_modified_gmt": "2018-07-25T21:20:31",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "Vn\u00f3",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 11,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1035,
          968,
          912,
          978,
          1040
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3202,
            "date_created": "2016-12-27T02:40:52",
            "date_created_gmt": "2016-12-27T05:40:52",
            "date_modified": "2016-12-27T02:40:52",
            "date_modified_gmt": "2016-12-27T05:40:52",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/37.png",
            "name": "37",
            "alt": "",
            "position": 0
          },
          {
            "id": 3174,
            "date_created": "2016-12-27T02:31:38",
            "date_created_gmt": "2016-12-27T05:31:38",
            "date_modified": "2016-12-27T02:31:38",
            "date_modified_gmt": "2016-12-27T05:31:38",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/9.png",
            "name": "9",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 493,
        "meta_data": [
          {
            "id": 11139,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11144,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11145,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11146,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11171,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11172,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11173,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11174,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11175,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11176,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11180,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3273"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3271,
        "name": "Vestido Liganete REF:v14socorro <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refv14socorro-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refv14socorro-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T15:01:38",
        "date_created_gmt": "2016-12-27T15:01:38",
        "date_modified": "2018-07-25T21:20:31",
        "date_modified_gmt": "2018-07-25T21:20:31",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "v14socorro",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 4,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1028,
          972,
          1035,
          973,
          967
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3170,
            "date_created": "2016-12-27T02:30:23",
            "date_created_gmt": "2016-12-27T05:30:23",
            "date_modified": "2016-12-27T02:30:23",
            "date_modified_gmt": "2016-12-27T05:30:23",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/5.png",
            "name": "5",
            "alt": "",
            "position": 0
          },
          {
            "id": 3196,
            "date_created": "2016-12-27T02:39:08",
            "date_created_gmt": "2016-12-27T05:39:08",
            "date_modified": "2016-12-27T02:39:08",
            "date_modified_gmt": "2016-12-27T05:39:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/31.png",
            "name": "31",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 466,
        "meta_data": [
          {
            "id": 11092,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11097,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11098,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11099,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11124,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11125,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11126,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11127,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11128,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11129,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11133,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3271"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3269,
        "name": "Vestido Liganete REF:vGolacaida <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refvgolacaida-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refvgolacaida-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T14:47:30",
        "date_created_gmt": "2016-12-27T14:47:30",
        "date_modified": "2018-07-25T21:27:23",
        "date_modified_gmt": "2018-07-25T21:27:23",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vGolacaida",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 22,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.750",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          976,
          1035,
          970,
          969,
          974
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3200,
            "date_created": "2016-12-27T02:40:18",
            "date_created_gmt": "2016-12-27T05:40:18",
            "date_modified": "2016-12-27T02:40:18",
            "date_modified_gmt": "2016-12-27T05:40:18",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/35.png",
            "name": "35",
            "alt": "",
            "position": 0
          },
          {
            "id": 3169,
            "date_created": "2016-12-27T02:30:03",
            "date_created_gmt": "2016-12-27T05:30:03",
            "date_modified": "2016-12-27T02:30:03",
            "date_modified_gmt": "2016-12-27T05:30:03",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/4.png",
            "name": "4",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 491,
        "meta_data": [
          {
            "id": 11043,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11048,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 11049,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 11050,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11075,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11076,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11077,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11078,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11079,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11080,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11083,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          },
          {
            "id": 11086,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3269"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 3268,
        "name": "Vestido Liganete REF:V5 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-ref-v5-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-ref-v5-lote-10unid-r740-cada\/",
        "date_created": "2016-12-27T14:38:23",
        "date_created_gmt": "2016-12-27T14:38:23",
        "date_modified": "2018-07-18T07:56:38",
        "date_modified_gmt": "2018-07-18T07:56:38",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "V5",
        "price": "74.40",
        "regular_price": "74.40",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,40<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 13,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.750",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          964,
          978,
          1036,
          975,
          912
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 3195,
            "date_created": "2016-12-27T02:38:49",
            "date_created_gmt": "2016-12-27T05:38:49",
            "date_modified": "2016-12-27T02:38:49",
            "date_modified_gmt": "2016-12-27T05:38:49",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/30.png",
            "name": "30",
            "alt": "",
            "position": 0
          },
          {
            "id": 3167,
            "date_created": "2016-12-27T02:29:26",
            "date_created_gmt": "2016-12-27T05:29:26",
            "date_modified": "2016-12-27T02:29:26",
            "date_modified_gmt": "2016-12-27T05:29:26",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/12\/2-1.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 475,
        "meta_data": [
          {
            "id": 10997,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10998,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10999,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 11024,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 11025,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 11026,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 11027,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 11028,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 11029,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 11032,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          },
          {
            "id": 11034,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 11037,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/3268"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2872,
        "name": "Conjunto Masculino REF: conjuntomasccommanga <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "conjunto-masculino-ref-conjuntomasccommanga-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/conjunto-masculino-ref-conjuntomasccommanga-lote-10-unid-r735-cada\/",
        "date_created": "2016-10-18T12:47:03",
        "date_created_gmt": "2016-10-18T12:47:03",
        "date_modified": "2018-08-01T14:27:50",
        "date_modified_gmt": "2018-08-01T14:27:50",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Conjuntinho masculino barato para voc\u00ea revender.<\/p>\n<p>Medidas do Modelo<\/p>\n<p>Veste tamanho 5 anos<\/p>\n<p>Tamanhos<\/p>\n<p>P,M e G<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "conjuntomasccommanga",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 95,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.300",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1314,
          2558,
          1217,
          1216,
          1363
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 46,
            "name": "Conjunto",
            "slug": "conjunto"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 77,
            "name": "Masculino",
            "slug": "infantil-maculino"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2873,
            "date_created": "2016-10-18T12:46:34",
            "date_created_gmt": "2016-10-18T15:46:34",
            "date_modified": "2016-10-18T12:46:34",
            "date_modified_gmt": "2016-10-18T15:46:34",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/fundo-das-fotos-do-rei-1.png",
            "name": "fundo-das-fotos-do-rei",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 185,
        "meta_data": [
          {
            "id": 10952,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10957,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10958,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10959,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10984,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10985,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10986,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10987,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 10988,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10989,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10993,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2872"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2870,
        "name": "Conjunto Masculino REF: conjinfantilmascsemmanga <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "conjunto-masculino-ref-conjinfantilmascsemmanga-lote-10-unid-r735-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/conjunto-masculino-ref-conjinfantilmascsemmanga-lote-10-unid-r735-cada\/",
        "date_created": "2016-10-18T12:44:28",
        "date_created_gmt": "2016-10-18T12:44:28",
        "date_modified": "2018-07-18T08:15:48",
        "date_modified_gmt": "2018-07-18T08:15:48",
        "type": "simple",
        "status": "publish",
        "featured": true,
        "catalog_visibility": "visible",
        "description": "<p>Conjuntinho masculino barato para voc\u00ea revender.<\/p>\n<p>Medidas do Modelo<\/p>\n<p>&nbsp;<\/p>\n<p>Veste tamanho 5 anos<\/p>\n<p>Tamanhos<\/p>\n<p>P,M e G<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "conjinfantilmascsemmanga",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 165,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.300",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1234,
          1305,
          1229,
          1313,
          1215
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 46,
            "name": "Conjunto",
            "slug": "conjunto"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 77,
            "name": "Masculino",
            "slug": "infantil-maculino"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2871,
            "date_created": "2016-10-18T12:43:39",
            "date_created_gmt": "2016-10-18T15:43:39",
            "date_modified": "2016-10-18T12:43:39",
            "date_modified_gmt": "2016-10-18T15:43:39",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/conjuntoinfmasc.png",
            "name": "conjuntoinfmasc",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 184,
        "meta_data": [
          {
            "id": 10910,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10915,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10916,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10917,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10942,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10943,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10944,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10945,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 10946,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10947,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10951,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2870"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2867,
        "name": "Vestido Infantil  REF: VestInf2003 <br \/>(lote 10 UN)<div id=\"destaque\">R$5,99 UN<\/div>",
        "slug": "vestido-infantil-ref-vestinf2003-lote-10-unid-r599-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestinf2003-lote-10-unid-r599-cada\/",
        "date_created": "2016-10-18T12:12:44",
        "date_created_gmt": "2016-10-18T12:12:44",
        "date_modified": "2018-07-18T07:56:39",
        "date_modified_gmt": "2018-07-18T07:56:39",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0 barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 1 ano<\/p>\n<p>Tamanho<\/p>\n<p>\u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VestInfNaNaBebe2003",
        "price": "59.90",
        "regular_price": "59.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>59,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 98,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "0.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1363,
          2558,
          1310,
          1234,
          1314
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2869,
            "date_created": "2016-10-18T12:12:01",
            "date_created_gmt": "2016-10-18T15:12:01",
            "date_modified": "2016-10-18T12:12:01",
            "date_modified_gmt": "2016-10-18T15:12:01",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/fundo-das-fotos-do-rei.png",
            "name": "fundo-das-fotos-do-rei",
            "alt": "",
            "position": 0
          },
          {
            "id": 2868,
            "date_created": "2016-10-18T12:11:34",
            "date_created_gmt": "2016-10-18T15:11:34",
            "date_modified": "2016-10-18T12:11:34",
            "date_modified_gmt": "2016-10-18T15:11:34",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/2.png",
            "name": "2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 365,
        "meta_data": [
          {
            "id": 10862,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10867,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10868,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10869,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10894,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10895,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10896,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10897,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 10898,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10899,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10903,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          },
          {
            "id": 10904,
            "key": "_yoast_wpseo_is_cornerstone",
            "value": ""
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2867"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2863,
        "name": "Vestido Infantil  REF: VestInf2001 <br \/>(lote 10 UN)<div id=\"destaque\">R$5,99 UN<\/div>",
        "slug": "vestido-infantil-ref-vestinf2001-lote-10-unid-r599-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestinf2001-lote-10-unid-r599-cada\/",
        "date_created": "2016-10-18T12:00:37",
        "date_created_gmt": "2016-10-18T12:00:37",
        "date_modified": "2018-07-18T07:56:39",
        "date_modified_gmt": "2018-07-18T07:56:39",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0 barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 1 ano<\/p>\n<p>Tamanho<\/p>\n<p>2 e 4 anos<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VestInfNaNaBebe2001",
        "price": "59.90",
        "regular_price": "59.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>59,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 96,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "0.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          2561,
          1221,
          1233,
          1363,
          1313
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2864,
            "date_created": "2016-10-18T11:58:11",
            "date_created_gmt": "2016-10-18T14:58:11",
            "date_modified": "2016-10-18T11:58:11",
            "date_modified_gmt": "2016-10-18T14:58:11",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/VestInfNaNaBebe2001-.png",
            "name": "vestinfnanabebe2001",
            "alt": "",
            "position": 0
          },
          {
            "id": 2865,
            "date_created": "2016-10-18T11:58:46",
            "date_created_gmt": "2016-10-18T14:58:46",
            "date_modified": "2016-10-18T11:58:46",
            "date_modified_gmt": "2016-10-18T14:58:46",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/VestInfNaNaBebe2001-1.png",
            "name": "vestinfnanabebe2001-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 364,
        "meta_data": [
          {
            "id": 10820,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10825,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10826,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10827,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10852,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10853,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10854,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10855,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 10856,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10857,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10861,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2863"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2861,
        "name": "Vestido Infantil Beb\u00ea REF: VESTInfIRM01 <br \/>(lote 10 UN)<div id=\"destaque\">R$5,99 UN<\/div>",
        "slug": "vestido-infantil-ref-vestinfirm01-lote-10-unid-r599-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vestinfirm01-lote-10-unid-r599-cada\/",
        "date_created": "2016-10-18T11:53:45",
        "date_created_gmt": "2016-10-18T11:53:45",
        "date_modified": "2018-07-18T07:56:39",
        "date_modified_gmt": "2018-07-18T07:56:39",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>&nbsp;<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 1 ano<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VESTInfIRM01",
        "price": "59.90",
        "regular_price": "59.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>59,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 3,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "0.780",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          2564,
          1311,
          1233,
          1364,
          1215
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2862,
            "date_created": "2016-10-18T11:53:13",
            "date_created_gmt": "2016-10-18T14:53:13",
            "date_modified": "2016-10-18T11:53:13",
            "date_modified_gmt": "2016-10-18T14:53:13",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/VESTInfIRM01.png",
            "name": "vestinfirm01",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 368,
        "meta_data": [
          {
            "id": 10778,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10783,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10784,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10785,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10810,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10811,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10812,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10813,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 10814,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10815,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10818,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2861"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2857,
        "name": "Conjunto Feminino Beb\u00ea REF: conjInfIRM01 <br \/>(lote 10 UN)<div id=\"destaque\">R$5,99 UN<\/div>",
        "slug": "conjunto-feminino-bebe-ref-conjinfirm01lote-10-unid-r599-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/conjunto-feminino-bebe-ref-conjinfirm01lote-10-unid-r599-cada\/",
        "date_created": "2016-10-18T11:47:54",
        "date_created_gmt": "2016-10-18T11:47:54",
        "date_modified": "2018-07-25T21:45:37",
        "date_modified_gmt": "2018-07-25T21:45:37",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Conjuntinho feminino beb\u00ea com \u00f3timo pre\u00e7o para voc\u00ea revender.<\/p>\n<p>Medidas da Modelo<\/p>\n<p>&nbsp;<\/p>\n<p>crian\u00e7a veste tamanho 1 ano<\/p>\n<p>Tamanho<\/p>\n<p>\u00daNICO<\/p>\n<p>Composi\u00e7\u00e3o<\/p>\n<p>100% Algod\u00e3o<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "conjInfFemininoIRM01",
        "price": "59.90",
        "regular_price": "59.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>59,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 11,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "0.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1310,
          1365,
          1311,
          1223,
          2561
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 44,
            "name": "Conjuntinho",
            "slug": "conjuntinho"
          },
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2858,
            "date_created": "2016-10-18T11:42:45",
            "date_created_gmt": "2016-10-18T14:42:45",
            "date_modified": "2016-10-18T11:42:45",
            "date_modified_gmt": "2016-10-18T14:42:45",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/conjInfFemininoIRM01-.png",
            "name": "conjinffemininoirm01",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 170,
        "meta_data": [
          {
            "id": 10736,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10741,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10742,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10743,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10768,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10769,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10770,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10771,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 10772,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10773,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10777,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2857"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2856,
        "name": "Macaquito Liganete REF:M19 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm19-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm19-lote-10unid-r740-cada\/",
        "date_created": "2016-10-18T11:26:39",
        "date_created_gmt": "2016-10-18T11:26:39",
        "date_modified": "2018-07-18T07:56:39",
        "date_modified_gmt": "2018-07-18T07:56:39",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M19",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 49,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.750",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1036,
          975,
          969,
          1030,
          974
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2854,
            "date_created": "2016-10-18T11:22:36",
            "date_created_gmt": "2016-10-18T14:22:36",
            "date_modified": "2016-10-18T11:22:36",
            "date_modified_gmt": "2016-10-18T14:22:36",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/m19-.png",
            "name": "m19",
            "alt": "",
            "position": 0
          },
          {
            "id": 2855,
            "date_created": "2016-10-18T11:22:56",
            "date_created_gmt": "2016-10-18T14:22:56",
            "date_modified": "2016-10-18T11:22:56",
            "date_modified_gmt": "2016-10-18T14:22:56",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/m19-1.png",
            "name": "m19-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 224,
        "meta_data": [
          {
            "id": 10694,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10699,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10700,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10701,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10726,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10727,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10728,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10729,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10730,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10731,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10734,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2856"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2851,
        "name": "Macaquito Liganete REF:gil5 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refgil5-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refgil5-lote-10unid-r740-cada\/",
        "date_created": "2016-10-18T11:24:51",
        "date_created_gmt": "2016-10-18T11:24:51",
        "date_modified": "2018-07-18T07:56:39",
        "date_modified_gmt": "2018-07-18T07:56:39",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "gil5",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 26,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.750",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          974,
          1038,
          972,
          973,
          1028
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2852,
            "date_created": "2016-10-18T11:22:01",
            "date_created_gmt": "2016-10-18T14:22:01",
            "date_modified": "2016-10-18T11:22:01",
            "date_modified_gmt": "2016-10-18T14:22:01",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/gil5-.png",
            "name": "gil5",
            "alt": "",
            "position": 0
          },
          {
            "id": 2853,
            "date_created": "2016-10-18T11:22:18",
            "date_created_gmt": "2016-10-18T14:22:18",
            "date_modified": "2016-10-18T11:22:18",
            "date_modified_gmt": "2016-10-18T14:22:18",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/gil5-1.png",
            "name": "gil5-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 213,
        "meta_data": [
          {
            "id": 10652,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10657,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10658,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10659,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10684,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10685,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10686,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10687,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10688,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10689,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10692,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2851"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2850,
        "name": "Vestido Liganete Plus Size REF:075 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref075-lote-10unid-r899-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref075-lote-10unid-r899-cada\/",
        "date_created": "2016-10-18T11:16:31",
        "date_created_gmt": "2016-10-18T11:16:31",
        "date_modified": "2018-07-18T07:56:40",
        "date_modified_gmt": "2018-07-18T07:56:40",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Modelo da imagem veste 48<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "75",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 67,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          973,
          976,
          968,
          975,
          1035
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2831,
            "date_created": "2016-10-18T10:18:53",
            "date_created_gmt": "2016-10-18T13:18:53",
            "date_modified": "2016-10-18T10:18:53",
            "date_modified_gmt": "2016-10-18T13:18:53",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/75-.png",
            "name": "75",
            "alt": "",
            "position": 0
          },
          {
            "id": 2832,
            "date_created": "2016-10-18T10:19:14",
            "date_created_gmt": "2016-10-18T13:19:14",
            "date_modified": "2016-10-18T10:19:14",
            "date_modified_gmt": "2016-10-18T13:19:14",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/75-1.png",
            "name": "75-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 421,
        "meta_data": [
          {
            "id": 10610,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10615,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10616,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10617,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10642,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10643,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10644,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10645,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10646,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10647,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10650,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2850"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2847,
        "name": "Vestido Liganete Plus Size REF:072 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref072-lote-10unid-r899-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref072-lote-10unid-r899-cada\/",
        "date_created": "2016-10-18T11:14:58",
        "date_created_gmt": "2016-10-18T11:14:58",
        "date_modified": "2018-07-18T07:56:40",
        "date_modified_gmt": "2018-07-18T07:56:40",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Modelo da imagem veste 48<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "72",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 58,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          965,
          1039,
          980,
          973,
          975
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2830,
            "date_created": "2016-10-18T10:18:32",
            "date_created_gmt": "2016-10-18T13:18:32",
            "date_modified": "2016-10-18T10:18:32",
            "date_modified_gmt": "2016-10-18T13:18:32",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/72-.png",
            "name": "72",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 420,
        "meta_data": [
          {
            "id": 10568,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10573,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10574,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10575,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10600,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10601,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10602,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10603,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10604,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10605,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10608,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2847"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2845,
        "name": "Vestido Liganete Plus Size REF:062 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref062-lote-10unid-r899-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref062-lote-10unid-r899-cada\/",
        "date_created": "2016-10-18T11:13:09",
        "date_created_gmt": "2016-10-18T11:13:09",
        "date_modified": "2018-07-18T07:56:40",
        "date_modified_gmt": "2018-07-18T07:56:40",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Modelo da imagem veste 48<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "62",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 49,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          972,
          1035,
          966,
          1036,
          915
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2829,
            "date_created": "2016-10-18T10:18:04",
            "date_created_gmt": "2016-10-18T13:18:04",
            "date_modified": "2016-10-18T10:18:04",
            "date_modified_gmt": "2016-10-18T13:18:04",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/62-.png",
            "name": "62",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 419,
        "meta_data": [
          {
            "id": 10526,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10531,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10532,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10533,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10558,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10559,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10560,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10561,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10562,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10563,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10566,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2845"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2843,
        "name": "Vestido Liganete Plus Size REF:055 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref055-lote-10unid-r899-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref055-lote-10unid-r899-cada\/",
        "date_created": "2016-10-18T11:11:45",
        "date_created_gmt": "2016-10-18T11:11:45",
        "date_modified": "2018-07-18T07:56:40",
        "date_modified_gmt": "2018-07-18T07:56:40",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Modelo da imagem veste 48<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "55",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 37,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          969,
          1038,
          1039,
          973,
          967
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2827,
            "date_created": "2016-10-18T10:17:02",
            "date_created_gmt": "2016-10-18T13:17:02",
            "date_modified": "2016-10-18T10:17:02",
            "date_modified_gmt": "2016-10-18T13:17:02",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/55-.png",
            "name": "55",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 418,
        "meta_data": [
          {
            "id": 10484,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10489,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10490,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10491,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10516,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10517,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10518,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10519,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10520,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10521,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10524,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2843"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2842,
        "name": "Vestido Liganete Plus Size REF:043 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref043-lote-10unid-r899-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref043-lote-10unid-r899-cada\/",
        "date_created": "2016-10-18T11:10:10",
        "date_created_gmt": "2016-10-18T11:10:10",
        "date_modified": "2018-07-18T07:56:40",
        "date_modified_gmt": "2018-07-18T07:56:40",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Modelo da imagem veste 48<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "43",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 28,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          970,
          967,
          964,
          1028,
          1039
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2826,
            "date_created": "2016-10-18T10:16:40",
            "date_created_gmt": "2016-10-18T13:16:40",
            "date_modified": "2016-10-18T10:16:40",
            "date_modified_gmt": "2016-10-18T13:16:40",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/43-.png",
            "name": "43",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 414,
        "meta_data": [
          {
            "id": 10442,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10447,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10448,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10449,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10474,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10475,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10476,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10477,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10478,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10479,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10482,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2842"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2841,
        "name": "Vestido Liganete Plus Size REF:030 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref030-lote-10unid-r899-cada-2",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref030-lote-10unid-r899-cada-2\/",
        "date_created": "2016-10-18T11:08:33",
        "date_created_gmt": "2016-10-18T11:08:33",
        "date_modified": "2018-07-18T07:56:41",
        "date_modified_gmt": "2018-07-18T07:56:41",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Modelo da imagem veste 48<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "30",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 80,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          978,
          1038,
          912,
          969,
          976
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2824,
            "date_created": "2016-10-18T10:15:55",
            "date_created_gmt": "2016-10-18T13:15:55",
            "date_modified": "2016-10-18T10:15:55",
            "date_modified_gmt": "2016-10-18T13:15:55",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/30-.png",
            "name": "30",
            "alt": "",
            "position": 0
          },
          {
            "id": 2825,
            "date_created": "2016-10-18T10:16:20",
            "date_created_gmt": "2016-10-18T13:16:20",
            "date_modified": "2016-10-18T10:16:20",
            "date_modified_gmt": "2016-10-18T13:16:20",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/30-1.png",
            "name": "30-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 406,
        "meta_data": [
          {
            "id": 10400,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10405,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10406,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10407,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10432,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10433,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10434,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10435,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10436,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10437,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10440,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2841"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2840,
        "name": "Vestido Liganete Plus Size REF:018 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref018-lote-10unid-r899-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref018-lote-10unid-r899-cada\/",
        "date_created": "2016-10-18T11:06:22",
        "date_created_gmt": "2016-10-18T11:06:22",
        "date_modified": "2018-07-18T07:56:41",
        "date_modified_gmt": "2018-07-18T07:56:41",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>&nbsp;<\/p>\n<p>Modelo da imagem veste 48<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "18",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 149,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.950",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          969,
          974,
          970,
          976,
          1028
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2822,
            "date_created": "2016-10-18T10:15:10",
            "date_created_gmt": "2016-10-18T13:15:10",
            "date_modified": "2016-10-18T10:15:10",
            "date_modified_gmt": "2016-10-18T13:15:10",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/18-.png",
            "name": "18",
            "alt": "",
            "position": 0
          },
          {
            "id": 2823,
            "date_created": "2016-10-18T10:15:32",
            "date_created_gmt": "2016-10-18T13:15:32",
            "date_modified": "2016-10-18T10:15:32",
            "date_modified_gmt": "2016-10-18T13:15:32",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/18-1.png",
            "name": "18-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 402,
        "meta_data": [
          {
            "id": 10358,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10363,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10364,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10365,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10390,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10391,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10392,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10393,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10394,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10395,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10398,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2840"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2839,
        "name": "Macaquito Liganete Plus Size REF:M15 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "macaquito-liganete-plus-size-refm15-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-plus-size-refm15-lote-10unid\/",
        "date_created": "2016-10-18T11:02:47",
        "date_created_gmt": "2016-10-18T11:02:47",
        "date_modified": "2018-07-18T07:56:41",
        "date_modified_gmt": "2018-07-18T07:56:41",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Macaquito Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>A modelo veste 48<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/p>\n",
        "short_description": "",
        "sku": "",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 57,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.850",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          965,
          1028,
          1030,
          970,
          969
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 24,
            "name": "Macaquito Plus Size",
            "slug": "macaquito-plus-size"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2835,
            "date_created": "2016-10-18T10:20:19",
            "date_created_gmt": "2016-10-18T13:20:19",
            "date_modified": "2016-10-18T10:20:19",
            "date_modified_gmt": "2016-10-18T13:20:19",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/m15-.png",
            "name": "m15",
            "alt": "",
            "position": 0
          },
          {
            "id": 2836,
            "date_created": "2016-10-18T10:20:47",
            "date_created_gmt": "2016-10-18T13:20:47",
            "date_modified": "2016-10-18T10:20:47",
            "date_modified_gmt": "2016-10-18T13:20:47",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/m15-1.png",
            "name": "m15-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 210,
        "meta_data": [
          {
            "id": 10316,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10321,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10322,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10323,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10348,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10349,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10350,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10351,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10352,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10353,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10356,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2839"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2838,
        "name": "Macaquito Liganete Plus Size REF:M11 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "macaquito-liganete-plus-size-refm11-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-plus-size-refm11-lote-10unid\/",
        "date_created": "2016-10-18T11:00:25",
        "date_created_gmt": "2016-10-18T11:00:25",
        "date_modified": "2018-07-25T21:52:39",
        "date_modified_gmt": "2018-07-25T21:52:39",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Macaquito Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>A modelo veste 48<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/p>\n",
        "short_description": "",
        "sku": "",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 102,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.850",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1030,
          979,
          1039,
          915,
          975
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 24,
            "name": "Macaquito Plus Size",
            "slug": "macaquito-plus-size"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2833,
            "date_created": "2016-10-18T10:19:34",
            "date_created_gmt": "2016-10-18T13:19:34",
            "date_modified": "2016-10-18T10:19:34",
            "date_modified_gmt": "2016-10-18T13:19:34",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/m11-.png",
            "name": "m11",
            "alt": "",
            "position": 0
          },
          {
            "id": 2834,
            "date_created": "2016-10-18T10:19:56",
            "date_created_gmt": "2016-10-18T13:19:56",
            "date_modified": "2016-10-18T10:19:56",
            "date_modified_gmt": "2016-10-18T13:19:56",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/m11-1.png",
            "name": "m11-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 208,
        "meta_data": [
          {
            "id": 10274,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10279,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10280,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10281,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10306,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10307,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10308,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10309,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10310,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10311,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10314,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2838"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2818,
        "name": "Macaquito Liganete Plus Size REF:M13 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "macaquito-liganete-plus-size-refm13-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-plus-size-refm13-lote-10unid\/",
        "date_created": "2016-10-18T10:55:43",
        "date_created_gmt": "2016-10-18T10:55:43",
        "date_modified": "2018-07-18T07:56:41",
        "date_modified_gmt": "2018-07-18T07:56:41",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Macaquito Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>&nbsp;<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>A modelo veste 48<\/p>\n<p>Veste do 40 ao 48<\/p>\n<p>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/p>\n",
        "short_description": "",
        "sku": "m13",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 3,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.850",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          968,
          974,
          966,
          1028,
          973
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 24,
            "name": "Macaquito Plus Size",
            "slug": "macaquito-plus-size"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2820,
            "date_created": "2016-10-18T10:14:24",
            "date_created_gmt": "2016-10-18T13:14:24",
            "date_modified": "2016-10-18T10:14:24",
            "date_modified_gmt": "2016-10-18T13:14:24",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/13-.png",
            "name": "13",
            "alt": "",
            "position": 0
          },
          {
            "id": 2821,
            "date_created": "2016-10-18T10:14:49",
            "date_created_gmt": "2016-10-18T13:14:49",
            "date_modified": "2016-10-18T10:14:49",
            "date_modified_gmt": "2016-10-18T13:14:49",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/10\/13-1.png",
            "name": "13-1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 209,
        "meta_data": [
          {
            "id": 10233,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10239,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10240,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10241,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10264,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 10265,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10266,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10267,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10268,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10269,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10272,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2818"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2634,
        "name": "Short Tactel REF:SHORTTactelCos-B049 <br \/>(lote 10 UN)<div id=\"destaque\">R$8,99 UN<\/div>",
        "slug": "short-tactel-refshorttactelcos-b049-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/short-tactel-refshorttactelcos-b049-lote-10-unid\/",
        "date_created": "2016-08-19T02:25:12",
        "date_created_gmt": "2016-08-19T02:25:12",
        "date_modified": "2018-07-18T07:56:41",
        "date_modified_gmt": "2018-07-18T07:56:41",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Short\u00a0masculino Tactel Cos barato de \u00f3tima qualidade, excelente para revenda.<\/p>\n<p>TAMANHOS:\u00a036 ao 46\u00a0<strong><em>(ATEN\u00c7\u00c3O! os tamanhos v\u00e3o sortidos)<\/em><\/strong><\/p>\n<p>100% Poliester<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "SHORTmasc-TactelCos-B049",
        "price": "89.90",
        "regular_price": "89.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>89,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 53,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.200",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1293,
          1301,
          3286,
          1295,
          4046
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          },
          {
            "id": 80,
            "name": "Shorts",
            "slug": "shots"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2630,
            "date_created": "2016-08-19T02:04:05",
            "date_created_gmt": "2016-08-19T05:04:05",
            "date_modified": "2016-08-19T02:04:05",
            "date_modified_gmt": "2016-08-19T05:04:05",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/SHORTmasc-TactelCos-B049.jpg",
            "name": "SHORTmasc-TactelCos-B049",
            "alt": "",
            "position": 0
          },
          {
            "id": 2629,
            "date_created": "2016-08-19T02:03:56",
            "date_created_gmt": "2016-08-19T05:03:56",
            "date_modified": "2016-08-19T02:03:56",
            "date_modified_gmt": "2016-08-19T05:03:56",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/SHORTmasc-TactelCos-B049.2.jpg",
            "name": "SHORTmasc-TactelCos-B049.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 345,
        "meta_data": [
          {
            "id": 10190,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10195,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10196,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10197,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10222,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10223,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10224,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "80"
          },
          {
            "id": 10225,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10226,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10231,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2634"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2633,
        "name": "Short Tactel REF:shottactelmascS046 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,99 UN<\/div>",
        "slug": "short-tactel-refshottactelmascs046-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/short-tactel-refshottactelmascs046-lote-10-unid\/",
        "date_created": "2016-08-19T02:15:31",
        "date_created_gmt": "2016-08-19T02:15:31",
        "date_modified": "2018-08-01T14:33:20",
        "date_modified_gmt": "2018-08-01T14:33:20",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Short\u00a0masculino Tactel barato de \u00f3tima qualidade, excelente para revenda.<\/p>\n<p>TAMANHOS: P\/M\/G\/GG<strong>\u00a0<em>(ATEN\u00c7\u00c3O! os tamanhos v\u00e3o sortidos por exemplo: 3P,3M,3G,1GG)<\/em><\/strong><\/p>\n<p>Medidas do Modelo<\/p>\n<p>&nbsp;<\/p>\n<p>Modelo da foto veste tamanho 38<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "shottactelmascS046",
        "price": "79.90",
        "regular_price": "79.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>79,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 38,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.200",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1300,
          1295,
          1293,
          1335,
          1301
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 35,
            "name": "MASCULINO",
            "slug": "masculino"
          },
          {
            "id": 80,
            "name": "Shorts",
            "slug": "shots"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2631,
            "date_created": "2016-08-19T02:04:13",
            "date_created_gmt": "2016-08-19T05:04:13",
            "date_modified": "2016-08-19T02:04:13",
            "date_modified_gmt": "2016-08-19T05:04:13",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/shottactelmascS046.2.jpg",
            "name": "shottactelmascS046.2",
            "alt": "",
            "position": 0
          },
          {
            "id": 2632,
            "date_created": "2016-08-19T02:04:22",
            "date_created_gmt": "2016-08-19T05:04:22",
            "date_modified": "2016-08-19T02:04:22",
            "date_modified_gmt": "2016-08-19T05:04:22",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/shottactelmascS046.jpg",
            "name": "shottactelmascS046",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 346,
        "meta_data": [
          {
            "id": 10148,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10153,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10154,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10155,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10180,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10181,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10182,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "58"
          },
          {
            "id": 10183,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10184,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10189,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2633"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2627,
        "name": "Camisa Polo Feminina REF: polofemalgodao <br \/>(lote 10 UN)<div id=\"destaque\">R$6,50 UN<\/div>",
        "slug": "camisa-polo-feminina-ref-polofemalgodao-lote-10unid-r650-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/camisa-polo-feminina-ref-polofemalgodao-lote-10unid-r650-cada\/",
        "date_created": "2016-08-19T02:04:57",
        "date_created_gmt": "2016-08-19T02:04:57",
        "date_modified": "2018-07-18T07:56:42",
        "date_modified_gmt": "2018-07-18T07:56:42",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Camisa masculina barata de \u00f3tima qualidade, excelente para revenda.<\/p>\n<p>TAMANHOS: P\/M\/G<strong> <em>(ATEN\u00c7\u00c3O! os tamanhos v\u00e3o sortidos por exemplo: 3P,3M,4G)<\/em><\/strong><\/p>\n<p>Medidas do Modelo<\/p>\n<p>&nbsp;<\/p>\n<p>modelo da foto veste P<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "golapolofemalgodao",
        "price": "65.00",
        "regular_price": "65.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>65,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 182,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.400",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          976,
          912,
          967,
          915,
          965
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 53,
            "name": "Camisa polo feminina",
            "slug": "camisa-polo-feminina"
          },
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2628,
            "date_created": "2016-08-19T02:03:48",
            "date_created_gmt": "2016-08-19T05:03:48",
            "date_modified": "2016-08-19T02:03:48",
            "date_modified_gmt": "2016-08-19T05:03:48",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/golapolofemalgodao.jpg",
            "name": "golapolofemalgodao",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 148,
        "meta_data": [
          {
            "id": 10106,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10111,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10112,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10113,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10138,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10139,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10140,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10141,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10142,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10147,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2627"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2623,
        "name": "Macaquito infantil REF:Macaquitofeminf01 <br \/>(lote 10 UN)<div id=\"destaque\">R$6,50 UN<\/div>",
        "slug": "macaquito-infantil-refmacaquitofeminf01-lote-10unid-r650-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-infantil-refmacaquitofeminf01-lote-10unid-r650-cada\/",
        "date_created": "2016-08-19T01:50:27",
        "date_created_gmt": "2016-08-19T01:50:27",
        "date_modified": "2018-07-18T07:56:42",
        "date_modified_gmt": "2018-07-18T07:56:42",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito infantil de excelente qualidade.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 95% Poli\u00e9ster 5% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "Macaquitofeminf01",
        "price": "65.00",
        "regular_price": "65.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>65,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 125,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.400",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1039,
          974,
          964,
          1038,
          973
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 52,
            "name": "Macaquito infantil",
            "slug": "macaquito-infantil"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2626,
            "date_created": "2016-08-19T01:46:08",
            "date_created_gmt": "2016-08-19T04:46:08",
            "date_modified": "2016-08-19T01:46:08",
            "date_modified_gmt": "2016-08-19T04:46:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/Macaquitofeminf01.jpg",
            "name": "Macaquitofeminf01",
            "alt": "",
            "position": 0
          },
          {
            "id": 2624,
            "date_created": "2016-08-19T01:45:48",
            "date_created_gmt": "2016-08-19T04:45:48",
            "date_modified": "2016-08-19T01:45:48",
            "date_modified_gmt": "2016-08-19T04:45:48",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/Macaquitofeminf01.2.jpg",
            "name": "Macaquitofeminf01.2",
            "alt": "",
            "position": 1
          },
          {
            "id": 2625,
            "date_created": "2016-08-19T01:45:58",
            "date_created_gmt": "2016-08-19T04:45:58",
            "date_modified": "2016-08-19T01:45:58",
            "date_modified_gmt": "2016-08-19T04:45:58",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/Macaquitofeminf01.3.jpg",
            "name": "Macaquitofeminf01.3",
            "alt": "",
            "position": 2
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 207,
        "meta_data": [
          {
            "id": 10064,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10069,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10070,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10071,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10096,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10097,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10098,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 10099,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10100,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10105,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2623"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2619,
        "name": "Macaquito Liganete REF:M21 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm21-lote-10unid-r740-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm21-lote-10unid-r740-cada\/",
        "date_created": "2016-08-19T01:38:55",
        "date_created_gmt": "2016-08-19T01:38:55",
        "date_modified": "2018-07-18T07:56:42",
        "date_modified_gmt": "2018-07-18T07:56:42",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M21",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 35,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1038,
          967,
          969,
          980,
          1040
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2621,
            "date_created": "2016-08-19T01:38:32",
            "date_created_gmt": "2016-08-19T04:38:32",
            "date_modified": "2016-08-19T01:38:32",
            "date_modified_gmt": "2016-08-19T04:38:32",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/m21.jpg",
            "name": "m21",
            "alt": "",
            "position": 0
          },
          {
            "id": 2620,
            "date_created": "2016-08-19T01:38:21",
            "date_created_gmt": "2016-08-19T04:38:21",
            "date_modified": "2016-08-19T01:38:21",
            "date_modified_gmt": "2016-08-19T04:38:21",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/m21.2.jpg",
            "name": "m21.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 227,
        "meta_data": [
          {
            "id": 10022,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 10027,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 10028,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 10029,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10054,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10055,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10056,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10057,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10058,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10063,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2619"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2616,
        "name": "Macaquito Liganete REF:M14 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm14-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm14-lote-10unid\/",
        "date_created": "2016-08-19T01:36:50",
        "date_created_gmt": "2016-08-19T01:36:50",
        "date_modified": "2018-07-18T07:56:42",
        "date_modified_gmt": "2018-07-18T07:56:42",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>&nbsp;<\/p>\n<p>modelo da foto veste 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "m14",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 50,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1030,
          915,
          980,
          1036,
          973
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2618,
            "date_created": "2016-08-19T01:35:52",
            "date_created_gmt": "2016-08-19T04:35:52",
            "date_modified": "2016-08-19T01:35:52",
            "date_modified_gmt": "2016-08-19T04:35:52",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/m14.jpg",
            "name": "Macaquinho liganete m14",
            "alt": "",
            "position": 0
          },
          {
            "id": 2617,
            "date_created": "2016-08-19T01:35:42",
            "date_created_gmt": "2016-08-19T04:35:42",
            "date_modified": "2016-08-19T01:35:42",
            "date_modified_gmt": "2016-08-19T04:35:42",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/m14.2.jpg",
            "name": "m14.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 218,
        "meta_data": [
          {
            "id": 9980,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9987,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9988,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9989,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 10014,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 10015,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 10016,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 10017,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 10018,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 10021,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2616"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2613,
        "name": "Short Tecido REF:shortsaiatecido <br \/>(lote 10 UN)<div id=\"destaque\">R$7,80 UN<\/div>",
        "slug": "short-tecido-refshortsaiatecido-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/short-tecido-refshortsaiatecido-lote-10unid\/",
        "date_created": "2016-08-18T21:16:03",
        "date_created_gmt": "2016-08-18T21:16:03",
        "date_modified": "2018-07-18T07:56:43",
        "date_modified_gmt": "2018-07-18T07:56:43",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um short tecido saia de excelente qualidade .<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o:\u00a0100% Poliester<\/p>\n<p>Modelo da foto veste tamanho 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "shortsaiatecido",
        "price": "78.00",
        "regular_price": "78.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>78,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 24,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.100",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          975,
          964,
          968,
          1033,
          967
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 48,
            "name": "Shorts",
            "slug": "shorts-masculino"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2837,
            "date_created": "2016-10-18T10:28:59",
            "date_created_gmt": "2016-10-18T13:28:59",
            "date_modified": "2016-10-18T10:28:59",
            "date_modified_gmt": "2016-10-18T13:28:59",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/shortsaiatecido.jpg",
            "name": "shortsaiatecido",
            "alt": "",
            "position": 0
          },
          {
            "id": 2614,
            "date_created": "2016-08-18T21:14:35",
            "date_created_gmt": "2016-08-19T00:14:35",
            "date_modified": "2016-08-18T21:14:35",
            "date_modified_gmt": "2016-08-19T00:14:35",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/shortsaiatecido2.jpg",
            "name": "shortsaiatecido2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 349,
        "meta_data": [
          {
            "id": 9938,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9943,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9944,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9945,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9970,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9971,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9972,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 9973,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9974,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9978,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2613"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2610,
        "name": "Short Tecido REF:shortcoslargo3botoes <br \/>(lote 10 UN)<div id=\"destaque\">R$7,70 UN<\/div>",
        "slug": "short-tecido-refshortcoslargo3botoes-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/short-tecido-refshortcoslargo3botoes-lote-10unid\/",
        "date_created": "2016-08-18T21:10:33",
        "date_created_gmt": "2016-08-18T21:10:33",
        "date_modified": "2018-07-18T07:56:43",
        "date_modified_gmt": "2018-07-18T07:56:43",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um short tecido\u00a0de excelente qualidade .<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o:\u00a0100% Poliester<\/p>\n<p>Modelo da foto veste tamanho 38<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "shortcoslargo3botoes",
        "price": "77.00",
        "regular_price": "77.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>77,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 74,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.050",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          915,
          912,
          966,
          974,
          976
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 48,
            "name": "Shorts",
            "slug": "shorts-masculino"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2605,
            "date_created": "2016-08-18T02:12:14",
            "date_created_gmt": "2016-08-18T05:12:14",
            "date_modified": "2016-08-18T02:12:14",
            "date_modified_gmt": "2016-08-18T05:12:14",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/fundo-das-fotos-do-rei1.jpg",
            "name": "fundo-das-fotos-do-rei1",
            "alt": "",
            "position": 0
          },
          {
            "id": 2606,
            "date_created": "2016-08-18T02:12:26",
            "date_created_gmt": "2016-08-18T05:12:26",
            "date_modified": "2016-08-18T02:12:26",
            "date_modified_gmt": "2016-08-18T05:12:26",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/fundo-das-fotos-do-rei3.jpg",
            "name": "fundo-das-fotos-do-rei3",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 348,
        "meta_data": [
          {
            "id": 9896,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9901,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9902,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9903,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9928,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9929,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9930,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 9931,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9932,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9936,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2610"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2583,
        "name": "Vestido Infantil REF: VINF.22 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-22-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-22-lote-10-unid\/",
        "date_created": "2016-08-17T22:48:55",
        "date_created_gmt": "2016-08-17T22:48:55",
        "date_modified": "2018-07-18T07:56:43",
        "date_modified_gmt": "2018-07-18T07:56:43",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.22",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 11,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.500",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          912,
          980,
          964,
          968,
          966
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2585,
            "date_created": "2016-08-17T22:47:19",
            "date_created_gmt": "2016-08-18T01:47:19",
            "date_modified": "2016-08-17T22:47:19",
            "date_modified_gmt": "2016-08-18T01:47:19",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.22.jpg",
            "name": "vestidoinf.22",
            "alt": "",
            "position": 0
          },
          {
            "id": 2584,
            "date_created": "2016-08-17T22:47:08",
            "date_created_gmt": "2016-08-18T01:47:08",
            "date_modified": "2016-08-17T22:47:08",
            "date_modified_gmt": "2016-08-18T01:47:08",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.22.2.jpg",
            "name": "vestidoinf.22.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 379,
        "meta_data": [
          {
            "id": 9854,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9859,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9860,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9861,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9886,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9887,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9888,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9889,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9890,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9895,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2583"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2579,
        "name": "Vestido Infantil REF: VINF.23 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-23-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-23-lote-10-unid\/",
        "date_created": "2016-08-17T22:45:24",
        "date_created_gmt": "2016-08-17T22:45:24",
        "date_modified": "2018-07-18T07:56:43",
        "date_modified_gmt": "2018-07-18T07:56:43",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.23",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 27,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.500",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          979,
          912,
          968,
          1033,
          974
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2582,
            "date_created": "2016-08-17T22:43:58",
            "date_created_gmt": "2016-08-18T01:43:58",
            "date_modified": "2016-08-17T22:43:58",
            "date_modified_gmt": "2016-08-18T01:43:58",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.23.jpg",
            "name": "vestidoinf.23",
            "alt": "",
            "position": 0
          },
          {
            "id": 2581,
            "date_created": "2016-08-17T22:43:46",
            "date_created_gmt": "2016-08-18T01:43:46",
            "date_modified": "2016-08-17T22:43:46",
            "date_modified_gmt": "2016-08-18T01:43:46",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.23.3.jpg",
            "name": "vestidoinf.23.3",
            "alt": "",
            "position": 1
          },
          {
            "id": 2580,
            "date_created": "2016-08-17T22:43:34",
            "date_created_gmt": "2016-08-18T01:43:34",
            "date_modified": "2016-08-17T22:43:34",
            "date_modified_gmt": "2016-08-18T01:43:34",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.23.2.jpg",
            "name": "vestidoinf.23.2",
            "alt": "",
            "position": 2
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 380,
        "meta_data": [
          {
            "id": 9812,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9817,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9818,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9819,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9844,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9845,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9846,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9847,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9848,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9852,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2579"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2576,
        "name": "Vestido Infantil REF: VINF.24 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-24-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-24-lote-10-unid\/",
        "date_created": "2016-08-17T22:41:22",
        "date_created_gmt": "2016-08-17T22:41:22",
        "date_modified": "2018-07-18T07:56:44",
        "date_modified_gmt": "2018-07-18T07:56:44",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.24",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 32,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.500",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1030,
          964,
          965,
          968,
          974
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2578,
            "date_created": "2016-08-17T22:40:23",
            "date_created_gmt": "2016-08-18T01:40:23",
            "date_modified": "2016-08-17T22:40:23",
            "date_modified_gmt": "2016-08-18T01:40:23",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.24.jpg",
            "name": "vestidoinf.24",
            "alt": "",
            "position": 0
          },
          {
            "id": 2577,
            "date_created": "2016-08-17T22:40:13",
            "date_created_gmt": "2016-08-18T01:40:13",
            "date_modified": "2016-08-17T22:40:13",
            "date_modified_gmt": "2016-08-18T01:40:13",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.24.2.jpg",
            "name": "vestidoinf.24.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 381,
        "meta_data": [
          {
            "id": 9770,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9775,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9776,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9777,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9802,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9803,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9804,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9805,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9806,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9810,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2576"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2573,
        "name": "Vestido Infantil REF: VINF.25 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-25-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-25-lote-10-unid\/",
        "date_created": "2016-08-17T22:37:56",
        "date_created_gmt": "2016-08-17T22:37:56",
        "date_modified": "2018-07-18T07:56:44",
        "date_modified_gmt": "2018-07-18T07:56:44",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.25",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 20,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.500",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1314,
          1217,
          1223,
          1234,
          1311
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2575,
            "date_created": "2016-08-17T22:35:50",
            "date_created_gmt": "2016-08-18T01:35:50",
            "date_modified": "2016-08-17T22:35:50",
            "date_modified_gmt": "2016-08-18T01:35:50",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.25.jpg",
            "name": "vestidoinf.25",
            "alt": "",
            "position": 0
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 382,
        "meta_data": [
          {
            "id": 9729,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9734,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9735,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9736,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9761,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9762,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9763,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9764,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9765,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9766,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2573"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2570,
        "name": "Vestido Infantil REF: VINF.26 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-26-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-26-lote-10-unid\/",
        "date_created": "2016-08-17T22:31:35",
        "date_created_gmt": "2016-08-17T22:31:35",
        "date_modified": "2018-07-18T08:15:49",
        "date_modified_gmt": "2018-07-18T08:15:49",
        "type": "simple",
        "status": "publish",
        "featured": true,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.26",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 39,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1038,
          1036,
          1030,
          980,
          1035
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2572,
            "date_created": "2016-08-17T22:31:06",
            "date_created_gmt": "2016-08-18T01:31:06",
            "date_modified": "2016-08-17T22:31:06",
            "date_modified_gmt": "2016-08-18T01:31:06",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.26.jpg",
            "name": "vestidoinf.26",
            "alt": "",
            "position": 0
          },
          {
            "id": 2571,
            "date_created": "2016-08-17T22:30:56",
            "date_created_gmt": "2016-08-18T01:30:56",
            "date_modified": "2016-08-17T22:30:56",
            "date_modified_gmt": "2016-08-18T01:30:56",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.26.2.jpg",
            "name": "vestidoinf.26.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 383,
        "meta_data": [
          {
            "id": 9687,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9692,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9693,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9694,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9719,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9720,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9721,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9722,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9723,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9727,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2570"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2567,
        "name": "Vestido Infantil REF: VINF.27 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-27-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-27-lote-10-unid\/",
        "date_created": "2016-08-17T22:28:07",
        "date_created_gmt": "2016-08-17T22:28:07",
        "date_modified": "2018-07-18T07:56:44",
        "date_modified_gmt": "2018-07-18T07:56:44",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.27",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 25,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          976,
          964,
          973,
          970,
          972
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2569,
            "date_created": "2016-08-17T22:26:54",
            "date_created_gmt": "2016-08-18T01:26:54",
            "date_modified": "2016-08-17T22:26:54",
            "date_modified_gmt": "2016-08-18T01:26:54",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.27.jpg",
            "name": "vestidoinf.27",
            "alt": "",
            "position": 0
          },
          {
            "id": 2568,
            "date_created": "2016-08-17T22:26:00",
            "date_created_gmt": "2016-08-18T01:26:00",
            "date_modified": "2016-08-17T22:26:00",
            "date_modified_gmt": "2016-08-18T01:26:00",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.27.2.jpg",
            "name": "vestidoinf.27.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 384,
        "meta_data": [
          {
            "id": 9645,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9650,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9651,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9652,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9677,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9678,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9679,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9680,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9681,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9686,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2567"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2564,
        "name": "Vestido Infantil REF: VINF.28 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-29-lote-10-unid-2",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-29-lote-10-unid-2\/",
        "date_created": "2016-08-17T22:20:03",
        "date_created_gmt": "2016-08-17T22:20:03",
        "date_modified": "2018-07-18T07:56:44",
        "date_modified_gmt": "2018-07-18T07:56:44",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.28",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 14,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1033,
          912,
          975,
          974,
          965
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2566,
            "date_created": "2016-08-17T22:19:29",
            "date_created_gmt": "2016-08-18T01:19:29",
            "date_modified": "2016-08-17T22:19:29",
            "date_modified_gmt": "2016-08-18T01:19:29",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.28.jpg",
            "name": "vestidoinf.28",
            "alt": "",
            "position": 0
          },
          {
            "id": 2565,
            "date_created": "2016-08-17T22:19:16",
            "date_created_gmt": "2016-08-18T01:19:16",
            "date_modified": "2016-08-17T22:19:16",
            "date_modified_gmt": "2016-08-18T01:19:16",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.28.2.jpg",
            "name": "vestidoinf.28.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 385,
        "meta_data": [
          {
            "id": 9603,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9608,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9609,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9610,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9635,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9636,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9637,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9638,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9639,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9643,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2564"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2561,
        "name": "Vestido Infantil REF: VINF.29 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-29-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-29-lote-10-unid\/",
        "date_created": "2016-08-17T22:16:35",
        "date_created_gmt": "2016-08-17T22:16:35",
        "date_modified": "2018-07-18T07:56:44",
        "date_modified_gmt": "2018-07-18T07:56:44",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00fanico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.29",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 61,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1028,
          976,
          967,
          1038,
          1030
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2563,
            "date_created": "2016-08-17T22:14:49",
            "date_created_gmt": "2016-08-18T01:14:49",
            "date_modified": "2016-08-17T22:14:49",
            "date_modified_gmt": "2016-08-18T01:14:49",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.29.jpg",
            "name": "vestidoinf.29",
            "alt": "",
            "position": 0
          },
          {
            "id": 2562,
            "date_created": "2016-08-17T22:13:35",
            "date_created_gmt": "2016-08-18T01:13:35",
            "date_modified": "2016-08-17T22:13:35",
            "date_modified_gmt": "2016-08-18T01:13:35",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.29.2.jpg",
            "name": "vestidoinf.29.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 386,
        "meta_data": [
          {
            "id": 9561,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9566,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9567,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9568,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9593,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9594,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9595,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9596,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9597,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9601,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2561"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2558,
        "name": "Vestido Infantil REF: VINF.30 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-30-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-30-lote-10-unid\/",
        "date_created": "2016-08-17T22:06:56",
        "date_created_gmt": "2016-08-17T22:06:56",
        "date_modified": "2018-07-18T07:56:45",
        "date_modified_gmt": "2018-07-18T07:56:45",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00danico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.30",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 20,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1039,
          1040,
          964,
          912,
          1036
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2560,
            "date_created": "2016-08-17T22:06:00",
            "date_created_gmt": "2016-08-18T01:06:00",
            "date_modified": "2016-08-17T22:06:00",
            "date_modified_gmt": "2016-08-18T01:06:00",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.30.jpg",
            "name": "vestidoinf.30",
            "alt": "",
            "position": 0
          },
          {
            "id": 2559,
            "date_created": "2016-08-17T22:05:48",
            "date_created_gmt": "2016-08-18T01:05:48",
            "date_modified": "2016-08-17T22:05:48",
            "date_modified_gmt": "2016-08-18T01:05:48",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.30.2.jpg",
            "name": "vestidoinf.30.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 387,
        "meta_data": [
          {
            "id": 9519,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9524,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9525,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9526,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9551,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9552,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9553,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9554,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9555,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9560,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2558"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2554,
        "name": "Vestido Infantil REF: VINF.31 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,35 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-31-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-31-lote-10-unid\/",
        "date_created": "2016-08-17T21:59:57",
        "date_created_gmt": "2016-08-17T21:59:57",
        "date_modified": "2018-07-18T07:56:45",
        "date_modified_gmt": "2018-07-18T07:56:45",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>5%\u00a0Elastano<\/p>\n<p>95% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p>Tamanho \u00danico<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.31",
        "price": "73.50",
        "regular_price": "73.50",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>73,50<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 24,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          978,
          912,
          1028,
          1040,
          915
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2556,
            "date_created": "2016-08-17T21:58:54",
            "date_created_gmt": "2016-08-18T00:58:54",
            "date_modified": "2016-08-17T21:58:54",
            "date_modified_gmt": "2016-08-18T00:58:54",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.31.jpg",
            "name": "vestidoinf.31",
            "alt": "",
            "position": 0
          },
          {
            "id": 2555,
            "date_created": "2016-08-17T21:58:42",
            "date_created_gmt": "2016-08-18T00:58:42",
            "date_modified": "2016-08-17T21:58:42",
            "date_modified_gmt": "2016-08-18T00:58:42",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.31.2.jpg",
            "name": "vestidoinf.31.2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 388,
        "meta_data": [
          {
            "id": 9477,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9482,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9483,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9484,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9509,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9510,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9511,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9512,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9513,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9518,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2554"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2551,
        "name": "Vestido Infantil REF: VINF.IRM300 <br \/>(lote 10 UN)<div id=\"destaque\">R$5,99 UN<\/div>",
        "slug": "vestido-infantil-ref-vinf-irm300-lote-10-unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-infantil-ref-vinf-irm300-lote-10-unid\/",
        "date_created": "2016-08-17T21:50:19",
        "date_created_gmt": "2016-08-17T21:50:19",
        "date_modified": "2018-07-18T07:56:45",
        "date_modified_gmt": "2018-07-18T07:56:45",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>Vestido Infantil \u00a0barato de \u00f3tima qualidade, excelente para revender.<\/p>\n<p>50% Algod\u00e3o<\/p>\n<p>50% Poliester<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Veste tamanho 6 e 7 anos<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma cor da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo cores variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "vestidoinf.IRM300",
        "price": "59.90",
        "regular_price": "59.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>59,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 11,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": false,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          969,
          979,
          1039,
          1036,
          964
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 45,
            "name": "Feminino",
            "slug": "feminino-infantil"
          },
          {
            "id": 34,
            "name": "INFANTIL",
            "slug": "infantil"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 49,
            "name": "Vestido",
            "slug": "vestido"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2553,
            "date_created": "2016-08-17T21:48:13",
            "date_created_gmt": "2016-08-18T00:48:13",
            "date_modified": "2016-08-17T21:48:13",
            "date_modified_gmt": "2016-08-18T00:48:13",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.IRM300.jpg",
            "name": "VESTIDO INFANTIL ATACADO vestidoinf.IRM300",
            "alt": "",
            "position": 0
          },
          {
            "id": 2552,
            "date_created": "2016-08-17T21:48:01",
            "date_created_gmt": "2016-08-18T00:48:01",
            "date_modified": "2016-08-17T21:48:01",
            "date_modified_gmt": "2016-08-18T00:48:01",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/vestidoinf.IRM300.1.jpg",
            "name": "vestidoinf.IRM300.1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 389,
        "meta_data": [
          {
            "id": 9435,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9440,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9441,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9442,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9467,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9468,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9469,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "66"
          },
          {
            "id": 9470,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9471,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9475,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2551"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2548,
        "name": "Vestido REF: elastexs <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-ref-elastexs-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-ref-elastexs-lote-10unid\/",
        "date_created": "2016-08-17T21:19:43",
        "date_created_gmt": "2016-08-17T21:19:43",
        "date_modified": "2018-07-18T07:56:45",
        "date_modified_gmt": "2018-07-18T07:56:45",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido Elastexs.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "elastexs",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 14,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1560",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          915,
          1038,
          978,
          1036,
          965
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2550,
            "date_created": "2016-08-17T21:18:20",
            "date_created_gmt": "2016-08-18T00:18:20",
            "date_modified": "2016-08-17T21:18:20",
            "date_modified_gmt": "2016-08-18T00:18:20",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/elastexs2.jpg",
            "name": "vestido elastexs2",
            "alt": "",
            "position": 0
          },
          {
            "id": 2549,
            "date_created": "2016-08-17T21:18:06",
            "date_created_gmt": "2016-08-18T00:18:06",
            "date_modified": "2016-08-17T21:18:06",
            "date_modified_gmt": "2016-08-18T00:18:06",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/elastexs1.jpg",
            "name": "elastexs1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 506,
        "meta_data": [
          {
            "id": 9393,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9398,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9399,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9400,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9425,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9426,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9427,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 9428,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9429,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9434,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2548"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2545,
        "name": "Vestido Liganete REF:V10 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refv10-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refv10-lote-10unid\/",
        "date_created": "2016-08-17T18:47:04",
        "date_created_gmt": "2016-08-17T18:47:04",
        "date_modified": "2018-07-18T07:56:45",
        "date_modified_gmt": "2018-07-18T07:56:45",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "V10",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 22,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.500",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          964,
          967,
          965,
          976,
          1028
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2547,
            "date_created": "2016-08-17T18:46:01",
            "date_created_gmt": "2016-08-17T21:46:01",
            "date_modified": "2016-08-17T18:46:01",
            "date_modified_gmt": "2016-08-17T21:46:01",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/v10.jpg",
            "name": "Vestido Liganete v10",
            "alt": "",
            "position": 0
          },
          {
            "id": 2546,
            "date_created": "2016-08-17T18:45:49",
            "date_created_gmt": "2016-08-17T21:45:49",
            "date_modified": "2016-08-17T18:45:49",
            "date_modified_gmt": "2016-08-17T21:45:49",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/v10.1.jpg",
            "name": "v10.1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 464,
        "meta_data": [
          {
            "id": 9351,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9356,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9357,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9358,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9383,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9384,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9385,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 9386,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9387,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9392,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2545"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2542,
        "name": "Vestido Liganete REF:V32 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refv32-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refv32-lote-10unid\/",
        "date_created": "2016-08-17T18:42:23",
        "date_created_gmt": "2016-08-17T18:42:23",
        "date_modified": "2018-07-18T07:56:46",
        "date_modified_gmt": "2018-07-18T07:56:46",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "V32",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 30,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.560",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          915,
          1028,
          964,
          969,
          976
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2544,
            "date_created": "2016-08-17T18:35:52",
            "date_created_gmt": "2016-08-17T21:35:52",
            "date_modified": "2016-08-17T18:35:52",
            "date_modified_gmt": "2016-08-17T21:35:52",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/v32.jpg",
            "name": "Vestido Liganete v32",
            "alt": "",
            "position": 0
          },
          {
            "id": 2543,
            "date_created": "2016-08-17T18:35:39",
            "date_created_gmt": "2016-08-17T21:35:39",
            "date_modified": "2016-08-17T18:35:39",
            "date_modified_gmt": "2016-08-17T21:35:39",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/v32.1.jpg",
            "name": "v32.1",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 469,
        "meta_data": [
          {
            "id": 9309,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9314,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9315,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9316,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9341,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9342,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9343,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 9344,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9345,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9350,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2542"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2539,
        "name": "Vestido Liganete REF:V14bico <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "vestido-liganete-refv14bico-lote-10unid",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-refv14bico-lote-10unid\/",
        "date_created": "2016-08-17T18:31:36",
        "date_created_gmt": "2016-08-17T18:31:36",
        "date_modified": "2018-07-18T08:15:49",
        "date_modified_gmt": "2018-07-18T08:15:49",
        "type": "simple",
        "status": "publish",
        "featured": true,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas. Esse tecido \u00e9 uma malha fria que n\u00e3o desfia e estica bem.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano.<\/p>\n<p>&nbsp;<\/p>\n<p>A modelo da foto veste tamanho 38.<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 36 ao 40<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "V14bico",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 94,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.500",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          973,
          1039,
          980,
          1038,
          967
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 20,
            "name": "LOJA DE 10 REAIS",
            "slug": "loja-de-10-reais"
          },
          {
            "id": 25,
            "name": "Vestido Liganete Curto",
            "slug": "vestido-liganete"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2540,
            "date_created": "2016-08-17T16:36:20",
            "date_created_gmt": "2016-08-17T19:36:20",
            "date_modified": "2016-08-17T16:36:20",
            "date_modified_gmt": "2016-08-17T19:36:20",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/v14bico.jpg",
            "name": "vestido liganete v14bico",
            "alt": "",
            "position": 0
          },
          {
            "id": 2541,
            "date_created": "2016-08-17T16:36:34",
            "date_created_gmt": "2016-08-17T19:36:34",
            "date_modified": "2016-08-17T16:36:34",
            "date_modified_gmt": "2016-08-17T19:36:34",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/08\/v14bico2.jpg",
            "name": "v14bico2",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 465,
        "meta_data": [
          {
            "id": 9267,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9272,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9273,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9274,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9299,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9300,
            "key": "_yoast_wpseo_primary_brand",
            "value": ""
          },
          {
            "id": 9301,
            "key": "_yoast_wpseo_primary_product_cat",
            "value": "23"
          },
          {
            "id": 9302,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9303,
            "key": "_yoast_wpseo_content_score",
            "value": "60"
          },
          {
            "id": 9308,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2539"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2187,
        "name": "Vestido Liganete Plus Size REF:051 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref051",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref051\/",
        "date_created": "2016-04-11T02:33:42",
        "date_created_gmt": "2016-04-11T02:33:42",
        "date_modified": "2018-07-18T07:56:46",
        "date_modified_gmt": "2018-07-18T07:56:46",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.67<\/p>\n<p>Busto- 84<\/p>\n<p>Cintura- 78<\/p>\n<p>Quadril- 110<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 44<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VGG51",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 76,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          912,
          1038,
          976,
          1028,
          966
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2180,
            "date_created": "2016-04-11T02:06:56",
            "date_created_gmt": "2016-04-11T05:06:56",
            "date_modified": "2016-04-11T02:06:56",
            "date_modified_gmt": "2016-04-11T05:06:56",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG51.png",
            "name": "GG51",
            "alt": "",
            "position": 0
          },
          {
            "id": 2181,
            "date_created": "2016-04-11T02:07:20",
            "date_created_gmt": "2016-04-11T05:07:20",
            "date_modified": "2016-04-11T02:07:20",
            "date_modified_gmt": "2016-04-11T05:07:20",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG51TRAS.png",
            "name": "GG51TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 417,
        "meta_data": [
          {
            "id": 9228,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9233,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9234,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9235,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9260,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9261,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9262,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9266,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2187"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2186,
        "name": "Vestido Liganete Plus Size REF:047 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref047-lote-10unid-r899-cada",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref047-lote-10unid-r899-cada\/",
        "date_created": "2016-04-11T02:31:32",
        "date_created_gmt": "2016-04-11T02:31:32",
        "date_modified": "2018-07-18T07:56:47",
        "date_modified_gmt": "2018-07-18T07:56:47",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.67<\/p>\n<p>Busto- 84<\/p>\n<p>Cintura- 78<\/p>\n<p>Quadril- 110<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 44<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VGG47",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 62,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1039,
          965,
          1030,
          967,
          972
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2178,
            "date_created": "2016-04-11T02:06:07",
            "date_created_gmt": "2016-04-11T05:06:07",
            "date_modified": "2016-04-11T02:06:07",
            "date_modified_gmt": "2016-04-11T05:06:07",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG47.png",
            "name": "GG47",
            "alt": "",
            "position": 0
          },
          {
            "id": 2179,
            "date_created": "2016-04-11T02:06:29",
            "date_created_gmt": "2016-04-11T05:06:29",
            "date_modified": "2016-04-11T02:06:29",
            "date_modified_gmt": "2016-04-11T05:06:29",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG47TRAS.png",
            "name": "GG47TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 415,
        "meta_data": [
          {
            "id": 9189,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9194,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9195,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9196,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9221,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9222,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9223,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9227,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2186"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2185,
        "name": "Vestido Liganete Plus Size REF:027 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref027",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref027\/",
        "date_created": "2016-04-11T02:29:32",
        "date_created_gmt": "2016-04-11T02:29:32",
        "date_modified": "2018-07-18T07:56:47",
        "date_modified_gmt": "2018-07-18T07:56:47",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.67<\/p>\n<p>Busto- 84<\/p>\n<p>Cintura- 78<\/p>\n<p>Quadril- 110<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 44<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VGG27",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 67,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.600",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1039,
          980,
          968,
          972,
          974
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2176,
            "date_created": "2016-04-11T02:05:26",
            "date_created_gmt": "2016-04-11T05:05:26",
            "date_modified": "2016-04-11T02:05:26",
            "date_modified_gmt": "2016-04-11T05:05:26",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG27.png",
            "name": "GG27",
            "alt": "",
            "position": 0
          },
          {
            "id": 2177,
            "date_created": "2016-04-11T02:05:45",
            "date_created_gmt": "2016-04-11T05:05:45",
            "date_modified": "2016-04-11T02:05:45",
            "date_modified_gmt": "2016-04-11T05:05:45",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG27TRAS.png",
            "name": "GG27TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 404,
        "meta_data": [
          {
            "id": 9150,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9155,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9156,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9157,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9182,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9183,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9184,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9188,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2185"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2183,
        "name": "Vestido Liganete Plus Size REF:014 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref014-2",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref014-2\/",
        "date_created": "2016-04-11T02:27:07",
        "date_created_gmt": "2016-04-11T02:27:07",
        "date_modified": "2018-07-18T07:56:47",
        "date_modified_gmt": "2018-07-18T07:56:47",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.67<\/p>\n<p>Busto- 84<\/p>\n<p>Cintura- 78<\/p>\n<p>Quadril- 110<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 44<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VGG14",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 19,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.550",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          966,
          975,
          1033,
          965,
          1040
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2174,
            "date_created": "2016-04-11T02:04:40",
            "date_created_gmt": "2016-04-11T05:04:40",
            "date_modified": "2016-04-11T02:04:40",
            "date_modified_gmt": "2016-04-11T05:04:40",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG14.png",
            "name": "GG14",
            "alt": "",
            "position": 0
          },
          {
            "id": 2175,
            "date_created": "2016-04-11T02:05:06",
            "date_created_gmt": "2016-04-11T05:05:06",
            "date_modified": "2016-04-11T02:05:06",
            "date_modified_gmt": "2016-04-11T05:05:06",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG14TRAS.png",
            "name": "GG14TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 399,
        "meta_data": [
          {
            "id": 9111,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9116,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9117,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9118,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9143,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9144,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9145,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9149,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2183"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2171,
        "name": "Vestido Liganete Plus Size REF:013 <br \/>(lote 10 UN)<div id=\"destaque\">R$9,49 UN<\/div>",
        "slug": "vestido-liganete-plus-size-ref013",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/vestido-liganete-plus-size-ref013\/",
        "date_created": "2016-04-11T02:24:42",
        "date_created_gmt": "2016-04-11T02:24:42",
        "date_modified": "2018-07-18T07:56:47",
        "date_modified_gmt": "2018-07-18T07:56:47",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Vestido Plus Size de \u00a0excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.67<\/p>\n<p>Busto- 84<\/p>\n<p>Cintura- 78<\/p>\n<p>Quadril- 110<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p>Veste do 40 ao 44<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "VGG13",
        "price": "94.90",
        "regular_price": "94.90",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>94,90<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 42,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "2.500",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1033,
          972,
          1035,
          915,
          976
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 21,
            "name": "Vestido Liganete Plus Size",
            "slug": "vestido-liganete-plus-size"
          },
          {
            "id": 22,
            "name": "Vestidos",
            "slug": "vestidos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2173,
            "date_created": "2016-04-11T02:04:16",
            "date_created_gmt": "2016-04-11T05:04:16",
            "date_modified": "2016-04-11T02:04:16",
            "date_modified_gmt": "2016-04-11T05:04:16",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/GG13.png",
            "name": "GG13",
            "alt": "",
            "position": 0
          },
          {
            "id": 2172,
            "date_created": "2016-04-11T02:03:44",
            "date_created_gmt": "2016-04-11T05:03:44",
            "date_modified": "2016-04-11T02:03:44",
            "date_modified_gmt": "2016-04-11T05:03:44",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/13tras.png",
            "name": "13tras",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 397,
        "meta_data": [
          {
            "id": 9072,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9077,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9078,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9079,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9104,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9105,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9106,
            "key": "_wpb_vc_js_status",
            "value": "false"
          },
          {
            "id": 9110,
            "key": "_jetpack_dont_email_post_to_subs",
            "value": "1"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2171"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2170,
        "name": "Macaquito Liganete REF:M18 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm18",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm18\/",
        "date_created": "2016-04-11T02:00:37",
        "date_created_gmt": "2016-04-11T02:00:37",
        "date_modified": "2018-07-18T07:56:47",
        "date_modified_gmt": "2018-07-18T07:56:47",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.72<\/p>\n<p>Busto- 75<\/p>\n<p>Quadril 89<\/p>\n<p>Cintura 60<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M18",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 10,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.680",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1028,
          975,
          969,
          968,
          973
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2157,
            "date_created": "2016-04-11T01:20:07",
            "date_created_gmt": "2016-04-11T04:20:07",
            "date_modified": "2016-04-11T01:20:07",
            "date_modified_gmt": "2016-04-11T04:20:07",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M18.png",
            "name": "M18",
            "alt": "",
            "position": 0
          },
          {
            "id": 2158,
            "date_created": "2016-04-11T01:20:31",
            "date_created_gmt": "2016-04-11T04:20:31",
            "date_modified": "2016-04-11T01:20:31",
            "date_modified_gmt": "2016-04-11T04:20:31",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M18TRAS.png",
            "name": "M18TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 223,
        "meta_data": [
          {
            "id": 9034,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9039,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9040,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9041,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9066,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9067,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9068,
            "key": "_wpb_vc_js_status",
            "value": "false"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2170"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2166,
        "name": "Macaquito Liganete REF:M17 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm17",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm17\/",
        "date_created": "2016-04-11T01:57:50",
        "date_created_gmt": "2016-04-11T01:57:50",
        "date_modified": "2018-07-18T07:56:47",
        "date_modified_gmt": "2018-07-18T07:56:47",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.72<\/p>\n<p>Busto- 75<\/p>\n<p>Quadril 89<\/p>\n<p>Cintura 60<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M17",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 14,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.650",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          1038,
          969,
          1028,
          1030,
          1040
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2155,
            "date_created": "2016-04-11T01:19:21",
            "date_created_gmt": "2016-04-11T04:19:21",
            "date_modified": "2016-04-11T01:19:21",
            "date_modified_gmt": "2016-04-11T04:19:21",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M17.png",
            "name": "M17",
            "alt": "",
            "position": 0
          },
          {
            "id": 2156,
            "date_created": "2016-04-11T01:19:45",
            "date_created_gmt": "2016-04-11T04:19:45",
            "date_modified": "2016-04-11T01:19:45",
            "date_modified_gmt": "2016-04-11T04:19:45",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M17TRAS.png",
            "name": "M17TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 222,
        "meta_data": [
          {
            "id": 8996,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 9001,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 9002,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 9003,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 9028,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 9029,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 9030,
            "key": "_wpb_vc_js_status",
            "value": "false"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2166"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2165,
        "name": "Macaquito Liganete REF:M15 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm15-2",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm15-2\/",
        "date_created": "2016-04-11T01:55:10",
        "date_created_gmt": "2016-04-11T01:55:10",
        "date_modified": "2018-07-18T07:56:48",
        "date_modified_gmt": "2018-07-18T07:56:48",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.72<\/p>\n<p>Busto- 75<\/p>\n<p>Quadril 89<\/p>\n<p>Cintura 60<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 32,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.700",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          912,
          1030,
          1036,
          1038,
          975
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2153,
            "date_created": "2016-04-11T01:18:34",
            "date_created_gmt": "2016-04-11T04:18:34",
            "date_modified": "2016-04-11T01:18:34",
            "date_modified_gmt": "2016-04-11T04:18:34",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M15.png",
            "name": "M15",
            "alt": "",
            "position": 0
          },
          {
            "id": 2154,
            "date_created": "2016-04-11T01:19:00",
            "date_created_gmt": "2016-04-11T04:19:00",
            "date_modified": "2016-04-11T01:19:00",
            "date_modified_gmt": "2016-04-11T04:19:00",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M15TRAS.png",
            "name": "M15TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 220,
        "meta_data": [
          {
            "id": 8959,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 8964,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 8965,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 8966,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 8990,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 8991,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 8992,
            "key": "_wpb_vc_js_status",
            "value": "false"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2165"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2164,
        "name": "Macaquito Liganete REF:M11 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm11",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm11\/",
        "date_created": "2016-04-11T01:52:47",
        "date_created_gmt": "2016-04-11T01:52:47",
        "date_modified": "2018-07-18T07:56:48",
        "date_modified_gmt": "2018-07-18T07:56:48",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.72<\/p>\n<p>Busto- 75<\/p>\n<p>Quadril 89<\/p>\n<p>Cintura 60<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M11",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 4,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.670",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          974,
          975,
          964,
          968,
          980
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2151,
            "date_created": "2016-04-11T01:17:47",
            "date_created_gmt": "2016-04-11T04:17:47",
            "date_modified": "2016-04-11T01:17:47",
            "date_modified_gmt": "2016-04-11T04:17:47",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M11.png",
            "name": "M11",
            "alt": "",
            "position": 0
          },
          {
            "id": 2152,
            "date_created": "2016-04-11T01:18:09",
            "date_created_gmt": "2016-04-11T04:18:09",
            "date_modified": "2016-04-11T01:18:09",
            "date_modified_gmt": "2016-04-11T04:18:09",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M11TRAS.png",
            "name": "M11TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 215,
        "meta_data": [
          {
            "id": 8921,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 8926,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 8927,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 8928,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 8953,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 8954,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 8955,
            "key": "_wpb_vc_js_status",
            "value": "false"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2164"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2163,
        "name": "Macaquito Liganete REF:M8 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm8",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm8\/",
        "date_created": "2016-04-11T01:50:36",
        "date_created_gmt": "2016-04-11T01:50:36",
        "date_modified": "2018-07-18T07:56:48",
        "date_modified_gmt": "2018-07-18T07:56:48",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.72<\/p>\n<p>Busto- 75<\/p>\n<p>Quadril 89<\/p>\n<p>Cintura 60<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M8",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 57,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.690",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          965,
          970,
          978,
          1036,
          969
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2149,
            "date_created": "2016-04-11T01:17:02",
            "date_created_gmt": "2016-04-11T04:17:02",
            "date_modified": "2016-04-11T01:17:02",
            "date_modified_gmt": "2016-04-11T04:17:02",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M8.png",
            "name": "M8",
            "alt": "",
            "position": 0
          },
          {
            "id": 2150,
            "date_created": "2016-04-11T01:17:27",
            "date_created_gmt": "2016-04-11T04:17:27",
            "date_modified": "2016-04-11T01:17:27",
            "date_modified_gmt": "2016-04-11T04:17:27",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M8TRAS.png",
            "name": "M8TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 236,
        "meta_data": [
          {
            "id": 8883,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 8888,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 8889,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 8890,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 8915,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 8916,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 8917,
            "key": "_wpb_vc_js_status",
            "value": "false"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2163"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2162,
        "name": "Macaquito Liganete REF:M4 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm4",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm4\/",
        "date_created": "2016-04-11T01:48:23",
        "date_created_gmt": "2016-04-11T01:48:23",
        "date_modified": "2018-07-18T07:56:48",
        "date_modified_gmt": "2018-07-18T07:56:48",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.72<\/p>\n<p>Busto- 75<\/p>\n<p>Quadril 89<\/p>\n<p>Cintura 60<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M4",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 9,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.680",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          970,
          972,
          1039,
          973,
          979
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2147,
            "date_created": "2016-04-11T01:16:20",
            "date_created_gmt": "2016-04-11T04:16:20",
            "date_modified": "2016-04-11T01:16:20",
            "date_modified_gmt": "2016-04-11T04:16:20",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M4.png",
            "name": "M4",
            "alt": "",
            "position": 0
          },
          {
            "id": 2148,
            "date_created": "2016-04-11T01:16:42",
            "date_created_gmt": "2016-04-11T04:16:42",
            "date_modified": "2016-04-11T01:16:42",
            "date_modified_gmt": "2016-04-11T04:16:42",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M4TRAS.png",
            "name": "M4TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 232,
        "meta_data": [
          {
            "id": 8845,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 8850,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 8851,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 8852,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 8877,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 8878,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 8879,
            "key": "_wpb_vc_js_status",
            "value": "false"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2162"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      },
      {
        "id": 2161,
        "name": "Macaquito Liganete REF:M3 <br \/>(lote 10 UN)<div id=\"destaque\">R$7,40 UN<\/div>",
        "slug": "macaquito-liganete-refm3",
        "permalink": "https:\/\/oreidaliganete.com.br\/produto\/macaquito-liganete-refm3\/",
        "date_created": "2016-04-11T01:45:46",
        "date_created_gmt": "2016-04-11T01:45:46",
        "date_modified": "2018-07-18T07:56:48",
        "date_modified_gmt": "2018-07-18T07:56:48",
        "type": "simple",
        "status": "publish",
        "featured": false,
        "catalog_visibility": "visible",
        "description": "<p>\u00c9 um Macaquito de excelente qualidade feito com tecido liganete.<br \/>\nAs estampas s\u00e3o lindas e variadas.<\/p>\n<p>Composi\u00e7\u00e3o: 96% Poli\u00e9ster 4% Elastano<\/p>\n<p>Medidas da Modelo<\/p>\n<p>Altura- 1.72<\/p>\n<p>Busto- 75<\/p>\n<p>Quadril 89<\/p>\n<p>Cintura 60<\/p>\n<p>TAMANHO \u00daNICO<\/p>\n<p><strong>Aten\u00e7\u00e3o! Pode acontecer de o pedido n\u00e3o ir com a mesma estampa ou cores da imagem em visualiza\u00e7\u00e3o, mas ser\u00e1 respeitado o modelo solicitado,\u00a0sendo estampas variadas. \u00c9 uma f\u00e1brica e temos uma rotatividade muito grande no estoque. Obrigado pela compreens\u00e3o.<\/strong><\/p>\n",
        "short_description": "",
        "sku": "M3",
        "price": "74.00",
        "regular_price": "74.00",
        "sale_price": "",
        "date_on_sale_from": null,
        "date_on_sale_from_gmt": null,
        "date_on_sale_to": null,
        "date_on_sale_to_gmt": null,
        "price_html": "<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#82;&#36;<\/span>74,00<\/span>",
        "on_sale": false,
        "purchasable": true,
        "total_sales": 26,
        "virtual": false,
        "downloadable": false,
        "downloads": [],
        "download_limit": -1,
        "download_expiry": -1,
        "external_url": "",
        "button_text": "",
        "tax_status": "taxable",
        "tax_class": "",
        "manage_stock": false,
        "stock_quantity": null,
        "in_stock": true,
        "backorders": "no",
        "backorders_allowed": false,
        "backordered": false,
        "sold_individually": false,
        "weight": "1.680",
        "dimensions": {
          "length": "",
          "width": "",
          "height": ""
        },
        "shipping_required": true,
        "shipping_taxable": true,
        "shipping_class": "",
        "shipping_class_id": 0,
        "reviews_allowed": true,
        "average_rating": "0.00",
        "rating_count": 0,
        "related_ids": [
          912,
          1038,
          979,
          978,
          964
        ],
        "upsell_ids": [],
        "cross_sell_ids": [],
        "parent_id": 0,
        "purchase_note": "",
        "categories": [
          {
            "id": 19,
            "name": "Feminino",
            "slug": "feminino"
          },
          {
            "id": 27,
            "name": "Macaquitos",
            "slug": "macaquitos"
          }
        ],
        "tags": [],
        "images": [
          {
            "id": 2145,
            "date_created": "2016-04-11T01:15:31",
            "date_created_gmt": "2016-04-11T04:15:31",
            "date_modified": "2016-04-11T01:15:31",
            "date_modified_gmt": "2016-04-11T04:15:31",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M3.png",
            "name": "M3",
            "alt": "",
            "position": 0
          },
          {
            "id": 2146,
            "date_created": "2016-04-11T01:15:59",
            "date_created_gmt": "2016-04-11T04:15:59",
            "date_modified": "2016-04-11T01:15:59",
            "date_modified_gmt": "2016-04-11T04:15:59",
            "src": "https:\/\/oreidaliganete.com.br\/wp-content\/uploads\/2016\/04\/M3TRAS.png",
            "name": "M3TRAS",
            "alt": "",
            "position": 1
          }
        ],
        "attributes": [],
        "default_attributes": [],
        "variations": [],
        "grouped_products": [],
        "menu_order": 230,
        "meta_data": [
          {
            "id": 8807,
            "key": "_vc_post_settings",
            "value": {
              "vc_grid_id": []
            }
          },
          {
            "id": 8812,
            "key": "_wpas_done_all",
            "value": "1"
          },
          {
            "id": 8813,
            "key": "_product_video_gallery",
            "value": ""
          },
          {
            "id": 8814,
            "key": "_product_video_code",
            "value": ""
          },
          {
            "id": 8839,
            "key": "product_new",
            "value": "disable"
          },
          {
            "id": 8840,
            "key": "slide_template",
            "value": ""
          },
          {
            "id": 8841,
            "key": "_wpb_vc_js_status",
            "value": "false"
          }
        ],
        "_links": {
          "self": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products\/2161"
            }
          ],
          "collection": [
            {
              "href": "https:\/\/oreidaliganete.com.br\/wp-json\/wc\/v2\/products"
            }
          ]
        }
      }
    ],
    (error) => {
      if(!error) {
        console.log("Products upload success!")
        process.exit(0)
      } else {
        console.log(error)
      }
    }
)

