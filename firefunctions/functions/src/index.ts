import * as functions from 'firebase-functions';

export const onEntryCreate = functions.database
.ref('/Products/{productId}')
.onCreate((snapshot, context) => {
  const productId = context.params.productId
  console.log(`Product ${productId} being changed`)
  const product = snapshot.val()
  console.log(product)
  const productInfo = getInfo(product)

  return snapshot.ref.update(productInfo)
})

function getInfo(product: any) {

  const product_name = product.name.split("REF")[0].replace(' ', '')
  console.log(product_name) 
  
  const unit_price_cartao = product.name.split("R$")[1].split(" ")[0].replace(',', '.').replace(' ', '')
  console.log(unit_price_cartao)
  
  const unit_price_boleto = getDiscountPrice(unit_price_cartao)
  
  console.log(unit_price_boleto)
  
  const qtd_lote = product.name.split("lote")[1].split(")")[0].replace("UN", "").replace(' ', '')
  console.log(qtd_lote)
  
  const ref = product.name.split("REF:")[1].split("<")[0].replace(' ', '')
  console.log(ref)

    return {
      'ref': ref,
      'title': product_name,
      'price_unit_bol': unit_price_boleto,
      'price_unit_cartao': unit_price_cartao,
      'qtd_lote': qtd_lote,
    }
}

function getDiscountPrice(price: number) {
  return (price - (price * (5 / 100))).toFixed(2);
}
